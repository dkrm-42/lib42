#!/usr/bin/env sh

set -ex

ninja -C build/ clang-format
