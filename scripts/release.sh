#!/usr/bin/env sh

set -e

cd build/
meson configure -Db_sanitize=none --buildtype=release
meson compile
