#!/usr/bin/env sh

set -e

cd build/
meson configure -Db_sanitize=address,undefined --buildtype=debug
meson compile
