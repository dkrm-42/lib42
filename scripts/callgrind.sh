#!/usr/bin/env sh

valgrind --tool=callgrind --callgrind-out-file=$(CG_OUTPUT_FILE) ./$(NAME)
callgrind_annotate --auto=yes $(CG_OUTPUT_FILE)
