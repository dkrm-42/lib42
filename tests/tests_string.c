#include "header.h"

static void test_00_buffer_append_Simple(void)
{
	t_buffer *dst;
	t_buffer *src;
	char *s1 = "Hello";
	char *s2 = " World!";
	char cat[20];
	size_t l1 = strlen(s1);
	size_t l2 = strlen(s2);

	dst = buffer_ndup(s1, l1);
	src = buffer_ndup(s2, l2);
	strcpy(cat, s1);
	strcpy(cat + l1, s2);
	buffer_append(dst, src);

	v_assert_size_t(l2, ==, src->len);
	v_assert_str(s2, src->str);

	/* v_assert_size_t(16, ==, dst->sizemax); */
	v_assert_size_t(l1 + l2, ==, dst->len);
	v_assert_str(cat, dst->str);

	free(src->str);
	free(dst->str);
	free(src);
	free(dst);

	VTS;
}

static void test_01_buffer_append_EmptyBuffer(void)
{
	t_buffer *dst;
	t_buffer *src;
	char *s1 = "Emtpy";
	char *s2 = "";
	char cat[20];
	size_t l1 = strlen(s1);
	size_t l2 = strlen(s2);

	dst = buffer_ndup(s1, l1);
	src = buffer_ndup(s2, l2);
	strcpy(cat, s1);
	strcpy(cat + l1, s2);
	buffer_append(dst, src);

	v_assert_size_t(l2, ==, src->len);
	v_assert_str(s2, src->str);

	/* v_assert_size_t(16, ==, dst->sizemax); */
	v_assert_size_t(l1 + l2, ==, dst->len);
	v_assert_str(cat, dst->str);

	free(src->str);
	free(dst->str);
	free(src);
	free(dst);

	VTS;
}

void suite_buffer_append(void)
{
	test_00_buffer_append_Simple();
	test_01_buffer_append_EmptyBuffer();

	VSS;
}
#include "header.h"

void test_00_buffer_cat_Simple(void)
{
	t_buffer *b;
	char *s = "Hello World!";
	size_t l = strlen(s);

	b = buffer_new(4);
	buffer_cat(b, s);

	v_assert_size_t(l, ==, b->len);
	v_assert_size_t(16, ==, b->sizemax);
	v_assert_str(s, b->str);

	free(b->str);
	free(b);

	VTS;
}

void test_01_buffer_cat_NeedResize(void)
{
	t_buffer *b;
	char *s = "Hello World!";
	size_t l = strlen(s);

	b = buffer_new(12);
	buffer_cat(b, s);

	v_assert_size_t(l, ==, b->len);
	v_assert_size_t(24, ==, b->sizemax);
	v_assert_str(s, b->str);

	free(b->str);
	free(b);

	VTS;
}

void test_02_buffer_cat_BigConcatenation(void)
{
	t_buffer *b;
	char a[300];
	char m[3000];
	char am[4000];
	size_t alen;
	size_t mlen;

	b = buffer_new(8);

	memset(a, 'a', sizeof(a));
	a[sizeof(a) - 1] = '\0';
	alen = strlen(a);
	buffer_ncat(b, a, alen);
	v_assert_size_t(alen, ==, b->len);
	v_assert_size_t(512, ==, b->sizemax);
	v_assert_str(a, b->str);

	memset(m, 'm', sizeof(m));
	m[sizeof(m) - 1] = '\0';
	mlen = strlen(m);
	memset(am, 0, sizeof(am));
	strcat(am, a);
	strcat(am + sizeof(a) - 1, m);
	buffer_ncat(b, m, mlen);
	v_assert_size_t(alen + mlen, ==, b->len);
	v_assert_size_t(4096, ==, b->sizemax);
	v_assert_str(am, b->str);

	free(b->str);
	free(b);

	VTS;
}

void suite_buffer_cat(void)
{
	test_00_buffer_cat_Simple();
	test_01_buffer_cat_NeedResize();
	test_02_buffer_cat_BigConcatenation();

	VSS;
}
#include "header.h"

void test_00_buffer_insert_AddStringMiddle(void)
{
	t_buffer b;
	char *s = "docfolamour";
	char *good = "docteur folamour";
	size_t len_good = strlen(good);

	buffer_init(&b, 32);
	buffer_ncat(&b, s, strlen(s));
	buffer_insert(&b, 3, "teur ", 5);
	v_assert_str(good, b.str);
	v_assert_size_t(len_good, ==, b.len);

	TBUFFER_FREE(&b);
	VTS;
}

void test_01_buffer_insert_AddStringHead(void)
{
	t_buffer b;
	char *s = "folamour";
	char *good = "docteur folamour";
	size_t len_good = strlen(good);

	buffer_init(&b, 32);
	buffer_ncat(&b, s, strlen(s));
	buffer_insert(&b, 0, "docteur ", 8);
	v_assert_str(good, b.str);
	v_assert_size_t(len_good, ==, b.len);

	TBUFFER_FREE(&b);
	VTS;
}

void test_02_buffer_insert_AddStringTail(void)
{
	t_buffer b;
	char *s = "docteur";
	char *good = "docteur folamour";
	size_t len_good = strlen(good);

	buffer_init(&b, 32);
	buffer_ncat(&b, s, strlen(s));
	buffer_insert(&b, b.len, " folamour", 9);

	v_assert_str(good, b.str);
	v_assert_size_t(len_good, ==, b.len);

	TBUFFER_FREE(&b);
	VTS;
}

void suite_buffer_insert(void)
{
	test_00_buffer_insert_AddStringMiddle();
	test_01_buffer_insert_AddStringHead();
	test_02_buffer_insert_AddStringTail();

	VSS;
}
#include "header.h"

static void test_00_buffer_RESET_InIfStatement(void)
{
	t_buffer *buf;

	buf = buffer_new(8);
	buffer_cat(buf, "hello");
	if (1)
		TBUFFER_RESET(buf);

	v_assert_size_t(0, ==, buf->len);
	v_assert_str("", buf->str);

	VTS;
}

static void test_01_buffer_RESET_NotInBlockStatement(void)
{
	t_buffer *buf;

	buf = buffer_new(8);
	buffer_cat(buf, "world");

	TBUFFER_RESET(buf);

	v_assert_size_t(0, ==, buf->len);
	v_assert_str("", buf->str);

	VTS;
}

void suite_buffer_macros(void)
{
	test_00_buffer_RESET_InIfStatement();
	test_01_buffer_RESET_NotInBlockStatement();

	VSS;
}
#include "header.h"

void test_00_buffer_remove_Middle(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 5, 5);

	v_assert_size_t(5, ==, ret);
	v_assert_size_t(6, ==, b.len);
	v_assert_str("hellod", b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void test_01_buffer_remove_OutOfRange(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 11, 1);

	v_assert_size_t(0, ==, ret);
	v_assert_size_t(len, ==, b.len);
	v_assert_str(s, b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void test_02_buffer_remove_MiddleLongRange(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 5, 20);

	v_assert_size_t(6, ==, ret);
	v_assert_size_t(5, ==, b.len);
	v_assert_str("hello", b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void test_03_buffer_remove_Head(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 0, 6);

	v_assert_size_t(6, ==, ret);
	v_assert_size_t(5, ==, b.len);
	v_assert_str("world", b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void test_04_buffer_remove_HeadLongRange(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 0, 20);

	v_assert_size_t(11, ==, ret);
	v_assert_size_t(0, ==, b.len);
	v_assert_str("", b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void test_05_buffer_remove_Tail(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 10, 1);

	v_assert_size_t(1, ==, ret);
	v_assert_size_t(10, ==, b.len);
	v_assert_str("hello worl", b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void test_06_buffer_remove_TailLongRange(void)
{
	t_buffer b;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	buffer_init(&b, 32);
	buffer_ncat(&b, s, len);
	ret = buffer_remove(&b, 10, 20);

	v_assert_size_t(1, ==, ret);
	v_assert_size_t(10, ==, b.len);
	v_assert_str("hello worl", b.str);

	TBUFFER_FREE(&b);
	VTS;
}

void suite_buffer_remove(void)
{
	test_00_buffer_remove_Middle();
	test_01_buffer_remove_OutOfRange();
	test_02_buffer_remove_MiddleLongRange();
	test_03_buffer_remove_Head();
	test_04_buffer_remove_HeadLongRange();
	test_05_buffer_remove_Tail();
	test_06_buffer_remove_TailLongRange();

	VSS;
}
#include "header.h"

static t_buffer b;

static void setup(void)
{
	buffer_init(&b, 128);
	buffer_ncat(&b, "Hello World!", 12);
}

static void teardown(void) { TBUFFER_FREE(&b); }

void test_00_buffer_rewind_SimpleSize(void)
{
	int res;
	setup();

	res = buffer_rewind(&b, 3);
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(9, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello Wor", TBUFFER_GET(&b));
	v_assert_int(9, ==, res);

	teardown();
	VTS;
}

void test_01_buffer_rewind_ZeroSize(void)
{
	int res;
	setup();

	res = buffer_rewind(&b, 0);
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(12, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello World!", TBUFFER_GET(&b));
	v_assert_int(12, ==, res);

	teardown();
	VTS;
}

void test_02_buffer_rewind_FullSize(void)
{
	int res;
	setup();

	res = buffer_rewind(&b, 12);
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(0, ==, TBUFFER_LEN(&b));
	v_assert_str("", TBUFFER_GET(&b));
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

void test_03_buffer_rewind_SizeOverflow(void)
{
	int res;
	setup();

	res = buffer_rewind(&b, 42);
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(12, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello World!", TBUFFER_GET(&b));
	v_assert_int(-1, ==, res);

	teardown();
	VTS;
}

void suite_buffer_rewind(void)
{
	test_00_buffer_rewind_SimpleSize();
	test_01_buffer_rewind_ZeroSize();
	test_02_buffer_rewind_FullSize();
	test_03_buffer_rewind_SizeOverflow();

	VSS;
}
#include "header.h"

static t_buffer b;

static void setup(void)
{
	buffer_init(&b, 128);
	buffer_ncat(&b, "Hello World!", 12);
}

static void teardown(void) { TBUFFER_FREE(&b); }

void test_00_buffer_rewindchr_SimpleRewind(void)
{
	/* size_t res; */
	setup();

	/* res = buffer_rewindchr(&b, 'l'); */
	buffer_rewindchr(&b, 'l');
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(9, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello Wor", TBUFFER_GET(&b));

	teardown();
	VTS;
}

void test_01_buffer_rewindchr_EndOfString(void)
{
	/* size_t res; */
	setup();

	/* res = buffer_rewindchr(&b, '\0'); */
	buffer_rewindchr(&b, '\0');
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(12, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello World!", TBUFFER_GET(&b));

	teardown();
	VTS;
}

void test_02_buffer_rewindchr_LastChar(void)
{
	/* size_t res; */
	setup();

	/* res = buffer_rewindchr(&b, '!'); */
	buffer_rewindchr(&b, '!');
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(11, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello World", TBUFFER_GET(&b));

	teardown();
	VTS;
}

void test_03_buffer_rewindchr_FirstChar(void)
{
	/* size_t res; */
	setup();

	/* res = buffer_rewindchr(&b, 'H'); */
	buffer_rewindchr(&b, 'H');
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(0, ==, TBUFFER_LEN(&b));
	v_assert_str("", TBUFFER_GET(&b));

	teardown();
	VTS;
}

void test_04_buffer_rewindchr_MultipleOccurrence(void)
{
	/* size_t res; */
	setup();

	/* res = buffer_rewindchr(&b, 'o'); */
	buffer_rewindchr(&b, 'o');
	v_assert_size_t(128, ==, TBUFFER_MAX(&b));
	v_assert_size_t(7, ==, TBUFFER_LEN(&b));
	v_assert_str("Hello W", TBUFFER_GET(&b));

	teardown();
	VTS;
}

void suite_buffer_rewindchr(void)
{
	test_00_buffer_rewindchr_SimpleRewind();
	test_01_buffer_rewindchr_EndOfString();
	test_02_buffer_rewindchr_LastChar();
	test_03_buffer_rewindchr_FirstChar();
	test_04_buffer_rewindchr_MultipleOccurrence();

	VSS;
}
#include "header.h"

void test_00_buffer_set_simple(void)
{
	t_buffer b;
	char *s = "**********";
	size_t len = strlen(s);

	buffer_init(&b, 0);
	buffer_set(&b, '*', 10);
	buffer_set(&b, '\0', 1);
	v_assert_str(s, b.str);
	v_assert_size_t(len + 1, ==, TBUFFER_LEN(&b));

	buffer_set(&b, '#', 1);
	v_assert_str(s, b.str);
	v_assert_size_t(len + 2, ==, TBUFFER_LEN(&b));

	TBUFFER_FREE(&b);
	VTS;
}

void suite_buffer_set(void)
{
	test_00_buffer_set_simple();

	VSS;
}
#include "header.h"

static void test_00_string_append_Simple(void)
{
	t_string dst;
	t_string src;
	char *s1 = "Hello";
	char *s2 = " World!";
	char cat[20];
	size_t l1 = strlen(s1);
	size_t l2 = strlen(s2);

	string_init_ndup(&dst, s1, l1);
	string_init_ndup(&src, s2, l2);
	strcpy(cat, s1);
	strcpy(cat + l1, s2);
	string_append(&dst, &src);

	v_assert_size_t(l2, ==, src.len);
	v_assert_str(s2, src.str);

	/* v_assert_size_t(16, ==, dst.capacity); */
	v_assert_size_t(l1 + l2, ==, dst.len);
	v_assert_str(cat, dst.str);

	free(src.str);
	free(dst.str);

	VTS;
}

static void test_01_string_append_Emptystring(void)
{
	t_string dst;
	t_string src;
	char *s1 = "Empty";
	char *s2 = "";
	char cat[20];
	size_t l1 = strlen(s1);
	size_t l2 = strlen(s2);

	string_init_ndup(&dst, s1, l1);
	string_init_ndup(&src, s2, l2);
	strcpy(cat, s1);
	strcpy(cat + l1, s2);
	string_append(&dst, &src);

	v_assert_size_t(l2, ==, src.len);
	v_assert_str(s2, src.str);

	/* v_assert_size_t(16, ==, dst.capacity); */
	v_assert_size_t(l1 + l2, ==, dst.len);
	v_assert_str(cat, dst.str);

	free(src.str);
	free(dst.str);

	VTS;
}

void suite_string_append(void)
{
	test_00_string_append_Simple();
	test_01_string_append_Emptystring();

	VSS;
}
#include "header.h"

static void test_00_string_cat_Simple(void)
{
	t_string string;
	char *s = "Hello World!";
	size_t l = strlen(s);

	string_init_with_capacity(&string, 4);
	string_cat(&string, s);

	v_assert_size_t(l, ==, string.len);
	v_assert_size_t(64, ==, string.capacity);
	v_assert_str(s, string.str);

	free(string.str);

	VTS;
}

static void test_01_string_cat_NeedResize(void)
{
	t_string string;
	char *s = "Hello World!";
	size_t l = strlen(s);

	string_init_with_capacity(&string, 12);
	string_cat(&string, s);

	v_assert_size_t(l, ==, string.len);
	v_assert_size_t(64, ==, string.capacity);
	v_assert_str(s, string.str);

	free(string.str);

	VTS;
}

static void test_02_string_cat_BigConcatenation(void)
{
	t_string string;
	char a[300];
	char m[3000];
	char am[4000];
	size_t alen;
	size_t mlen;

	string_init_with_capacity(&string, 8);

	memset(a, 'a', sizeof(a));
	a[sizeof(a) - 1] = '\0';
	alen = strlen(a);
	string_ncat(&string, a, alen);
	v_assert_size_t(alen, ==, string.len);
	v_assert_size_t(512, ==, string.capacity);
	v_assert_str(a, string.str);

	memset(m, 'm', sizeof(m));
	m[sizeof(m) - 1] = '\0';
	mlen = strlen(m);
	memset(am, 0, sizeof(am));
	strcat(am, a);
	strcat(am + sizeof(a) - 1, m);
	string_ncat(&string, m, mlen);
	v_assert_size_t(alen + mlen, ==, string.len);
	v_assert_size_t(4096, ==, string.capacity);
	v_assert_str(am, string.str);

	free(string.str);

	VTS;
}

static void test_03_string_cat_Empty(void)
{
	t_string string;
	char *s = "Hello";

	string_init(&string);

	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	string_cat(&string, s);

	v_assert_size_t(5, ==, string.len);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_str(s, string.str);

	string_shutdown(&string);
}

static void test_04_string_ncat_Simple(void)
{
	t_string string;
	char *s = " World!";

	string_init_dup(&string, "Hello");

	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	string_ncat(&string, s, 7);

	v_assert_size_t(12, ==, string.len);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_str("Hello World!", string.str);

	string_shutdown(&string);
}

static void test_05_string_ncat_Empty(void)
{
	t_string string;
	char *s = "Hello World!";

	string_init(&string);

	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	string_ncat(&string, s, 5);

	v_assert_size_t(5, ==, string.len);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_str("Hello", string.str);

	string_shutdown(&string);
}

void suite_string_cat(void)
{
	test_00_string_cat_Simple();
	test_01_string_cat_NeedResize();
	test_02_string_cat_BigConcatenation();
	test_03_string_cat_Empty();

	test_04_string_ncat_Simple();
	test_05_string_ncat_Empty();

	VSS;
}
#include "header.h"

static void test_00_string_clone_Simple(void)
{
	t_string origin;
	t_string cp;

	string_init_dup(&origin, "Hello World!");
	string_clone(&cp, &origin);

	v_assert_size_t(origin.len, ==, cp.len);
	v_assert_str(origin.str, cp.str);
	v_assert_ptr(origin.str, !=, cp.str);
	v_assert_ptr(&origin, !=, &cp);

	string_shutdown(&origin);
	string_shutdown(&cp);
	VTS;
}

static void test_01_string_clone_Empty(void)
{
	t_string origin;
	t_string cp;

	string_init(&origin);
	string_clone(&cp, &origin);

	v_assert_size_t(origin.len, ==, cp.len);
	v_assert_str(origin.str, cp.str);
	v_assert_ptr(origin.str, !=, cp.str);
	v_assert_ptr(&origin, !=, &cp);

	string_shutdown(&origin);
	string_shutdown(&cp);
	VTS;
}

void suite_string_clone(void)
{
	test_00_string_clone_Simple();
	test_01_string_clone_Empty();

	VSS;
}
#include "header.h"

static t_string string;

static void test_00_string_init_SizeBelowMinimum(void)
{
	string_init_with_capacity(&string, 4);

	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);

	string_shutdown(&string);
	VTS;
}

static void test_01_string_init_SizeAboveMinimum(void)
{
	string_init_with_capacity(&string, 1111);

	v_assert_size_t(2048, ==, string.capacity);

	string_shutdown(&string);
	VTS;
}

static void test_02_string_init_SizePowerOf2(void)
{
	string_init_with_capacity(&string, 256);

	v_assert_size_t(512, ==, string.capacity);

	string_shutdown(&string);
	VTS;
}

static void test_03_string_init_SizeOfInit(void)
{
	string_init_with_capacity(&string, STRING_INIT_SIZE);

	v_assert_size_t(next_power_of_2(STRING_INIT_SIZE), ==, string.capacity);

	string_shutdown(&string);
	VTS;
}

static void test_04_string_init_SizePowerOf2MinsOne(void)
{
	string_init_with_capacity(&string, 255);

	v_assert_size_t(256, ==, string.capacity);

	string_shutdown(&string);
	VTS;
}

static void test_05_string_create_SizeBelowMinimum(void)
{
	t_string *string = string_create_with_capacity(4);

	v_assert_size_t(STRING_INIT_SIZE, ==, string->capacity);

	string_destroy(string);
	VTS;
}

static void test_06_string_create_SizeAboveMinimum(void)
{
	t_string *string = string_create_with_capacity(1111);

	v_assert_size_t(2048, ==, string->capacity);

	string_destroy(string);
	VTS;
}

static void test_07_string_create_SizePowerOf2(void)
{
	t_string *string = string_create_with_capacity(256);

	v_assert_size_t(512, ==, string->capacity);

	string_destroy(string);
	VTS;
}

static void test_08_string_create_SizeOfInit(void)
{
	t_string *string = string_create_with_capacity(STRING_INIT_SIZE);

	v_assert_size_t(next_power_of_2(STRING_INIT_SIZE), ==,
			string->capacity);

	string_destroy(string);
	VTS;
}

static void test_09_string_create_SizePowerOf2MinsOne(void)
{
	t_string *string = string_create_with_capacity(255);

	v_assert_size_t(256, ==, string->capacity);

	string_destroy(string);
	VTS;
}

static void test_10_string_init_default_size(void)
{
	string_init(&string);

	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);

	string_shutdown(&string);
	VTS;
}
static void test_11_string_create_default_size(void)
{
	t_string *string = string_create();

	v_assert_size_t(STRING_INIT_SIZE, ==, string->capacity);

	string_destroy(string);
	VTS;
}

void suite_string_create_init(void)
{
	test_00_string_init_SizeBelowMinimum();
	test_01_string_init_SizeAboveMinimum();
	test_02_string_init_SizePowerOf2();
	test_03_string_init_SizeOfInit();
	test_04_string_init_SizePowerOf2MinsOne();

	test_05_string_create_SizeBelowMinimum();
	test_06_string_create_SizeAboveMinimum();
	test_07_string_create_SizePowerOf2();
	test_08_string_create_SizeOfInit();
	test_09_string_create_SizePowerOf2MinsOne();

	test_10_string_init_default_size();
	test_11_string_create_default_size();

	VSS;
}

#include "header.h"

static t_string string;

static void teardown(void) { free(string.str); }

static void test_00_string_dup_String(void)
{
	char *s = "Hello world";
	size_t len = strlen(s);

	string_init_dup(&string, s);

	v_assert_size_t(len, ==, string.len);
	v_assert_str(s, string.str);

	teardown();
	VTS;
}
static void test_01_string_dup_EmptyString(void)
{
	char *s = "";
	size_t len = strlen(s);

	string_init_dup(&string, s);

	v_assert_size_t(len, ==, string.len);
	v_assert_str(s, string.str);

	teardown();
	VTS;
}

static void test_02_string_ndup_SimpleString(void)
{
	char *s = "Hello World!";
	/* size_t	len = strlen(s); */

	string_init_ndup(&string, s, 5);

	v_assert_size_t(5, ==, string.len);
	v_assert_str("Hello", string.str);

	teardown();
	VTS;
}

// static void	test_03_string_ndup_ShortenString(void)
// {
// 	char	*s = "abc";
// 	size_t	len = strlen(s);

// 	string_init_ndup(&string, s, 10);

// 	v_assert_size_t(64, ==, string.capacity);
// 	v_assert_size_t(len, ==, string.len);
// 	v_assert_str(s, string.str);

// 	teardown();
// 	VTS;
// }

static void test_04_string_ndup_ZeroLength(void)
{
	char *s = "World!";
	/* size_t	len = strlen(s); */

	string_init_ndup(&string, s, 0);

	v_assert_size_t(0, ==, string.len);
	v_assert_str("", string.str);

	teardown();
	VTS;
}

static void test_05_string_create_dup_Simple(void)
{
	t_string *str;
	char *s = "World!";

	str = string_create_dup(s);

	v_assert_ptr(NULL, !=, str);
	v_assert_size_t(strlen(s), ==, str->len);
	v_assert_size_t(64, ==, str->capacity);
	v_assert_str(s, str->str);

	string_destroy(str);
	VTS;
}

static void test_06_string_create_dup_ZeroLength(void)
{
	t_string *str;
	char *s = "";

	str = string_create_dup(s);

	v_assert_ptr(NULL, !=, str);
	v_assert_size_t(strlen(s), ==, str->len);
	v_assert_size_t(64, ==, str->capacity);
	v_assert_str(s, str->str);

	string_destroy(str);
	VTS;
}

static void test_07_string_create_ndup_Simple(void)
{
	t_string *str;
	char *s = "World!";

	str = string_create_ndup(s, 3);

	v_assert_ptr(NULL, !=, str);
	v_assert_size_t(3, ==, str->len);
	v_assert_size_t(64, ==, str->capacity);
	v_assert_str("Wor", str->str);

	string_destroy(str);
	VTS;
}

static void test_08_string_create_ndup_ZeroLength(void)
{
	t_string *str;
	char *s = "World!";

	str = string_create_ndup(s, 0);

	v_assert_ptr(NULL, !=, str);
	v_assert_size_t(0, ==, str->len);
	v_assert_size_t(64, ==, str->capacity);
	v_assert_str("", str->str);

	string_destroy(str);
	VTS;
}

void suite_string_dup(void)
{
	test_00_string_dup_String();
	test_01_string_dup_EmptyString();
	test_02_string_ndup_SimpleString();
	// test_03_string_ndup_ShortenString(); // string doesn't check if give
	// len is right
	test_04_string_ndup_ZeroLength();
	test_05_string_create_dup_Simple();
	test_06_string_create_dup_ZeroLength();
	test_07_string_create_ndup_Simple();
	test_08_string_create_ndup_ZeroLength();

	VSS;
}
#include "header.h"

void test_00_string_insert_AddStringMiddle(void)
{
	t_string string;
	char *s = "docfolamour";
	char *good = "docteur folamour";
	size_t len_good = strlen(good);

	string_init(&string);
	string_ncat(&string, s, strlen(s));
	string_insert(&string, 3, "teur ", 5);
	v_assert_str(good, string.str);
	v_assert_size_t(len_good, ==, string.len);

	free(string.str);
	VTS;
}

void test_01_string_insert_AddStringHead(void)
{
	t_string string;
	char *s = "folamour";
	char *good = "docteur folamour";
	size_t len_good = strlen(good);

	string_init(&string);
	string_ncat(&string, s, strlen(s));
	string_insert(&string, 0, "docteur ", 8);
	v_assert_str(good, string.str);
	v_assert_size_t(len_good, ==, string.len);

	free(string.str);
	VTS;
}

void test_02_string_insert_AddStringTail(void)
{
	t_string string;
	char *s = "docteur";
	char *good = "docteur folamour";
	size_t len_good = strlen(good);

	string_init(&string);
	string_ncat(&string, s, strlen(s));
	string_insert(&string, string.len, " folamour", 9);

	v_assert_str(good, string.str);
	v_assert_size_t(len_good, ==, string.len);

	free(string.str);
	VTS;
}

void suite_string_insert(void)
{
	test_00_string_insert_AddStringMiddle();
	test_01_string_insert_AddStringHead();
	test_02_string_insert_AddStringTail();

	VSS;
}
#include "header.h"

static void test_00_string_RESET_InIfStatement(void)
{
	t_string *buf;

	buf = string_new(8);
	string_cat(buf, "hello");
	if (1)
		TBUFFER_RESET(buf);

	v_assert_size_t(0, ==, buf->len);
	v_assert_str("", buf->str);

	free(TBUFFER_GET(buf));
	free(buf);
	VTS;
}

static void test_01_string_RESET_NotInBlockStatement(void)
{
	t_string *buf;

	buf = string_new(8);
	string_cat(buf, "world");

	TBUFFER_RESET(buf);

	v_assert_size_t(0, ==, buf->len);
	v_assert_str("", buf->str);

	free(TBUFFER_GET(buf));
	free(buf);
	VTS;
}

void suite_string_macros(void)
{
	test_00_string_RESET_InIfStatement();
	test_01_string_RESET_NotInBlockStatement();

	VSS;
}
#include "header.h"

static void test_00_string_merge_NoRealloc(void)
{
	t_string b1;
	t_string b2;
	t_string merge;
	char *s1 = "Hello World!";
	char *s2 = "Good Morning";
	char s[100];
	size_t l1 = strlen(s1);
	size_t l2 = strlen(s2);

	s[0] = '\0';
	strcat(s, s1);
	strcat(s, s2);
	string_init_dup(&b1, s1);
	string_init_dup(&b2, s2);
	string_merge(&merge, &b1, &b2);

	v_assert_str(s1, b1.str);
	v_assert_str(s2, b2.str);
	v_assert_size_t(STRING_INIT_SIZE, ==, merge.capacity);
	v_assert_size_t(l1 + l2, ==, merge.len);
	v_assert_str(s, merge.str);

	free(b1.str);
	free(b2.str);
	free(merge.str);
	VTS;
}

static void test_01_string_merge_Realloc(void)
{
	t_string b1;
	t_string b2;
	t_string merge;
	char s1[100];
	char s2[100];

	// setup
	memset(s1, '\0', 100);
	memset(s1, '*', STRING_INIT_SIZE + 10);
	memset(s2, '\0', 100);
	memset(s2, '#', 6);
	string_init_dup(&b1, s1);
	string_init_dup(&b2, s2);
	strcat(s1, s2);
	string_merge(&merge, &b1, &b2);

	// test
	v_assert_size_t(next_power_of_2(STRING_INIT_SIZE), ==, b1.capacity);
	v_assert_size_t(STRING_INIT_SIZE, ==, b2.capacity);
	v_assert_size_t(next_power_of_2(STRING_INIT_SIZE), ==, merge.capacity);
	v_assert_size_t(80, ==, merge.len);
	v_assert_str(s1, merge.str);

	// teardown
	free(b1.str);
	free(b2.str);
	free(merge.str);
	VTS;
}

void suite_string_merge(void)
{
	test_00_string_merge_NoRealloc();
	test_01_string_merge_Realloc();

	VSS;
}
#include "header.h"

void test_00_string_remove_Middle(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 5, 5);

	v_assert_size_t(5, ==, ret);
	v_assert_size_t(6, ==, string.len);
	v_assert_str("hellod", string.str);

	free(string.str);
	VTS;
}

void test_01_string_remove_OutOfRange(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 11, 1);

	v_assert_size_t(0, ==, ret);
	v_assert_size_t(len, ==, string.len);
	v_assert_str(s, string.str);

	free(string.str);
	VTS;
}

void test_02_string_remove_MiddleLongRange(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 5, 20);

	v_assert_size_t(6, ==, ret);
	v_assert_size_t(5, ==, string.len);
	v_assert_str("hello", string.str);

	free(string.str);
	VTS;
}

void test_03_string_remove_Head(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 0, 6);

	v_assert_size_t(6, ==, ret);
	v_assert_size_t(5, ==, string.len);
	v_assert_str("world", string.str);

	free(string.str);
	VTS;
}

void test_04_string_remove_HeadLongRange(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 0, 20);

	v_assert_size_t(11, ==, ret);
	v_assert_size_t(0, ==, string.len);
	v_assert_str("", string.str);

	free(string.str);
	VTS;
}

void test_05_string_remove_Tail(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 10, 1);

	v_assert_size_t(1, ==, ret);
	v_assert_size_t(10, ==, string.len);
	v_assert_str("hello worl", string.str);

	free(string.str);
	VTS;
}

void test_06_string_remove_TailLongRange(void)
{
	t_string string;
	char *s = "hello world";
	size_t len = strlen(s);
	size_t ret;

	string_init(&string);
	string_ncat(&string, s, len);
	ret = string_remove(&string, 10, 20);

	v_assert_size_t(1, ==, ret);
	v_assert_size_t(10, ==, string.len);
	v_assert_str("hello worl", string.str);

	free(string.str);
	VTS;
}

void suite_string_remove(void)
{
	test_00_string_remove_Middle();
	test_01_string_remove_OutOfRange();
	test_02_string_remove_MiddleLongRange();
	test_03_string_remove_Head();
	test_04_string_remove_HeadLongRange();
	test_05_string_remove_Tail();
	test_06_string_remove_TailLongRange();

	VSS;
}
#include "header.h"

static t_string string;

static void setup(void)
{
	string_init(&string);
	string_ncat(&string, "Hello World!", 12);
}

static void teardown(void) { free(string.str); }

static void test_00_string_remove_back_SimpleSize(void)
{
	int res;
	setup();

	res = string_remove_back(&string, 3);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(9, ==, string.len);
	v_assert_str("Hello Wor", string.str);
	v_assert_int(9, ==, res);

	teardown();
	VTS;
}

static void test_01_string_remove_back_ZeroSize(void)
{
	int res;
	setup();

	res = string_remove_back(&string, 0);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(12, ==, string.len);
	v_assert_str("Hello World!", string.str);
	v_assert_int(12, ==, res);

	teardown();
	VTS;
}

static void test_02_string_remove_back_FullSize(void)
{
	int res;
	setup();

	res = string_remove_back(&string, 12);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(0, ==, string.len);
	v_assert_str("", string.str);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

static void test_03_string_remove_back_SizeOverflow(void)
{
	int res;
	setup();

	res = string_remove_back(&string, 42);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(12, ==, string.len);
	v_assert_str("Hello World!", string.str);
	v_assert_int(-1, ==, res);

	teardown();
	VTS;
}

void suite_string_remove_back(void)
{
	test_00_string_remove_back_SimpleSize();
	test_01_string_remove_back_ZeroSize();
	test_02_string_remove_back_FullSize();
	test_03_string_remove_back_SizeOverflow();

	VSS;
}
#include "header.h"

static t_string string;

static void setup(void)
{
	string_init(&string);
	string_ncat(&string, "Hello World!", 12);
}

static void teardown(void) { free(string.str); }

static void test_00_string_remove_back_chr_SimpleRewind(void)
{
	/* size_t res; */
	setup();

	/* res = string_remove_back_chr(&string, 'l'); */
	string_remove_back_chr(&string, 'l');
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(9, ==, string.len);
	v_assert_str("Hello Wor", string.str);

	teardown();
	VTS;
}

static void test_01_string_remove_back_chr_EndOfString(void)
{
	/* size_t res; */
	setup();

	/* res = string_remove_back_chr(&string, '\0'); */
	string_remove_back_chr(&string, '\0');
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(12, ==, string.len);
	v_assert_str("Hello World!", string.str);

	teardown();
	VTS;
}

static void test_02_string_remove_back_chr_LastChar(void)
{
	/* size_t res; */
	setup();

	/* res = string_remove_back_chr(&string, '!'); */
	string_remove_back_chr(&string, '!');
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(11, ==, string.len);
	v_assert_str("Hello World", string.str);

	teardown();
	VTS;
}

static void test_03_string_remove_back_chr_FirstChar(void)
{
	/* size_t res; */
	setup();

	/* res = string_remove_back_chr(&string, 'H'); */
	string_remove_back_chr(&string, 'H');
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(0, ==, string.len);
	v_assert_str("", string.str);

	teardown();
	VTS;
}

static void test_04_string_remove_back_chr_MultipleOccurrence(void)
{
	/* size_t res; */
	setup();

	/* res = string_remove_back_chr(&string, 'o'); */
	string_remove_back_chr(&string, 'o');
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(7, ==, string.len);
	v_assert_str("Hello W", string.str);

	teardown();
	VTS;
}

void suite_string_remove_back_chr(void)
{
	test_00_string_remove_back_chr_SimpleRewind();
	test_01_string_remove_back_chr_EndOfString();
	test_02_string_remove_back_chr_LastChar();
	test_03_string_remove_back_chr_FirstChar();
	test_04_string_remove_back_chr_MultipleOccurrence();

	VSS;
}
#include "header.h"

static t_string string;

static void setup(char *s, size_t len, size_t max)
{
	string.str = malloc(max);
	memset(string.str, 0, max);
	memcpy(string.str, s, len);
	string.len = len;
	string.capacity = max;
}

static void teardown(void) { free(string.str); }

static void test_00_string_replace_BiggerString(void)
{
	char *s = "Hello";
	size_t len = strlen(s);
	size_t max = 8;
	setup(s, len, max);

	char *replace = "World!";
	size_t lrep = strlen(replace);
	string_replace(&string, replace);

	v_assert_size_t(max, ==, string.capacity);
	v_assert_size_t(lrep, ==, string.len);
	v_assert_str(replace, string.str);

	teardown();
	VTS;
}

static void test_01_string_nreplace_BigStringNotNullTerminated(void)
{
	char *s = "Hello";
	size_t len = strlen(s);
	size_t max = 8;
	setup(s, len, max);

	char *replace = "World";
	size_t lrep = strlen(replace);
	char arr[501];
	for (size_t i = 0; i < 100; ++i)
		memcpy(arr + (i * lrep), replace, lrep);
	arr[500] = 'x';

	string_nreplace(&string, arr, 500);

	v_assert_size_t(512, ==, string.capacity);
	v_assert_size_t(500, ==, string.len);
	arr[500] = '\0';
	v_assert_str(arr, string.str);

	teardown();
	VTS;
}

static void test_02_string_replace_LowerString(void)
{
	char *s = "Hello World!";
	size_t len = strlen(s);
	size_t max = 16;
	setup(s, len, max);

	char *replace = "abc";
	size_t lrep = strlen(replace);
	string_replace(&string, replace);

	v_assert_size_t(16, ==, string.capacity);
	v_assert_size_t(lrep, ==, string.len);
	v_assert_str(replace, string.str);

	teardown();
	VTS;
}

void suite_string_replace(void)
{
	test_00_string_replace_BiggerString();
	test_01_string_nreplace_BigStringNotNullTerminated();
	test_02_string_replace_LowerString();

	VSS;
}
#include "header.h"

static t_string string;

static void teardown(void) { free(string.str); }

static void test_00_string_reserve_DontNeedExpansion(void)
{
	string_init_with_capacity(&string, 4);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);

	string_reserve(&string, 7);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);

	teardown();
	VTS;
}

static void test_01_string_reserve_ExpandMoreThan2(void)
{
	string_init_with_capacity(&string, 1111);
	v_assert_size_t(2048, ==, string.capacity);

	string_reserve(&string, 5555);
	v_assert_size_t(8192, ==, string.capacity);

	teardown();
	VTS;
}

static void test_02_string_reserve_ExpandUseless(void)
{
	string_init_with_capacity(&string, 256); // '\0' make it be 257
	v_assert_size_t(512, ==, string.capacity);

	// len is 0 then it doesn't need to realloc
	string_reserve(&string, 256); // 0 + 256 = 256 -> 512
	v_assert_size_t(512, ==, string.capacity);

	teardown();
	VTS;
}

static void test_03_string_reserve_ExpandPowerOf2(void)
{
	char *big_str;

	string_init_with_capacity(&string, 255);
	v_assert_size_t(256, ==, string.capacity);

	big_str = ft_memalloc(257); // '\0' everywhere
	ft_memset(big_str, 'c', 256);

	string_ncat(&string, big_str, 256); // 0 + (256 + 1) = 257 -> 1024
	v_assert_size_t(512, ==, string.capacity);

	teardown();
	free(big_str);
	VTS;
}

static void test_03_string_reserve_ExpandFor2Strings(void)
{
	char *first_str;
	char *second_str;

	first_str = ft_memalloc(256); // '\0' everywhere
	ft_memset(first_str, 'a', 255);

	string_init_ndup(&string, first_str, 255);
	v_assert_size_t(256, ==, string.capacity);

	second_str = ft_memalloc(258); // '\0' everywhere
	ft_memset(second_str, 'b', 257);

	string_ncat(&string, second_str, 257); // 255 + (257 + 1) = 513 -> 1024
	v_assert_size_t(1024, ==, string.capacity);

	free(first_str);
	free(second_str);
	teardown();
	VTS;
}

void suite_string_reserve(void)
{
	test_00_string_reserve_DontNeedExpansion();
	test_01_string_reserve_ExpandMoreThan2();
	test_02_string_reserve_ExpandUseless();
	test_03_string_reserve_ExpandPowerOf2();
	test_03_string_reserve_ExpandFor2Strings();

	VSS;
}
#include "header.h"

static t_string *string;

static void teardown(void)
{
	free(TBUFFER_GET(string));
	free(string);
}

static void test_00_string_resize_ExpandNotMuch(void)
{
	string = string_new(4);
	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(string));

	string = string_resize(string, 7);
	v_assert_size_t(STRING_INIT_SIZE * 2, ==, TBUFFER_MAX(string));

	teardown();
	VTS;
}

static void test_01_string_resize_ExpandMoreThan2(void)
{
	string = string_new(1111);
	v_assert_size_t(2048, ==, TBUFFER_MAX(string));

	string = string_resize(string, 5555);
	v_assert_size_t(8192, ==, TBUFFER_MAX(string));

	teardown();
	VTS;
}

static void test_02_string_resize_ExpandPowerOf2(void)
{
	string = string_new(256);
	v_assert_size_t(512, ==, TBUFFER_MAX(string));

	string = string_resize(string, 256);
	v_assert_size_t(1024, ==, TBUFFER_MAX(string));

	teardown();
	VTS;
}

void suite_string_resize(void)
{
	test_00_string_resize_ExpandNotMuch();
	test_01_string_resize_ExpandMoreThan2();
	test_02_string_resize_ExpandPowerOf2();

	VSS;
}
#include "header.h"

static t_string *b;
static char *s = "Hello World!";
static int len = 12;

static void setup(void) { b = string_dup(s); }

static void teardown(void)
{
	TBUFFER_FREE(b);
	free(b);
}

static void test_00_string_rewindnchr_FirstChar(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, 'l', 1);
	v_assert_int(3, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len - res, ==, TBUFFER_LEN(b));
	v_assert_str("Hello Wor", TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_01_string_rewindnchr_EndOfString(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, '\0', 1);
	v_assert_int(-1, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len, ==, TBUFFER_LEN(b));
	v_assert_str(s, TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_02_string_rewindnchr_LastChar(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, '!', 2);
	v_assert_int(1, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len - res, ==, TBUFFER_LEN(b));
	v_assert_str("Hello World", TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_03_string_rewindnchr_FirstChar(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, 'H', 8);
	v_assert_int(len, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(0, ==, TBUFFER_LEN(b));
	v_assert_str("", TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_04_string_rewindnchr_MultipleOccurrence(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, 'o', 2);
	v_assert_int(8, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len - res, ==, TBUFFER_LEN(b));
	v_assert_str("Hell", TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_05_string_rewindnchr_MultipleOccurrence0Times(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, 'o', 0);
	v_assert_int(-1, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len, ==, TBUFFER_LEN(b));
	v_assert_str(s, TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_06_string_rewindnchr_MultipleOccurrenceNTimes(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, 'l', 2);
	v_assert_int(9, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len - res, ==, TBUFFER_LEN(b));
	v_assert_str("Hel", TBUFFER_GET(b));

	teardown();
	VTS;
}

static void test_07_string_rewindnchr_NotFound(void)
{
	int res;
	setup();

	res = string_rewindnchr(b, '#', 1);
	v_assert_int(-1, ==, res);

	v_assert_size_t(STRING_INIT_SIZE, ==, TBUFFER_MAX(b));
	v_assert_size_t(len, ==, TBUFFER_LEN(b));
	v_assert_str(s, TBUFFER_GET(b));

	teardown();
	VTS;
}

void suite_string_rewindnchr(void)
{
	test_00_string_rewindnchr_FirstChar();
	test_01_string_rewindnchr_EndOfString();
	test_02_string_rewindnchr_LastChar();
	test_03_string_rewindnchr_FirstChar();
	test_04_string_rewindnchr_MultipleOccurrence();
	test_05_string_rewindnchr_MultipleOccurrence0Times();
	test_06_string_rewindnchr_MultipleOccurrenceNTimes();
	test_07_string_rewindnchr_NotFound();

	VSS;
}
#include "header.h"

void test_00_string_set_simple(void)
{
	t_string string;
	char *s = "**********";
	char *s2 = "#*********";

	string_init(&string);
	string_set(&string, 0, 10, '*');
	v_assert_str(s, string.str);
	v_assert_size_t(strlen(s), ==, string.len);

	string_set(&string, 0, 1, '#');
	v_assert_str(s2, string.str);
	v_assert_size_t(strlen(s), ==, string.len);

	free(string.str);
	VTS;
}

void test_01_string_set_fromSimple(void)
{
	t_string string;
	char *s = "**********";
	char *s2 = "***######*";

	string_init(&string);
	string_set(&string, 0, 10, '*');
	v_assert_str(s, string.str);
	v_assert_size_t(strlen(s), ==, string.len);

	string_set(&string, 3, 6, '#');
	v_assert_str(s2, string.str);
	v_assert_size_t(strlen(s), ==, string.len);

	free(string.str);
	VTS;
}

void test_02_string_set_ofLenZero(void)
{
	t_string string;
	char *s = "**********";
	char *s2 = "**********";

	string_init(&string);
	string_set(&string, 0, 10, '*');
	v_assert_str(s, string.str);
	v_assert_size_t(strlen(s), ==, string.len);

	string_set(&string, 3, 0, '#');
	v_assert_str(s2, string.str);
	v_assert_size_t(strlen(s), ==, string.len);

	free(string.str);
	VTS;
}

void suite_string_set(void)
{
	test_00_string_set_simple();
	test_01_string_set_fromSimple();
	test_02_string_set_ofLenZero();

	VSS;
}
#include "header.h"

static t_string string;

static void teardown(void) { string_shutdown(&string); }

static void test_00_string_reserve_DontNeedShrinking(void)
{
	char *before_shrink;

	string_init_with_capacity(&string, 4);
	before_shrink = string.str;
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);

	string_shrink_to_fit(&string);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_ptr(before_shrink, ==, string.str);

	teardown();
	VTS;
}

static void test_01_string_reserve_NeedShrinking(void)
{
	string_init_with_capacity(&string, 255);
	v_assert_size_t(256, ==, string.capacity);

	string_shrink_to_fit(&string);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);

	teardown();
	VTS;
}

void suite_string_shrink_to_fit(void)
{
	test_00_string_reserve_DontNeedShrinking();
	test_01_string_reserve_NeedShrinking();

	VSS;
}
#include "header.h"

static t_string string;

static void test_00_string_truncate_SimpleSize(void)
{
	t_string *res;
	string_init_dup(&string, "Hello World!");

	res = string_truncate(&string, 9);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(9, ==, string.len);
	v_assert_str("Hello Wor", string.str);
	v_assert_ptr(NULL, !=, res);

	string_shutdown(&string);
	VTS;
}

static void test_01_string_truncate_ZeroSize(void)
{
	t_string *res;
	string_init_dup(&string, "Hello World!");

	res = string_truncate(&string, string.len);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(12, ==, string.len);
	v_assert_str("Hello World!", string.str);
	v_assert_ptr(NULL, !=, res);

	string_shutdown(&string);
	VTS;
}

static void test_02_string_truncate_FullSize(void)
{
	t_string *res;
	string_init_dup(&string, "Hello World!");

	res = string_truncate(&string, 0);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(0, ==, string.len);
	v_assert_str("", string.str);
	v_assert_ptr(NULL, !=, res);

	string_shutdown(&string);
	VTS;
}

static void test_03_string_truncate_SizeOverflow(void)
{
	t_string *res;
	string_init_dup(&string, "Hello World!");

	res = string_truncate(&string, 42);
	v_assert_size_t(STRING_INIT_SIZE, ==, string.capacity);
	v_assert_size_t(12, ==, string.len);
	v_assert_str("Hello World!", string.str);
	v_assert_ptr(NULL, ==, res);

	string_shutdown(&string);
	VTS;
}

void suite_string_truncate(void)
{
	test_00_string_truncate_SimpleSize();
	test_01_string_truncate_ZeroSize();
	test_02_string_truncate_FullSize();
	test_03_string_truncate_SizeOverflow();

	VSS;
}
