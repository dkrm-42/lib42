#include "ft_util.h"
#include "narwhal.h"

TEST(util_next_power_of_2_test_00_NotAPowerOf2)
{
	size_t ret;

	ret = next_power_of_2(7);
	ASSERT_EQ(ret, 8);

	ret = next_power_of_2(51);
	ASSERT_EQ(ret, 64);

	ret = next_power_of_2(1234);
	ASSERT_EQ(ret, 2048);
}

TEST(util_next_power_of_2_test_01_APowerOf2)
{
	size_t ret;

	ret = next_power_of_2(8);
	ASSERT_EQ(ret, 16);

	ret = next_power_of_2(128);
	ASSERT_EQ(ret, 256);
}

TEST(util_next_power_of_2_test_02_Zero)
{
	size_t ret;

	ret = next_power_of_2(0);
	ASSERT_EQ(ret, 1);
}

TEST(util_ft_strchrpos_test_00_SimpleStrings)
{
	int ret;

	ret = ft_strchrpos("Hello World!", 'H');
	ASSERT_EQ(ret, 0);

	ret = ft_strchrpos("Hello World!", '!');
	ASSERT_EQ(ret, 11);
}

TEST(util_ft_strchrpos_test_01_CharNotFound)
{
	int ret;

	ret = ft_strchrpos("Hello World!", '#');
	ASSERT_EQ(ret, -1);
}

TEST(util_ft_strchrpos_test_02_TwoOccurrence)
{
	int ret;

	ret = ft_strchrpos("Hello World!", 'o');
	ASSERT_EQ(ret, 4);
}

TEST(util_ft_strchrpos_test_03_EndOfString)
{
	int ret;

	ret = ft_strchrpos("Hello World!", 0);
	ASSERT_EQ(ret, 12);
}

TEST(util_ft_strrchrpos_test_00_SimpleStrings)
{
	int ret;

	ret = ft_strrchrpos("Hello World!", 'H');
	ASSERT_EQ(ret, 0);

	ret = ft_strrchrpos("Hello World!", '!');
	ASSERT_EQ(ret, 11);

	ret = ft_strrchrpos("Hello World!", 'd');
	ASSERT_EQ(ret, 10);
}

TEST(util_ft_strrchrpos_test_01_strrchrpos_CharNotFound)
{
	int ret;

	ret = ft_strrchrpos("Hello World!", '#');
	ASSERT_EQ(ret, -1);
}

TEST(util_ft_strrchrpos_test_02_TwoOccurrence)
{
	int ret;

	ret = ft_strrchrpos("Hello World!", 'o');
	ASSERT_EQ(ret, 7);
}

TEST(util_ft_strrchrpos_test_03_EndOfString)
{
	int ret;

	ret = ft_strrchrpos("Hello World!", 0);
	ASSERT_EQ(ret, 12);
}

TEST(util_ft_strrev_test_01_SimpleReverse)
{
	char deep[] = "Are you feeling the true reality of the Truth...";
	char hw[] = "HelloWorld!";
	char nb[] = "0123456789";
	char onlyA[] = "A";

	ASSERT_SUBSTRING(ft_strrev(deep),
			 "...hturT eht fo ytilaer eurt eht gnileef uoy erA");
	ASSERT_SUBSTRING(ft_strrev(hw), "!dlroWolleH");
	ASSERT_SUBSTRING(ft_strrev(nb), "9876543210");
	ASSERT_SUBSTRING(ft_strrev(onlyA), "A");
}

TEST(util_ft_abs_test_00)
{
	ASSERT_EQ(FT_ABS(0), 0U);
	ASSERT_EQ(FT_ABS(1), 1U);
	ASSERT_EQ(FT_ABS(-1), 1U);
	ASSERT_EQ(FT_ABS(INT32_MAX), (unsigned int)INT32_MAX);
}

TEST(util_round_up_8_test_00)
{
	size_t res;

	res = ROUND_UP_8(0U);
	ASSERT_EQ(res, 8);

	res = ROUND_UP_8(1U);
	ASSERT_EQ(res, 8);

	res = ROUND_UP_8(7U);
	ASSERT_EQ(res, 8);

	res = ROUND_UP_8(8U);
	ASSERT_EQ(res, 8);

	res = ROUND_UP_8(13U);
	ASSERT_EQ(res, 16);

	res = ROUND_UP_8(15U);
	ASSERT_EQ(res, 16);

	res = ROUND_UP_8(16U);
	ASSERT_EQ(res, 16);

	res = ROUND_UP_8(21U);
	ASSERT_EQ(res, 24);
}
