static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&v, 8);
	v.data[0] = str[0];
	v.data[1] = str[1];
	v.data[2] = str[2];
	v.data[3] = str[3];
	v.data[4] = str[4];
	v.count = 5;
}

static void teardown(void)
{
	free(v.data);
	memset(&v, 0, sizeof(t_array));
}

void test_00_array_add_SimpleAdd(void)
{
	char *s1 = "firstadd";

	setup();

	array_add(&v, s1);

	v_assert_size_t(6, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(s1, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));
	v_assert_str(s1, array_get(&v, 5));

	teardown();
	VTS;
}

void test_01_array_add_ResizeAdd(void)
{
	char *s1 = "firstadd";
	char *s2 = "secondadd";
	char *s3 = "tertio";

	setup();

	array_add(&v, s1);
	array_add(&v, s2);
	array_add(&v, s3);

	v_assert_size_t(8, ==, v.count);
	v_assert_size_t(16, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(s1, ==, array_get(&v, 5));
	v_assert_ptr(s2, ==, array_get(&v, 6));
	v_assert_ptr(s3, ==, array_get(&v, 7));
	v_assert_ptr(NULL, ==, array_get(&v, 8));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));
	v_assert_str(s1, array_get(&v, 5));
	v_assert_str(s2, array_get(&v, 6));
	v_assert_str(s3, array_get(&v, 7));

	teardown();
	VTS;
}

void suite_array_add(void)
{
	test_00_array_add_SimpleAdd();
	test_01_array_add_ResizeAdd();

	VSS;
}
#include "header.h"

static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning", "string", "padding",
};

static void setup(void)
{
	array_init(&v, 8);
	/* printf("ARR_SIZ_MAX: %zu\n", ARR_SIZ_MAX(str)); */
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i)
		array_add(&v, strdup(str[i]));
}

void test_00_array_clear_Simple(void)
{
	setup();

	array_clear(&v);

	v_assert_size_t(8, ==, v.max);
	v_assert_size_t(0, ==, v.count);
	v_assert_size_t(0, ==, v.iterator);
	v_assert_ptr(NULL, ==, v.data[0]);
	v_assert_ptr(NULL, ==, v.data[1]);
	v_assert_ptr(NULL, ==, v.data[2]);
	v_assert_ptr(NULL, ==, v.data[3]);
	v_assert_ptr(NULL, ==, v.data[4]);
	v_assert_ptr(NULL, ==, v.data[5]);
	v_assert_ptr(NULL, ==, v.data[6]);
	v_assert_ptr(NULL, ==, v.data[7]);

	VTS;
}

void suite_array_clear(void)
{
	test_00_array_clear_Simple();

	VSS;
}

#include "header.h"

void test_00_array_copy_SimpleCopy(void)
{
	char *s1 = "hello";
	char *s2 = "world";
	char *s3 = "and";
	char *s4 = "good";
	char *s5 = "morning";
	t_array v;
	t_array *cp;

	array_init(&v, 8);
	array_add(&v, s1);
	array_add(&v, s2);
	array_add(&v, s3);
	array_add(&v, s4);
	array_add(&v, s5);

	cp = array_copy(&v);

	v.data[0] = NULL;
	v.count = 0;

	v_assert_ptr(NULL, !=, cp);
	v_assert_size_t(8, ==, cp->max);
	v_assert_size_t(5, ==, cp->count);
	v_assert_size_t(0, ==, cp->iterator);
	v_assert_str(s1, array_get(cp, 0));
	v_assert_ptr(s1, ==, array_get(cp, 0));
	v_assert_str(s2, array_get(cp, 1));
	v_assert_ptr(s2, ==, array_get(cp, 1));
	v_assert_str(s3, array_get(cp, 2));
	v_assert_ptr(s3, ==, array_get(cp, 2));
	v_assert_str(s4, array_get(cp, 3));
	v_assert_ptr(s4, ==, array_get(cp, 3));
	v_assert_str(s5, array_get(cp, 4));
	v_assert_ptr(s5, ==, array_get(cp, 4));
	v_assert_ptr(NULL, ==, array_get(cp, 5));
	v_assert_ptr(NULL, ==, array_get(cp, 6));
	v_assert_ptr(NULL, ==, array_get(cp, 7));

	free(v.data);
	free(cp->data);
	free(cp);
	VTS;
}

void suite_array_copy(void)
{
	test_00_array_copy_SimpleCopy();

	VSS;
}
#include "header.h"

static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&v, 8);
	v.data[0] = str[0];
	v.data[1] = str[1];
	v.data[2] = str[2];
	v.data[3] = str[3];
	v.data[4] = str[4];
	v.count = 5;
}

static void teardown(void)
{
	free(v.data);
	memset(&v, 0, sizeof(t_array));
}

void test_00_array_indexof_First(void)
{
	int index;

	setup();

	index = array_indexof(&v, str[0]);
	v_assert_int(0, ==, index);
	v_assert_ptr(str[0], ==, v.data[index]);
	v_assert_str(str[0], v.data[index]);

	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);
	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));

	teardown();
	VTS;
}

void test_01_array_indexof_Last(void)
{
	int index;

	setup();

	index = array_indexof(&v, str[4]);
	v_assert_int(4, ==, index);
	v_assert_ptr(str[4], ==, v.data[index]);
	v_assert_str(str[4], v.data[index]);

	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);
	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));

	teardown();
	VTS;
}

void test_02_array_indexof_DoesntExist(void)
{
	int index;

	setup();

	index = array_indexof(&v, "zut");
	v_assert_int(-1, ==, index);

	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);
	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));

	teardown();
	VTS;
}

void suite_array_indexof(void)
{
	test_00_array_indexof_First();
	test_01_array_indexof_Last();
	test_02_array_indexof_DoesntExist();

	VSS;
}
#include "header.h"

static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&v, 8);
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i)
		array_add(&v, str[i]);
}

static void teardown(void)
{
	free(v.data);
	memset(&v, 0, sizeof(t_array));
}

void test_00_array_insert_FirstPlace(void)
{
	char *s1 = "zut";
	setup();

	array_insert(&v, 0, s1);

	// Check array integrity
	v_assert_size_t(6, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(s1, ==, array_get(&v, 0));
	v_assert_ptr(str[0], ==, array_get(&v, 1));
	v_assert_ptr(str[1], ==, array_get(&v, 2));
	v_assert_ptr(str[2], ==, array_get(&v, 3));
	v_assert_ptr(str[3], ==, array_get(&v, 4));
	v_assert_ptr(str[4], ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(s1, array_get(&v, 0));
	v_assert_str(str[0], array_get(&v, 1));
	v_assert_str(str[1], array_get(&v, 2));
	v_assert_str(str[2], array_get(&v, 3));
	v_assert_str(str[3], array_get(&v, 4));
	v_assert_str(str[4], array_get(&v, 5));

	teardown();
	VTS;
}

void test_01_array_insert_LastPlace(void)
{
	char *s1 = "zut";
	setup();

	array_insert(&v, 4, s1);

	// Check array integrity
	v_assert_size_t(6, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(s1, ==, array_get(&v, 4));
	v_assert_ptr(str[4], ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(s1, array_get(&v, 4));
	v_assert_str(str[4], array_get(&v, 5));

	teardown();
	VTS;
}

void test_02_array_insert_MiddlePlace(void)
{
	char *s1 = "zut";
	setup();

	array_insert(&v, 2, s1);

	// Check array integrity
	v_assert_size_t(6, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(s1, ==, array_get(&v, 2));
	v_assert_ptr(str[2], ==, array_get(&v, 3));
	v_assert_ptr(str[3], ==, array_get(&v, 4));
	v_assert_ptr(str[4], ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(s1, array_get(&v, 2));
	v_assert_str(str[2], array_get(&v, 3));
	v_assert_str(str[3], array_get(&v, 4));
	v_assert_str(str[4], array_get(&v, 5));

	teardown();
	VTS;
}

void test_03_array_insert_Resize(void)
{
	char *s1 = "zut1";
	char *s2 = "zut2";
	setup();

	array_insert(&v, v.count, s1);
	array_insert(&v, v.count, s2);

	// Check array integrity
	v_assert_size_t(7, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(s1, ==, array_get(&v, 5));
	v_assert_ptr(s2, ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));
	v_assert_ptr(NULL, ==, array_get(&v, 8));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));
	v_assert_str(s1, array_get(&v, 5));
	v_assert_str(s2, array_get(&v, 6));

	teardown();
	VTS;
}

void suite_array_insert(void)
{
	test_00_array_insert_FirstPlace();
	test_01_array_insert_LastPlace();
	test_02_array_insert_MiddlePlace();
	test_03_array_insert_Resize();

	VSS;
}
#include "header.h"

static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&v, 8);
	array_add(&v, str[0]);
	array_add(&v, str[1]);
	array_add(&v, str[2]);
	array_add(&v, str[3]);
	array_add(&v, str[4]);
}

static void teardown(void)
{
	free(v.data);
	memset(&v, 0, sizeof(t_array));
}

void test_00_array_iterator_ToTheEnd(void)
{
	void *p;

	setup();

	for (size_t i = 0; (p = array_iterator(&v)) != NULL; ++i) {
		v_assert_ptr(NULL, !=, p);
		v_assert_ptr(str[i], ==, p);
		v_assert_str(str[i], p);
		v_assert_size_t(i + 1, ==, v.iterator);
	}
	v_assert_ptr(NULL, ==, p);
	v_assert_size_t(0, ==, v.iterator);

	free(v.data);
	VTS;
}

void test_01_array_iterator_Interrupted(void)
{
	void *p;

	setup();

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[0], ==, p);
	v_assert_str(str[0], p);
	v_assert_size_t(1, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[1], ==, p);
	v_assert_str(str[1], p);
	v_assert_size_t(2, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[2], ==, p);
	v_assert_str(str[2], p);
	v_assert_size_t(3, ==, v.iterator);

	TARRAY_RESET_ITER(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[2], ==, p);
	v_assert_size_t(0, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[0], ==, p);
	v_assert_str(str[0], p);
	v_assert_size_t(1, ==, v.iterator);

	free(v.data);
	VTS;
}

void test_02_array_iterator_RemoveElementBefore(void)
{
	void *p;

	setup();

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[0], ==, p);
	v_assert_str(str[0], p);
	v_assert_size_t(1, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[1], ==, p);
	v_assert_str(str[1], p);
	v_assert_size_t(2, ==, v.iterator);

	array_remove(&v, 0);
	v_assert_size_t(1, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[2], ==, p);
	v_assert_str(str[2], p);
	v_assert_size_t(2, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[3], ==, p);
	v_assert_str(str[3], p);
	v_assert_size_t(3, ==, v.iterator);

	free(v.data);
	VTS;
}

void test_03_array_iterator_RemoveElementAfter(void)
{
	void *p;

	setup();

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[0], ==, p);
	v_assert_str(str[0], p);
	v_assert_size_t(1, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[1], ==, p);
	v_assert_str(str[1], p);
	v_assert_size_t(2, ==, v.iterator);

	array_remove(&v, 4);
	v_assert_size_t(2, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[2], ==, p);
	v_assert_str(str[2], p);
	v_assert_size_t(3, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[3], ==, p);
	v_assert_str(str[3], p);
	v_assert_size_t(4, ==, v.iterator);

	free(v.data);
	VTS;
}

void test_04_array_iterator_RemoveElementOnIt(void)
{
	void *p;

	setup();

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[0], ==, p);
	v_assert_str(str[0], p);
	v_assert_size_t(1, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[1], ==, p);
	v_assert_str(str[1], p);
	v_assert_size_t(2, ==, v.iterator);

	array_remove(&v, 2);
	v_assert_size_t(2, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[3], ==, p);
	v_assert_str(str[3], p);
	v_assert_size_t(3, ==, v.iterator);

	p = array_iterator(&v);
	v_assert_ptr(NULL, !=, p);
	v_assert_ptr(str[4], ==, p);
	v_assert_str(str[4], p);
	v_assert_size_t(4, ==, v.iterator);

	free(v.data);
	VTS;
}

void suite_array_iterator(void)
{
	test_00_array_iterator_ToTheEnd();
	test_01_array_iterator_Interrupted();
	test_02_array_iterator_RemoveElementBefore();
	test_03_array_iterator_RemoveElementAfter();
	test_04_array_iterator_RemoveElementOnIt();

	VSS;
}
#include "header.h"

static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning", "string", "padding", "enable",
};

static void setup(void)
{
	array_init(&v, 8);
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i)
		array_add(&v, str[i]);
}

static void teardown(void)
{
	free(v.data);
	memset(&v, 0, sizeof(t_array));
}

void test_00_array_remove_FirstItem(void)
{
	char *ptr;
	size_t ind = 0;
	setup();

	ptr = array_remove(&v, ind);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_str(str[ind], ptr);
	v_assert_ptr(str[ind], ==, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, v.count);
	v_assert_size_t(16, ==, v.max);

	v_assert_ptr(str[1], ==, array_get(&v, 0));
	v_assert_ptr(str[2], ==, array_get(&v, 1));
	v_assert_ptr(str[3], ==, array_get(&v, 2));
	v_assert_ptr(str[4], ==, array_get(&v, 3));
	v_assert_ptr(str[5], ==, array_get(&v, 4));
	v_assert_ptr(str[6], ==, array_get(&v, 5));
	v_assert_ptr(str[7], ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));
	v_assert_ptr(NULL, ==, array_get(&v, 8));

	v_assert_str(str[1], array_get(&v, 0));
	v_assert_str(str[2], array_get(&v, 1));
	v_assert_str(str[3], array_get(&v, 2));
	v_assert_str(str[4], array_get(&v, 3));
	v_assert_str(str[5], array_get(&v, 4));
	v_assert_str(str[6], array_get(&v, 5));
	v_assert_str(str[7], array_get(&v, 6));

	teardown();
	VTS;
}

void test_01_array_remove_MiddleItem(void)
{
	char *ptr;
	size_t ind = 3;
	setup();

	ptr = array_remove(&v, ind);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_ptr(str[ind], ==, ptr);
	v_assert_str(str[ind], ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, v.count);
	v_assert_size_t(16, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[4], ==, array_get(&v, 3));
	v_assert_ptr(str[5], ==, array_get(&v, 4));
	v_assert_ptr(str[6], ==, array_get(&v, 5));
	v_assert_ptr(str[7], ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));
	v_assert_ptr(NULL, ==, array_get(&v, 8));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[4], array_get(&v, 3));
	v_assert_str(str[5], array_get(&v, 4));
	v_assert_str(str[6], array_get(&v, 5));
	v_assert_str(str[7], array_get(&v, 6));

	teardown();
	VTS;
}

void test_02_array_remove_LastItem(void)
{
	char *ptr;
	size_t ind = 7;
	setup();

	ptr = array_remove(&v, ind);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_ptr(str[ind], ==, ptr);
	v_assert_str(str[ind], ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, v.count);
	v_assert_size_t(16, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(str[5], ==, array_get(&v, 5));
	v_assert_ptr(str[6], ==, array_get(&v, 6));
	v_assert_ptr(NULL, ==, array_get(&v, 7));
	v_assert_ptr(NULL, ==, array_get(&v, 8));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));
	v_assert_str(str[5], array_get(&v, 5));
	v_assert_str(str[6], array_get(&v, 6));

	teardown();
	VTS;
}

void test_03_array_remove_OutOfRange(void)
{
	char *ptr;
	size_t ind = 8;
	setup();

	ptr = array_remove(&v, ind);

	// Check return value
	v_assert_ptr(NULL, ==, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str), ==, v.count);
	v_assert_size_t(16, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(str[5], ==, array_get(&v, 5));
	v_assert_ptr(str[6], ==, array_get(&v, 6));
	v_assert_ptr(str[7], ==, array_get(&v, 7));
	v_assert_ptr(NULL, ==, array_get(&v, 8));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));
	v_assert_str(str[5], array_get(&v, 5));
	v_assert_str(str[6], array_get(&v, 6));
	v_assert_str(str[7], array_get(&v, 7));

	teardown();
	VTS;
}

void suite_array_remove(void)
{
	test_00_array_remove_FirstItem();
	test_01_array_remove_MiddleItem();
	test_02_array_remove_LastItem();
	test_03_array_remove_OutOfRange();

	VSS;
}
#include "header.h"

static t_array v;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&v, 8);
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i)
		array_add(&v, str[i]);
}

static void teardown(void)
{
	free(v.data);
	memset(&v, 0, sizeof(t_array));
}

void test_00_array_replace_FirstItem(void)
{
	char *rep = "firstitem";
	char *ptr;
	size_t ind = 0;
	setup();

	ptr = array_replace(&v, ind, rep);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_str(str[ind], ptr);
	v_assert_ptr(str[ind], ==, ptr);

	// Check array integrity
	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(rep, ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));

	v_assert_str(rep, array_get(&v, ind));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));

	teardown();
	VTS;
}

void test_01_array_replace_MiddleItem(void)
{
	char *rep = "middleitem";
	char *ptr;
	size_t ind = 2;
	setup();

	ptr = array_replace(&v, ind, rep);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_str(str[ind], ptr);
	v_assert_ptr(str[ind], ==, ptr);

	// Check array integrity
	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(rep, ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(rep, array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));

	teardown();
	VTS;
}

void test_02_array_replace_LastItem(void)
{
	char *rep = "lastitem";
	char *ptr;
	size_t ind = 4;
	setup();

	ptr = array_replace(&v, ind, rep);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_str(str[ind], ptr);
	v_assert_ptr(str[ind], ==, ptr);

	// Check array integrity
	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(rep, ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(rep, array_get(&v, 4));

	teardown();
	VTS;
}

void test_03_array_replace_OutOfRange(void)
{
	char *rep = "outofrange";
	char *ptr;
	size_t ind = 5;
	setup();

	ptr = array_replace(&v, ind, rep);

	// Check return value
	v_assert_ptr(NULL, ==, ptr);

	// Check array integrity
	v_assert_size_t(5, ==, v.count);
	v_assert_size_t(8, ==, v.max);

	v_assert_ptr(str[0], ==, array_get(&v, 0));
	v_assert_ptr(str[1], ==, array_get(&v, 1));
	v_assert_ptr(str[2], ==, array_get(&v, 2));
	v_assert_ptr(str[3], ==, array_get(&v, 3));
	v_assert_ptr(str[4], ==, array_get(&v, 4));
	v_assert_ptr(NULL, ==, array_get(&v, 5));
	v_assert_ptr(NULL, ==, array_get(&v, 6));

	v_assert_str(str[0], array_get(&v, 0));
	v_assert_str(str[1], array_get(&v, 1));
	v_assert_str(str[2], array_get(&v, 2));
	v_assert_str(str[3], array_get(&v, 3));
	v_assert_str(str[4], array_get(&v, 4));

	teardown();
	VTS;
}

void suite_array_replace(void)
{
	test_00_array_replace_FirstItem();
	test_01_array_replace_MiddleItem();
	test_02_array_replace_LastItem();
	test_03_array_replace_OutOfRange();

	VSS;
}
#include "header.h"

static t_array *v;

static void teardown(void)
{
	array_clear(v);
	array_destroy(v);
	v = NULL;
}

void test_00_array_strsplit_3ElemsSepColon(void)
{
	char *str = "elem1:elem2:elem3";

	v = array_strsplit(str, ':');

	v_assert_ptr(NULL, !=, v);
	v_assert_size_t(8, ==, v->max);
	v_assert_size_t(3, ==, v->count);
	v_assert_str("elem1", array_get(v, 0));
	v_assert_str("elem2", array_get(v, 1));
	v_assert_str("elem3", array_get(v, 2));
	v_assert_ptr(NULL, ==, array_get(v, 3));

	teardown();
	VTS;
}

void test_01_array_strsplit_1ElemSepEndOfString(void)
{
	char *str = "elem1";

	v = array_strsplit(str, '\0');

	v_assert_ptr(NULL, !=, v);
	v_assert_size_t(8, ==, v->max);
	v_assert_size_t(1, ==, v->count);
	v_assert_str("elem1", array_get(v, 0));
	v_assert_ptr(NULL, ==, array_get(v, 1));

	teardown();
	VTS;
}

void test_02_array_strsplit_EmptyStringSepEndOfString(void)
{
	char *str = "";

	v = array_strsplit(str, '\0');

	v_assert_ptr(NULL, !=, v);
	v_assert_size_t(8, ==, v->max);
	v_assert_size_t(1, ==, v->count);
	v_assert_str("", array_get(v, 0));
	v_assert_ptr(NULL, ==, array_get(v, 1));

	teardown();
	VTS;
}

void test_03_array_strsplit_8ElemsSepSpace(void)
{
	char *str = "Hello world and good morning on this day!";

	v = array_strsplit(str, ' ');

	v_assert_ptr(NULL, !=, v);
	v_assert_size_t(16, ==, v->max);
	v_assert_size_t(8, ==, v->count);
	v_assert_str("Hello", array_get(v, 0));
	v_assert_str("world", array_get(v, 1));
	v_assert_str("and", array_get(v, 2));
	v_assert_str("good", array_get(v, 3));
	v_assert_str("morning", array_get(v, 4));
	v_assert_str("on", array_get(v, 5));
	v_assert_str("this", array_get(v, 6));
	v_assert_str("day!", array_get(v, 7));
	v_assert_ptr(NULL, ==, array_get(v, 8));

	teardown();
	VTS;
}

void test_04_array_strsplit_OnlySep(void)
{
	char *str = "///";

	v = array_strsplit(str, '/');

	v_assert_ptr(NULL, !=, v);
	v_assert_size_t(8, ==, v->max);
	v_assert_size_t(4, ==, v->count);
	v_assert_str("", array_get(v, 0));
	v_assert_str("", array_get(v, 1));
	v_assert_str("", array_get(v, 2));
	v_assert_str("", array_get(v, 3));
	v_assert_ptr(NULL, ==, array_get(v, 4));

	teardown();
	VTS;
}

void test_05_array_strsplit_SimpleStringSepSlash(void)
{
	char *str = "a/b//c";

	v = array_strsplit(str, '/');

	v_assert_ptr(NULL, !=, v);
	v_assert_size_t(8, ==, v->max);
	v_assert_size_t(4, ==, v->count);
	v_assert_str("a", array_get(v, 0));
	v_assert_str("b", array_get(v, 1));
	v_assert_str("", array_get(v, 2));
	v_assert_str("c", array_get(v, 3));
	v_assert_ptr(NULL, ==, array_get(v, 4));

	teardown();
	VTS;
}

void suite_array_strsplit(void)
{
	test_00_array_strsplit_3ElemsSepColon();
	test_01_array_strsplit_1ElemSepEndOfString();
	test_02_array_strsplit_EmptyStringSepEndOfString();
	test_03_array_strsplit_8ElemsSepSpace();
	test_04_array_strsplit_OnlySep();
	test_05_array_strsplit_SimpleStringSepSlash();

	VSS;
}
#include "header.h"

static t_array array;

static void setup(void *data, size_t elem_size, size_t len)
{
	unsigned char *p;
	unsigned char *d;

	array_init(&array, elem_size);
	if (data != NULL) {
		p = array.data;
		d = data;
		for (size_t i = 0; i < len; ++i)
			memcpy(p + (elem_size * i), d + (elem_size * i),
			       elem_size);
		array.len = len;
	}
}

static void teardown(void) { free(array.data); }

static void test_00_array_at_Int(void)
{
	int data[4] = {11, 22, 33, 44};
	setup(data, sizeof(int), ARR_SIZ_MAX(data));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		int *value = array_get_at(&array, i);
		v_assert_int(data[i], ==, *value);
	}

	// set
	int new = 8;
	int *value = array_get_at(&array, 3);
	array_set_at(&array, 3, &new);
	v_assert_int(8, ==, *value);

	teardown();
	VTS;
}

static void test_01_array_at_String(void)
{
	char *data[] = {
	    "hello", "world", "and", "good", "morning",
	};
	setup(data, sizeof(char *), ARR_SIZ_MAX(data));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		char **value = array_get_at(&array, i);
		v_assert_str(data[i], *value);
	}

	// set
	char *s = "zut";
	char **value = array_get_at(&array, 2);
	array_set_at(&array, 2, &s);
	v_assert_ptr(s, ==, *value);
	v_assert_str(s, *value);

	teardown();
	VTS;
}

static void test_02_array_at_Struct(void)
{
	struct s_test {
		void *e;
		int i;
		char c;
		char padding[3];
	} data[3] = {
	    {NULL, 42, 'a', {0}},
	    {(void *)0xdeadbeef, 8, 'z', {0}},
	    {(void *)0xabcdef, -1, '*', {0}},
	};
	setup(data, sizeof(struct s_test), ARR_SIZ_MAX(data));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		struct s_test value = *(struct s_test *)array_get_at(&array, i);
		v_assert_ptr(data[i].e, ==, value.e);
		v_assert_int(data[i].i, ==, value.i);
		v_assert_char(data[i].c, ==, value.c);
	}

	// set
	struct s_test new = {(void *)0x1234, 1 << 31, 'X', {0}};
	array_set_at(&array, 0, &new);
	struct s_test value = *(struct s_test *)array_get_at(&array, 0);
	v_assert_ptr(new.e, ==, value.e);
	v_assert_int(new.i, ==, value.i);
	v_assert_char(new.c, ==, value.c);

	teardown();
	VTS;
}

static void test_03_array_at_OutOfRange(void)
{
	int data[4] = {11, 22, 33, 44};
	setup(data, sizeof(int), ARR_SIZ_MAX(data));

	v_assert_ptr(NULL, ==, array_get_at(&array, 4));

	teardown();
	VTS;
}

static void test_04_array_get_last_Int(void)
{
	int data[4] = {1, 2, 3, 4};
	setup(data, sizeof(int), ARR_SIZ_MAX(data));

	int *value = array_get_last(&array);
	v_assert_int(data[3], ==, *value);

	array.len -= 1;

	value = array_get_last(&array);
	v_assert_int(data[2], ==, *value);

	array.len -= 1;

	value = array_get_last(&array);
	v_assert_int(data[1], ==, *value);

	array.len -= 1;

	value = array_get_last(&array);
	v_assert_int(data[0], ==, *value);

	array.len -= 1;

	int *empty = array_get_last(&array);
	v_assert_size_t(0, ==, array.len);
	v_assert_ptr(NULL, ==, empty);

	teardown();
	VTS;
}

static void test_05_array_get_first_Int(void)
{
	int data[4] = {1, 2, 3, 4};
	setup(data, sizeof(int), ARR_SIZ_MAX(data));

	int *value = array_get_first(&array);
	v_assert_int(data[0], ==, *value);

	array.len = 0;

	int *empty = array_get_first(&array);
	v_assert_size_t(0, ==, array.len);
	v_assert_ptr(NULL, ==, empty);

	teardown();
	VTS;
}

void suite_array_at(void)
{
	test_00_array_at_Int();
	test_01_array_at_String();
	test_02_array_at_Struct();
	test_03_array_at_OutOfRange();
	test_04_array_get_last_Int();
	test_05_array_get_first_Int();

	VSS;
}
#include "header.h"

static void test_00_array_clone_Simple(void)
{
	t_array v;
	t_array cp;
	char *s1 = "hello";
	char *s2 = "world";
	char *s3 = "and";
	char *s4 = "good";
	char *s5 = "morning";

	array_init(&v, sizeof(char *));
	array_push(&v, &s1);
	array_push(&v, &s2);
	array_push(&v, &s3);
	array_push(&v, &s4);
	array_push(&v, &s5);

	array_clone(&cp, &v);

	v_assert_size_t(ARRAY_INIT_SIZE, ==, cp.capacity);
	v_assert_size_t(5, ==, cp.len);
	v_assert_str(s1, *(char **)array_get_at(&cp, 0));
	v_assert_ptr(s1, ==, *(char **)array_get_at(&cp, 0));
	v_assert_str(s2, *(char **)array_get_at(&cp, 1));
	v_assert_ptr(s2, ==, *(char **)array_get_at(&cp, 1));
	v_assert_str(s3, *(char **)array_get_at(&cp, 2));
	v_assert_ptr(s3, ==, *(char **)array_get_at(&cp, 2));
	v_assert_str(s4, *(char **)array_get_at(&cp, 3));
	v_assert_ptr(s4, ==, *(char **)array_get_at(&cp, 3));
	v_assert_str(s5, *(char **)array_get_at(&cp, 4));
	v_assert_ptr(s5, ==, *(char **)array_get_at(&cp, 4));
	v_assert_ptr(NULL, ==, array_get_at(&cp, 5));
	v_assert_ptr(NULL, ==, array_get_at(&cp, 6));
	v_assert_ptr(NULL, ==, array_get_at(&cp, 7));
	v_assert_ptr(v.data, !=, cp.data);

	array_shutdown(&v);
	array_shutdown(&cp);
	VTS;
}

static void test_01_array_clone_Empty(void)
{
	t_array array;
	t_array clone;

	array_init(&array, sizeof(char));

	v_assert_ptr(NULL, !=, array_clone(&clone, &array));
	v_assert_size_t(array.capacity, ==, clone.capacity);
	v_assert_size_t(array.len, ==, clone.len);
	v_assert_ptr(array.data, !=, clone.data);

	array_shutdown(&array);
	array_shutdown(&clone);

	VTS;
}

void suite_array_clone(void)
{
	test_00_array_clone_Simple();
	test_01_array_clone_Empty();

	VSS;
}
#include "header.h"

#define TEST_ARRAY_INIT_SIZE 8U

static t_array *a;

static void teardown(void)
{
	if (a != NULL) {
		free(a->data);
		free(a);
	}
	a = NULL;
}

// array_create
static void test_00_array_create_ElemSizeZero(void)
{
	a = array_create(0);

	v_assert_ptr(NULL, ==, a);

	teardown();
	VTS;
}

static void test_01_array_create_ElemSizeOne(void)
{
	a = array_create(1);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(1, ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

static void test_02_array_create_ElemSize21(void)
{
	a = array_create(21);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(21, ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

// array_create_with_capacity
static void test_00_array_create_with_capacity_ElemSizeZeroCapacityZero(void)
{
	a = array_create_with_capacity(0, 0);

	v_assert_ptr(NULL, ==, a);

	teardown();
	VTS;
}

static void test_01_array_create_with_capacity_ElemSizeOneCapacityZero(void)
{
	a = array_create_with_capacity(1, 0);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(1, ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

static void test_02_array_create_with_capacity_ElemSize21CapacityZero(void)
{
	a = array_create_with_capacity(21, 0);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(21, ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

static void
test_03_array_create_with_capacity_ElemSizeIntCapacityOverINIT_SIZE(void)
{
	a = array_create_with_capacity(sizeof(int), TEST_ARRAY_INIT_SIZE + 1);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(sizeof(int), ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE * 2, ==, a->capacity);

	teardown();
	VTS;
}

static void
test_04_array_create_with_capacity_ElemSizeIntCapacityUnderINIT_SIZE(void)
{
	a = array_create_with_capacity(sizeof(int), 1);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(sizeof(int), ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

static void
test_05_array_create_with_capacity_ElemSizeIntCapacityEqualINIT_SIZE(void)
{
	a = array_create_with_capacity(sizeof(int), TEST_ARRAY_INIT_SIZE);

	v_assert_ptr(NULL, !=, a);
	v_assert_size_t(sizeof(int), ==, a->elem_size);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

void suite_array_create(void)
{
	test_00_array_create_ElemSizeZero();
	test_01_array_create_ElemSizeOne();
	test_02_array_create_ElemSize21();

	test_00_array_create_with_capacity_ElemSizeZeroCapacityZero();
	test_01_array_create_with_capacity_ElemSizeOneCapacityZero();
	test_02_array_create_with_capacity_ElemSize21CapacityZero();
	test_03_array_create_with_capacity_ElemSizeIntCapacityOverINIT_SIZE();
	test_04_array_create_with_capacity_ElemSizeIntCapacityUnderINIT_SIZE();
	test_05_array_create_with_capacity_ElemSizeIntCapacityEqualINIT_SIZE();

	VSS;
}
#include "header.h"

static t_array *a;

static void teardown(void)
{
	if (a != NULL) {
		free(a->data);
		free(a);
	}
	a = NULL;
}

static int is_less_than(const void *n, const void *param)
{
	return (*(const int *)n < *(const int *)param);
}

static void test_00_array_find_from_simple(void)
{
	t_array array;
	int data[3][3] = {
	    {1, 4, -5},
	    {1, -5, 4},
	    {-5, 1, 4},
	};
	int zero = 0;

	for (int i = 0; i < 3; i++) {
		array_init(&array, sizeof(int));
		for (int j = 0; j < 3; j++)
			array_push(&array, &(data[i][j]));
		int ret =
		    *(int *)array_find_from(array, 0, &is_less_than, &zero);
		v_assert_int(-5, ==, ret);
		array_shutdown(&array);
	}
	VTS;
}

static void test_01_array_find_from_empty(void)
{
	t_array array;
	int zero = 0;

	array_init(&array, sizeof(int));
	void *ret = array_find_from(array, 0, &is_less_than, &zero);
	v_assert_ptr(NULL, ==, ret);
	array_shutdown(&array);
	VTS;
}

static void test_02_array_find_from_no_matches(void)
{
	t_array array;
	int zero = 0;
	int ref = 1;

	array_init(&array, sizeof(int));
	array_push(&array, &ref);
	void *ret = array_find_from(array, 0, &is_less_than, &zero);
	v_assert_ptr(NULL, ==, ret);
	array_shutdown(&array);
	VTS;
}

static void test_03_array_find_from_n(void)
{
	t_array array;
	int zero = 0;
	int ref[] = {-5, -42};

	array_init(&array, sizeof(int));
	array_push(&array, ref);
	array_push(&array, ref + 1);
	void *ret = array_find_from(array, 1, &is_less_than, &zero);
	v_assert_int(-42, ==, *(int *)ret);
	array_shutdown(&array);
	VTS;
}

static void test_04_array_find_from_param(void)
{
	t_array array;
	int four = 4;
	int five = 5;
	int six = 6;

	array_init(&array, sizeof(int));
	array_push(&array, &five);
	void *ret1 = array_find_from(array, 0, &is_less_than, &four);
	void *ret2 = array_find_from(array, 0, &is_less_than, &six);
	v_assert_ptr(ret1, !=, ret2);
	array_shutdown(&array);
	VTS;
}

void suite_array_find_from(void)
{
	test_00_array_find_from_simple();
	test_01_array_find_from_empty();
	test_02_array_find_from_no_matches();
	test_03_array_find_from_n();
	test_04_array_find_from_param();
	VSS;
}
#include "header.h"

static void test_00_array_get_available_Simple(void)
{
	t_array array;
	int *avlbl;
	int value = ~0;

	array_init(&array, sizeof(int));

	array_push(&array, &value);
	array_push(&array, &value);
	v_assert_size_t(2, ==, array.len);
	v_assert_int(value, ==, *(int *)array_get_at(&array, 0));
	v_assert_int(value, ==, *(int *)array_get_at(&array, 1));
	array_pop(&array, NULL);
	array_pop(&array, NULL);

	avlbl = array_get_available(&array);
	v_assert_ptr(NULL, !=, avlbl);
	*avlbl = 42;

	v_assert_int(42, ==, *(int *)array_get_at(&array, 0));

	array_shutdown(&array);
	VTS;
}

static void test_00_array_get_available_Realloc(void)
{
	t_array array;
	int *avlbl;

	array_init(&array, sizeof(int));

	for (size_t i = 0; i < array.capacity - 1; ++i) {
		int n = 21;
		v_assert_ptr(NULL, !=, array_push(&array, &n));
	}

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	avlbl = array_get_available(&array);
	v_assert_ptr(NULL, !=, avlbl);

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	avlbl = array_get_available(&array);
	v_assert_ptr(NULL, !=, avlbl);

	v_assert_size_t(ARRAY_INIT_SIZE * 2, ==, array.capacity);

	array_shutdown(&array);
	VTS;
}

void suite_array_get_available(void)
{
	test_00_array_get_available_Simple();
	test_00_array_get_available_Realloc();

	VSS;
}
#include "header.h"

static t_array array;
static int tab[7] = {1, 2, 3, 4, 5, 6, 7};

static void setup(void)
{
	array_init(&array, sizeof(int));
	for (size_t i = 0; i < ARR_SIZ_MAX(tab); ++i)
		array_push(&array, tab + i);
}

static void teardown(void) { free(array.data); }

static void test_00_array_index_of_CheckAllValue(void)
{
	setup();

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < 7; ++i) {
		int *value = array_get_at(&array, i);
		int index = (int)array_index_of(&array, value);
		v_assert_int(i, ==, index);
	}

	// Last index
	int *value = array_get_at(&array, array.len);
	int index = (int)array_index_of(&array, value);
	v_assert_int(-1, ==, index);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(7, ==, array.len);

	teardown();
	VTS;
}

static void test_01_array_index_of_NotInArray(void)
{
	setup();

	int value;
	int index = (int)array_index_of(&array, &value);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(7, ==, array.len);
	v_assert_int(-1, ==, index);

	teardown();
	VTS;
}

void suite_array_index_of(void)
{
	test_00_array_index_of_CheckAllValue();
	test_01_array_index_of_NotInArray();

	VSS;
}
#include "header.h"

#define TEST_ARRAY_INIT_SIZE 8U

static t_array a;
static t_array *pa;

static void teardown(void)
{
	free(a.data);
	memset(&a, 0, sizeof(t_array));
}

// array_init
static void test_00_array_init_ElemSizeZero(void)
{
	pa = array_init(&a, 0);

	v_assert_ptr(NULL, ==, pa);

	teardown();
	VTS;
}

static void test_01_array_init_ElemSizeOne(void)
{
	pa = array_init(&a, 1);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(1, ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, pa->capacity);

	teardown();
	VTS;
}

static void test_02_array_init_ElemSize21(void)
{
	pa = array_init(&a, 21);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(21, ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, pa->capacity);

	teardown();
	VTS;
}

// array_init_with_capacity
static void test_00_array_init_with_capacity_ElemSizeZeroCapacityZero(void)
{
	pa = array_init_with_capacity(&a, 0, 0);

	v_assert_ptr(NULL, ==, pa);

	teardown();
	VTS;
}

static void test_01_array_init_with_capacity_ElemSizeOneCapacityZero(void)
{
	pa = array_init_with_capacity(&a, 1, 0);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(1, ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, pa->capacity);

	teardown();
	VTS;
}

static void test_02_array_init_with_capacity_ElemSize21CapacityZero(void)
{
	pa = array_init_with_capacity(&a, 21, 0);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(21, ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, pa->capacity);

	teardown();
	VTS;
}

static void
test_03_array_init_with_capacity_ElemSizeIntCapacityOverINIT_SIZE(void)
{
	pa =
	    array_init_with_capacity(&a, sizeof(int), TEST_ARRAY_INIT_SIZE + 1);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(sizeof(int), ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE * 2, ==, pa->capacity);

	teardown();
	VTS;
}

static void
test_04_array_init_with_capacity_ElemSizeIntCapacityUnderINIT_SIZE(void)
{
	pa = array_init_with_capacity(&a, sizeof(int), 1);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(sizeof(int), ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, pa->capacity);

	teardown();
	VTS;
}

static void
test_05_array_init_with_capacity_ElemSizeIntCapacityEqualINIT_SIZE(void)
{
	pa = array_init_with_capacity(&a, sizeof(int), TEST_ARRAY_INIT_SIZE);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(&a, ==, pa);
	v_assert_size_t(sizeof(int), ==, pa->elem_size);
	v_assert_size_t(0, ==, pa->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, pa->capacity);

	teardown();
	VTS;
}

void suite_array_init(void)
{
	test_00_array_init_ElemSizeZero();
	test_01_array_init_ElemSizeOne();
	test_02_array_init_ElemSize21();

	test_00_array_init_with_capacity_ElemSizeZeroCapacityZero();
	test_01_array_init_with_capacity_ElemSizeOneCapacityZero();
	test_02_array_init_with_capacity_ElemSize21CapacityZero();
	test_03_array_init_with_capacity_ElemSizeIntCapacityOverINIT_SIZE();
	test_04_array_init_with_capacity_ElemSizeIntCapacityUnderINIT_SIZE();
	test_05_array_init_with_capacity_ElemSizeIntCapacityEqualINIT_SIZE();

	VSS;
}
#include "header.h"

static t_array array;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&array, sizeof(char *));
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i)
		array_push(&array, &str[i]);
}

static void teardown(void) { free(array.data); }

static void test_00_array_insert_at_FirstPlace(void)
{
	char *s1 = "zut";
	char *value;
	size_t index = 0;

	setup();

	array_insert_at(&array, index, &s1);

	// Check array integrity
	v_assert_size_t(6, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0, j = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		if (i == index) {
			v_assert_ptr(s1, ==, value);
			v_assert_str(s1, value);
		} else {
			v_assert_ptr(str[j], ==, value);
			v_assert_str(str[j], value);
			++j;
		}
	}

	// last, unassigned
	value = array_get_at(&array, array.len);
	v_assert_ptr(NULL, ==, value);

	teardown();
	VTS;
}

static void test_01_array_insert_at_LastPlace(void)
{
	char *s1 = "zut";
	char *value;
	size_t index;

	setup();

	index = array.len - 1;
	array_insert_at(&array, index, &s1);

	// Check array integrity
	v_assert_size_t(6, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0, j = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);

		if (i == index) {
			v_assert_ptr(s1, ==, value);
			v_assert_str(s1, value);
		} else {
			v_assert_ptr(str[j], ==, value);
			v_assert_str(str[j], value);
			++j;
		}
	}

	// last, unassigned
	value = array_get_at(&array, array.len);
	v_assert_ptr(NULL, ==, value);

	teardown();
	VTS;
}

static void test_02_array_insert_at_MiddlePlace(void)
{
	char *s1 = "zut";
	char *value;
	size_t index;

	setup();

	index = array.len / 2;
	array_insert_at(&array, index, &s1);

	// Check array integrity
	v_assert_size_t(6, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0, j = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);

		if (i == index) {
			v_assert_ptr(s1, ==, value);
			v_assert_str(s1, value);
		} else {
			v_assert_ptr(str[j], ==, value);
			v_assert_str(str[j], value);
			++j;
		}
	}

	// last, unassigned
	value = array_get_at(&array, array.len);
	v_assert_ptr(NULL, ==, value);

	teardown();
	VTS;
}

static void test_03_array_insert_at_Resize(void)
{
	char *s1 = "zut1";
	char *s2 = "zut2";
	char *s3 = "zut3";
	char *value;
	size_t index;

	setup();

	array_insert_at(&array, array.len, &s1);
	array_insert_at(&array, array.len, &s2);
	array_insert_at(&array, array.len, &s3);

	// Check array integrity
	v_assert_size_t(8, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len - 3; ++i) {
		value = *(char **)array_get_at(&array, i);
		v_assert_ptr(str[i], ==, value);
		v_assert_str(str[i], value);
	}

	index = array.len - 1;
	value = *(char **)array_get_at(&array, index);
	v_assert_ptr(s3, ==, value);
	v_assert_str(s3, value);

	index = array.len - 2;
	value = *(char **)array_get_at(&array, index);
	v_assert_ptr(s2, ==, value);
	v_assert_str(s2, value);

	index = array.len - 3;
	value = *(char **)array_get_at(&array, index);
	v_assert_ptr(s1, ==, value);
	v_assert_str(s1, value);

	// last, unassigned
	value = array_get_at(&array, array.len);
	v_assert_ptr(NULL, ==, value);

	teardown();
	VTS;
}

void suite_array_insert_at(void)
{
	test_00_array_insert_at_FirstPlace();
	test_01_array_insert_at_LastPlace();
	test_02_array_insert_at_MiddlePlace();
	test_03_array_insert_at_Resize();

	VSS;
}
#include "header.h"

static t_array array;

static void test_00_array_pop_Int(void)
{
	int data[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	array_init(&array, sizeof(int));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		int value = data[i];
		array_push(&array, &value);
		v_assert_size_t(i + 1, ==, array.len);
		int *res = array_get_at(&array, i);
		v_assert_int(value, ==, *res);
	}
	v_assert_size_t(16, ==, array.capacity);

	int popped;

	array_pop(&array, &popped);
	v_assert_int(10, ==, popped);
	v_assert_size_t(9, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(9, ==, popped);
	v_assert_size_t(8, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(8, ==, popped);
	v_assert_size_t(7, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(7, ==, popped);
	v_assert_size_t(6, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(6, ==, popped);
	v_assert_size_t(5, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(5, ==, popped);
	v_assert_size_t(4, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(4, ==, popped);
	v_assert_size_t(3, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(3, ==, popped);
	v_assert_size_t(2, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(2, ==, popped);
	v_assert_size_t(1, ==, array.len);

	array_pop(&array, &popped);
	v_assert_int(1, ==, popped);
	v_assert_size_t(0, ==, array.len);

	v_assert_ptr(NULL, ==, array_pop(&array, &popped));
	v_assert_size_t(16, ==, array.capacity);
	v_assert_size_t(0, ==, array.len);

	array_shutdown(&array);
	VTS;
}

static void test_01_array_pop_whenEmpty(void)
{
	array_init(&array, 1);

	v_assert_ptr(NULL, ==, array_pop(&array, NULL));
	v_assert_size_t(8, ==, array.capacity);
	v_assert_size_t(0, ==, array.len);

	array_shutdown(&array);
}

void suite_array_pop(void)
{
	test_00_array_pop_Int();
	test_01_array_pop_whenEmpty();

	VSS;
}
#include "header.h"

static t_array array;

static void setup(void *data, size_t elem_size, size_t len)
{
	unsigned char *p;
	unsigned char *d;

	array_init(&array, elem_size);
	if (data != NULL) {
		p = array.data;
		d = data;
		for (size_t i = 0; i < len; ++i)
			memcpy(p + (elem_size * i), d + (elem_size * i),
			       elem_size);
		array.len = len;
	}
}

static void teardown(void) { free(array.data); }

static void test_00_array_push_Int(void)
{
	int data[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	setup(NULL, sizeof(int), 0);

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		int value = data[i];
		array_push(&array, &value);
		if (i < 8)
			v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
		else
			v_assert_size_t(16, ==, array.capacity);
		v_assert_size_t(i + 1, ==, array.len);
		int *res = array_get_at(&array, i);
		v_assert_int(value, ==, *res);
	}

	teardown();
	VTS;
}

static void test_01_array_push_String(void)
{
	char *data[] = {
	    "hello", "world", "and", "good", "morning",
	};
	setup(NULL, sizeof(char *), 0);

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		char *value = data[i];
		array_push(&array, &value);
		if (i < 7)
			v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
		else
			v_assert_size_t(16, ==, array.capacity);
		v_assert_size_t(i + 1, ==, array.len);
		char **res = array_get_at(&array, i);
		v_assert_str(value, *res);
	}

	teardown();
	VTS;
}

static void test_02_array_push_Struct(void)
{
	struct s_test {
		void *e;
		int i;
		char c[4];
	} data[3] = {{NULL, 42, {'a', 'b', 'c', 'd'}},
		     {(void *)0xdeadbeef, 8, {'z', 'y', 'x', 'w'}},
		     {(void *)0xabcdef, -1, {'*', '&', '^', '%'}}};
	setup(NULL, sizeof(struct s_test), 0);

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		struct s_test value = data[i];
		array_push(&array, &value);

		if (i < 7)
			v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
		else
			v_assert_size_t(16, ==, array.capacity);

		struct s_test *res = array_get_at(&array, i);
		v_assert_ptr(value.e, ==, res->e);
		v_assert_int(value.i, ==, res->i);
	}

	teardown();
	VTS;
}

void suite_array_push(void)
{
	test_00_array_push_Int();
	test_01_array_push_String();
	test_02_array_push_Struct();

	VSS;
}
#include "header.h"

static t_array array;
static char *str[] = {
    "hello", "world", "and", "good", "morning", "string", "padding", "enable",
};

static void setup(void)
{
	array_init(&array, sizeof(char *));
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i) {
		array_push(&array, &str[i]);
	}
}

static void teardown(void) { free(array.data); }

static void test_00_array_remove_at_FirstItem(void)
{
	void *ptr;
	char *old;
	char *value;
	size_t index;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	index = 0;
	ptr = array_remove_at(&array, index, &old);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_ptr(str[index], ==, old);
	v_assert_str(str[index], old);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		v_assert_ptr(str[i + 1], ==, value);
		v_assert_str(str[i + 1], value);
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 7));
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_01_array_remove_at_MiddleItem(void)
{
	void *ptr;
	char *old;
	char *value;
	size_t index;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	index = array.len / 2;
	ptr = array_remove_at(&array, index, &old);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_ptr(str[index], ==, old);
	v_assert_str(str[index], old);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		if (i < index) {
			v_assert_ptr(str[i], ==, value);
			v_assert_str(str[i], value);
		} else {
			v_assert_ptr(str[i + 1], ==, value);
			v_assert_str(str[i + 1], value);
		}
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 7));
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_02_array_remove_at_LastItem(void)
{
	void *ptr;
	char *old;
	char *value;
	size_t index;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	index = array.len - 1;
	ptr = array_remove_at(&array, index, &old);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);
	v_assert_ptr(str[index], ==, old);
	v_assert_str(str[index], old);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		if (i < index) {
			v_assert_ptr(str[i], ==, value);
			v_assert_str(str[i], value);
		} else {
			v_assert_ptr(str[i + 1], ==, value);
			v_assert_str(str[i + 1], value);
		}
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 7));
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_03_array_remove_at_OutOfRange(void)
{
	void *ptr;
	char *old;
	char *value;
	size_t index;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	index = array.len;
	ptr = array_remove_at(&array, index, &old);

	// Check return value
	v_assert_ptr(NULL, ==, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		v_assert_ptr(str[i], ==, value);
		v_assert_str(str[i], value);
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_04_array_remove_at_OneItemInArrayOfSizeOne(void)
{
	int ptr;

	array_init_with_capacity(&array, sizeof(int), 1);
	array_push(&array, &(int){42});
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(1, ==, array.len);

	array_remove_at(&array, 0, &ptr);
	v_assert_int(42, ==, ptr);
	v_assert_size_t(0, ==, array.len);

	teardown();
	VTS;
}

void suite_array_remove_at(void)
{
	test_00_array_remove_at_FirstItem();
	test_01_array_remove_at_MiddleItem();
	test_02_array_remove_at_LastItem();
	test_03_array_remove_at_OutOfRange();
	test_04_array_remove_at_OneItemInArrayOfSizeOne();

	VSS;
}
#include "header.h"

static t_array array;
static char *str[] = {
    "hello", "world", "and", "good", "morning", "string", "padding", "enable",
};

static void setup(void)
{
	array_init(&array, sizeof(char *));
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i) {
		array_push(&array, &str[i]);
	}
}

static void teardown(void) { free(array.data); }

static void test_00_array_remove_elem_FirstItem(void)
{
	void *ptr;
	char *value;
	size_t index;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	// remove first elem
	ptr = array_remove_elem(&array, array.data);

	v_assert_ptr(NULL, !=, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		v_assert_ptr(str[i + 1], ==, value);
		v_assert_str(str[i + 1], value);
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 7));
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_01_array_remove_elem_MiddleItem(void)
{
	void *ptr;
	char *value;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	size_t index = array.len / 2;
	uintptr_t elem = sizeof(void *) * index;
	ptr = array_remove_elem(&array, (char *)array.data + elem);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		if (i < index) {
			v_assert_ptr(str[i], ==, value);
			v_assert_str(str[i], value);
		} else {
			v_assert_ptr(str[i + 1], ==, value);
			v_assert_str(str[i + 1], value);
		}
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 7));
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_02_array_remove_elem_LastItem(void)
{
	void *ptr;
	char *value;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	size_t index = array.len - 1;
	uintptr_t elem = sizeof(void *) * index;
	ptr = array_remove_elem(&array, (char *)array.data + elem);

	// Check return value
	v_assert_ptr(NULL, !=, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str) - 1, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		if (i < index) {
			v_assert_ptr(str[i], ==, value);
			v_assert_str(str[i], value);
		} else {
			v_assert_ptr(str[i + 1], ==, value);
			v_assert_str(str[i + 1], value);
		}
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 7));
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_03_array_remove_elem_OutOfRange(void)
{
	void *ptr;
	char *value;

	setup();
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);

	size_t index = array.len;
	uintptr_t elem = sizeof(void *) * index;
	ptr = array_remove_elem(&array, (char *)array.data + elem);

	// Check return value
	v_assert_ptr(NULL, ==, ptr);

	// Check array integrity
	v_assert_size_t(ARR_SIZ_MAX(str), ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		v_assert_ptr(str[i], ==, value);
		v_assert_str(str[i], value);
	}

	// last, unassigned
	v_assert_ptr(NULL, ==, array_get_at(&array, 8));

	teardown();
	VTS;
}

static void test_04_array_remove_elem_OneItemInArrayOfSizeOne(void)
{
	array_init_with_capacity(&array, sizeof(int), 1);
	array_push(&array, &(int){42});
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(1, ==, array.len);

	array_remove_elem(&array, array.data);
	v_assert_size_t(0, ==, array.len);

	teardown();
	VTS;
}

void suite_array_remove_elem(void)
{
	test_00_array_remove_elem_FirstItem();
	test_01_array_remove_elem_MiddleItem();
	test_02_array_remove_elem_LastItem();
	test_03_array_remove_elem_OutOfRange();
	test_04_array_remove_elem_OneItemInArrayOfSizeOne();

	VSS;
}
#include "header.h"

static t_array array;
static char *str[] = {
    "hello", "world", "and", "good", "morning",
};

static void setup(void)
{
	array_init(&array, sizeof(char *));
	for (size_t i = 0; i < ARR_SIZ_MAX(str); ++i)
		array_push(&array, &str[i]);
}

static void teardown(void) { free(array.data); }

static void test_00_array_replace_at_FirstItem(void)
{
	char *rep = "firstitem";
	void *old = (void *)0xdeadbeef;
	char *value;
	size_t index = 0;

	setup();

	array_replace_at(&array, index, &rep, &old);

	// Check return value
	v_assert_ptr(NULL, !=, old);
	v_assert_ptr(str[index], ==, old);
	v_assert_str(str[index], old);

	// Check array integrity
	v_assert_size_t(5, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	value = *(char **)array_get_at(&array, 0);
	v_assert_ptr(rep, ==, value);

	value = *(char **)array_get_at(&array, 1);
	v_assert_ptr(str[1], ==, value);

	value = *(char **)array_get_at(&array, 2);
	v_assert_ptr(str[2], ==, value);

	value = *(char **)array_get_at(&array, 3);
	v_assert_ptr(str[3], ==, value);

	value = *(char **)array_get_at(&array, 4);
	v_assert_ptr(str[4], ==, value);

	value = array_get_at(&array, 5);
	v_assert_ptr(NULL, ==, value);

	value = *(char **)array_get_at(&array, 0);
	v_assert_str(rep, value);

	value = *(char **)array_get_at(&array, 1);
	v_assert_str(str[1], value);

	value = *(char **)array_get_at(&array, 2);
	v_assert_str(str[2], value);

	value = *(char **)array_get_at(&array, 3);
	v_assert_str(str[3], value);

	value = *(char **)array_get_at(&array, 4);
	v_assert_str(str[4], value);

	teardown();
	VTS;
}

static void test_01_array_replace_at_MiddleItem(void)
{
	char *rep = "middleitem";
	void *old = (void *)0xdeadbeef;
	char *value;
	size_t index = 2;

	setup();

	array_replace_at(&array, index, &rep, &old);

	// Check return value
	v_assert_ptr(NULL, !=, old);
	v_assert_ptr(str[index], ==, old);
	v_assert_str(str[index], old);

	// Check array integrity
	v_assert_size_t(5, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	value = *(char **)array_get_at(&array, 0);
	v_assert_ptr(str[0], ==, value);

	value = *(char **)array_get_at(&array, 1);
	v_assert_ptr(str[1], ==, value);

	value = *(char **)array_get_at(&array, 2);
	v_assert_ptr(rep, ==, value);

	value = *(char **)array_get_at(&array, 3);
	v_assert_ptr(str[3], ==, value);

	value = *(char **)array_get_at(&array, 4);
	v_assert_ptr(str[4], ==, value);

	value = array_get_at(&array, 5);
	v_assert_ptr(NULL, ==, value);

	value = *(char **)array_get_at(&array, 0);
	v_assert_str(str[0], value);

	value = *(char **)array_get_at(&array, 1);
	v_assert_str(str[1], value);

	value = *(char **)array_get_at(&array, 2);
	v_assert_str(rep, value);

	value = *(char **)array_get_at(&array, 3);
	v_assert_str(str[3], value);

	value = *(char **)array_get_at(&array, 4);
	v_assert_str(str[4], value);

	teardown();
	VTS;
}

static void test_02_array_replace_at_LastItem(void)
{
	char *rep = "lastitem";
	void *old = (void *)0xdeadbeef;
	char *value;
	size_t index = 4;

	setup();

	array_replace_at(&array, index, &rep, &old);

	// Check return value
	v_assert_ptr(NULL, !=, old);
	v_assert_ptr(str[index], ==, old);
	v_assert_str(str[index], old);

	// Check array integrity
	v_assert_size_t(5, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);

	value = *(char **)array_get_at(&array, 0);
	v_assert_ptr(str[0], ==, value);

	value = *(char **)array_get_at(&array, 1);
	v_assert_ptr(str[1], ==, value);

	value = *(char **)array_get_at(&array, 2);
	v_assert_ptr(str[2], ==, value);

	value = *(char **)array_get_at(&array, 3);
	v_assert_ptr(str[3], ==, value);

	value = *(char **)array_get_at(&array, 4);
	v_assert_ptr(rep, ==, value);

	value = array_get_at(&array, 5);
	v_assert_ptr(NULL, ==, value);

	value = *(char **)array_get_at(&array, 0);
	v_assert_str(str[0], value);

	value = *(char **)array_get_at(&array, 1);
	v_assert_str(str[1], value);

	value = *(char **)array_get_at(&array, 2);
	v_assert_str(str[2], value);

	value = *(char **)array_get_at(&array, 3);
	v_assert_str(str[3], value);

	value = *(char **)array_get_at(&array, 4);
	v_assert_str(rep, value);

	teardown();
	VTS;
}

static void test_03_array_replace_at_OutOfRange(void)
{
	char *rep = "outofrange";
	void *old = (void *)0xdeadbeef;
	char *value;
	void *ret;
	size_t index = 8;

	setup();

	ret = array_replace_at(&array, index, &rep, &old);

	// Check return value
	v_assert_ptr(NULL, ==, ret);

	// Check array integrity
	v_assert_size_t(5, ==, array.len);
	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	for (size_t i = 0; i < array.len; ++i) {
		value = *(char **)array_get_at(&array, i);
		v_assert_ptr(str[i], ==, value);
		v_assert_str(str[i], value);
	}

	teardown();
	VTS;
}

void suite_array_replace_at(void)
{
	test_00_array_replace_at_FirstItem();
	test_01_array_replace_at_MiddleItem();
	test_02_array_replace_at_LastItem();
	test_03_array_replace_at_OutOfRange();

	VSS;
}
#include "header.h"

#define TEST_ARRAY_INIT_SIZE 8U

static t_array *a;
static t_array *pa;
static void *comp;

static void setup(size_t len)
{
	a = malloc(sizeof(t_array));
	a->capacity = TEST_ARRAY_INIT_SIZE;
	a->len = len;
	a->elem_size = 16;
	a->data = malloc(a->capacity * a->elem_size);
	comp = malloc(a->capacity * a->elem_size);
	memset(a->data, '*', a->elem_size * a->len);
	memset(comp, '*', a->elem_size * a->len);
}

static void teardown(void)
{
	free(a->data);
	free(a);
	free(comp);
	a = NULL;
	pa = NULL;
	comp = NULL;
}

static void test_00_array_reserve_LenZeroAddOne(void)
{
	setup(0);
	pa = array_reserve(a, 1);

	v_assert_ptr(pa, ==, a);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	teardown();
	VTS;
}

static void test_01_array_reserve_LenFullAddOne(void)
{
	setup(TEST_ARRAY_INIT_SIZE);
	pa = array_reserve(a, 1);

	v_assert_ptr(pa, ==, a);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE * 2, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

static void test_02_array_reserve_LenZeroAddINIT_SIZE(void)
{
	setup(0);
	pa = array_reserve(a, TEST_ARRAY_INIT_SIZE);

	v_assert_ptr(pa, ==, a);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

static void test_03_array_reserve_LenFullAddINIT_SIZE(void)
{
	setup(TEST_ARRAY_INIT_SIZE);
	pa = array_reserve(a, TEST_ARRAY_INIT_SIZE);

	v_assert_ptr(pa, ==, a);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE, ==, a->len);
	v_assert_size_t(TEST_ARRAY_INIT_SIZE * 2, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

void suite_array_reserve(void)
{
	test_00_array_reserve_LenZeroAddOne();
	test_01_array_reserve_LenFullAddOne();
	test_02_array_reserve_LenZeroAddINIT_SIZE();
	test_03_array_reserve_LenFullAddINIT_SIZE();

	VSS;
}
#include "header.h"

#define TEST_ARRAY_INIT_SIZE 8U

static t_array *a;
static t_array *pa;
static void *comp;

static void setup(size_t len, size_t capacity)
{
	a = malloc(sizeof(t_array));
	a->capacity = capacity;
	a->len = len;
	a->elem_size = 16;
	a->data = malloc(a->capacity * a->elem_size);
	comp = malloc(a->capacity * a->elem_size);
	memset(a->data, '*', a->elem_size * a->len);
	memset(comp, '*', a->elem_size * a->len);
}

static void teardown(void)
{
	free(a->data);
	free(a);
	free(comp);
	a = NULL;
	pa = NULL;
	comp = NULL;
}

static void test_00_array_shrink_ReduceBy4(void)
{
	setup(8, 32);
	void *backup = a->data;
	pa = array_shrink_to_fit(a);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(a, ==, pa);
	v_assert_ptr(backup, !=, a->data);
	v_assert_size_t(8, ==, a->len);
	v_assert_size_t(8, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

static void test_01_array_shrink_ReduceOnLenZeroCapacityAboveINIT_SIZE(void)
{
	setup(0, 64);
	void *backup = a->data;
	pa = array_shrink_to_fit(a);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(a, ==, pa);
	v_assert_ptr(backup, !=, a->data);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(8, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

static void test_02_array_shrink_ReduceOnLenZeroCapacityEqualINIT_SIZE(void)
{
	setup(0, TEST_ARRAY_INIT_SIZE);
	void *backup = a->data;
	pa = array_shrink_to_fit(a);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(a, ==, pa);
	v_assert_ptr(backup, ==, a->data);
	v_assert_size_t(0, ==, a->len);
	v_assert_size_t(8, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

static void test_03_array_shrink_ReduceOnLenEqualCapacity(void)
{
	setup(16, 16);
	pa = array_shrink_to_fit(a);

	v_assert_ptr(NULL, !=, pa);
	v_assert_ptr(a, ==, pa);
	v_assert_size_t(16, ==, a->len);
	v_assert_size_t(16, ==, a->capacity);

	int res = memcmp(a->data, comp, a->len * a->elem_size);
	v_assert_int(0, ==, res);

	teardown();
	VTS;
}

void suite_array_shrink_to_fit(void)
{
	test_00_array_shrink_ReduceBy4();
	test_01_array_shrink_ReduceOnLenZeroCapacityAboveINIT_SIZE();
	test_02_array_shrink_ReduceOnLenZeroCapacityEqualINIT_SIZE();
	test_03_array_shrink_ReduceOnLenEqualCapacity();

	VSS;
}
#include "header.h"

static t_array array;

static void setup(void *data, size_t elem_size, size_t len)
{
	unsigned char *p;
	unsigned char *d;

	array_init(&array, elem_size);
	if (data != NULL) {
		p = array.data;
		d = data;
		for (size_t i = 0; i < len; ++i)
			memcpy(p + (elem_size * i), d + (elem_size * i),
			       elem_size);
		array.len = len;
	}
}

static void teardown(void) { free(array.data); }

static void test_00_array_swap_Int(void)
{
	int data[4] = {11, 22, 33, 44};
	setup(data, sizeof(int), ARR_SIZ_MAX(data));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		int *value = array_get_at(&array, i);
		v_assert_int(data[i], ==, *value);
	}

	// swap first with last
	void *ret = array_swap(&array, 0, 3);
	v_assert_ptr(NULL, !=, ret);
	v_assert_int(44, ==, *(int *)array_get_at(&array, 0));
	v_assert_int(11, ==, *(int *)array_get_at(&array, 3));

	// swap middle
	ret = array_swap(&array, 1, 2);
	v_assert_ptr(NULL, !=, ret);
	v_assert_int(33, ==, *(int *)array_get_at(&array, 1));
	v_assert_int(22, ==, *(int *)array_get_at(&array, 2));

	teardown();
	VTS;
}

static void test_01_array_swap_String(void)
{
	char *data[] = {
	    "hello", "world", "and", "good", "morning",
	};
	setup(data, sizeof(char *), ARR_SIZ_MAX(data));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		char **value = array_get_at(&array, i);
		v_assert_str(data[i], *value);
	}

	// swap first with last
	void *ret = array_swap(&array, 0, 4);
	v_assert_ptr(NULL, !=, ret);
	v_assert_str(data[4], *(char **)array_get_at(&array, 0));
	v_assert_str(data[0], *(char **)array_get_at(&array, 4));

	// swap middle
	ret = array_swap(&array, 1, 2);
	v_assert_ptr(NULL, !=, ret);
	v_assert_str(data[1], *(char **)array_get_at(&array, 2));
	v_assert_str(data[2], *(char **)array_get_at(&array, 1));

	teardown();
	VTS;
}

static void test_02_array_swap_Struct(void)
{
	struct s_test {
		void *e;
		int i;
		char c;
		char padding[3];
	} data[3] = {
	    {NULL, 42, 'a', {0}},
	    {(void *)0xdeadbeef, 8, 'z', {0}},
	    {(void *)0xabcdef, -1, '*', {0}},
	};
	setup(data, sizeof(struct s_test), ARR_SIZ_MAX(data));

	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		struct s_test value = *(struct s_test *)array_get_at(&array, i);
		v_assert_ptr(data[i].e, ==, value.e);
		v_assert_int(data[i].i, ==, value.i);
		v_assert_char(data[i].c, ==, value.c);
	}

	// swap first with last
	void *ret = array_swap(&array, 0, 2);
	v_assert_ptr(NULL, !=, ret);
	struct s_test old_first = *(struct s_test *)array_get_at(&array, 0);
	struct s_test old_last = *(struct s_test *)array_get_at(&array, 2);
	v_assert_ptr(data[2].e, ==, old_first.e);
	v_assert_int(data[2].i, ==, old_first.i);
	v_assert_char(data[2].c, ==, old_first.c);
	v_assert_ptr(data[0].e, ==, old_last.e);
	v_assert_int(data[0].i, ==, old_last.i);
	v_assert_char(data[0].c, ==, old_last.c);

	// swap middle, with itself
	ret = array_swap(&array, 0, 2);
	v_assert_ptr(NULL, !=, ret);
	ret = array_swap(&array, 1, 1);
	v_assert_ptr(NULL, !=, ret);
	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		struct s_test value = *(struct s_test *)array_get_at(&array, i);
		v_assert_ptr(data[i].e, ==, value.e);
		v_assert_int(data[i].i, ==, value.i);
		v_assert_char(data[i].c, ==, value.c);
	}

	teardown();
	VTS;
}

static void test_03_array_swap_OutOfRange(void)
{
	int data[4] = {11, 22, 33, 44};
	setup(data, sizeof(int), ARR_SIZ_MAX(data));

	v_assert_ptr(NULL, ==, array_swap(&array, 4, 0));

	// check integrity
	for (size_t i = 0; i < ARR_SIZ_MAX(data); ++i) {
		int value = *(int *)array_get_at(&array, i);
		v_assert_int(data[i], ==, value);
	}

	teardown();
	VTS;
}

void suite_array_swap(void)
{
	test_00_array_swap_Int();
	test_01_array_swap_String();
	test_02_array_swap_Struct();
	test_03_array_swap_OutOfRange();

	VSS;
}
#include "header.h"

static void test_00_array_truncate_Simple(void)
{
	t_array arr;
	char *s1 = "hello";
	char *s2 = "world";
	char *s3 = "and";
	char *s4 = "good";
	char *s5 = "morning";

	array_init(&arr, sizeof(char *));
	array_push(&arr, &s1);
	array_push(&arr, &s2);
	array_push(&arr, &s3);
	array_push(&arr, &s4);
	array_push(&arr, &s5);

	v_assert_size_t(ARRAY_INIT_SIZE, ==, arr.capacity);
	v_assert_size_t(5, ==, arr.len);
	v_assert_str(s1, *(char **)array_get_at(&arr, 0));
	v_assert_ptr(s1, ==, *(char **)array_get_at(&arr, 0));
	v_assert_str(s2, *(char **)array_get_at(&arr, 1));
	v_assert_ptr(s2, ==, *(char **)array_get_at(&arr, 1));
	v_assert_str(s3, *(char **)array_get_at(&arr, 2));
	v_assert_ptr(s3, ==, *(char **)array_get_at(&arr, 2));
	v_assert_str(s4, *(char **)array_get_at(&arr, 3));
	v_assert_ptr(s4, ==, *(char **)array_get_at(&arr, 3));
	v_assert_str(s5, *(char **)array_get_at(&arr, 4));
	v_assert_ptr(s5, ==, *(char **)array_get_at(&arr, 4));
	v_assert_ptr(NULL, ==, array_get_at(&arr, 5));
	v_assert_ptr(NULL, ==, array_get_at(&arr, 6));
	v_assert_ptr(NULL, ==, array_get_at(&arr, 7));

	v_assert_ptr(NULL, !=, array_truncate(&arr, 3));

	v_assert_size_t(ARRAY_INIT_SIZE, ==, arr.capacity);
	v_assert_size_t(3, ==, arr.len);
	v_assert_str(s1, *(char **)array_get_at(&arr, 0));
	v_assert_ptr(s1, ==, *(char **)array_get_at(&arr, 0));
	v_assert_str(s2, *(char **)array_get_at(&arr, 1));
	v_assert_ptr(s2, ==, *(char **)array_get_at(&arr, 1));
	v_assert_str(s3, *(char **)array_get_at(&arr, 2));
	v_assert_ptr(s3, ==, *(char **)array_get_at(&arr, 2));
	v_assert_ptr(NULL, ==, array_get_at(&arr, 3));
	v_assert_ptr(NULL, ==, array_get_at(&arr, 4));

	array_shutdown(&arr);
	VTS;
}

static void test_01_array_truncate_Zero(void)
{
	t_array array;
	int n = 42;

	array_init(&array, sizeof(char));

	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);

	v_assert_ptr(NULL, !=, array_truncate(&array, 0));

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(0, ==, array.len);

	array_shutdown(&array);
	VTS;
}

static void test_02_array_truncate_Nothing(void)
{
	t_array array;
	int n = 42;

	array_init(&array, sizeof(char));

	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);

	v_assert_ptr(NULL, !=, array_truncate(&array, 5));

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(5, ==, array.len);

	array_shutdown(&array);
	VTS;
}

static void test_03_array_truncate_Bigger(void)
{
	t_array array;
	int n = 42;

	array_init(&array, sizeof(char));

	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);

	v_assert_ptr(NULL, ==, array_truncate(&array, 15));

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(5, ==, array.len);

	array_shutdown(&array);
	VTS;
}

static void test_04_array_clear(void)
{
	t_array array;
	int n = 42;

	array_init(&array, sizeof(char));

	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);
	array_push(&array, &n);

	array_clear(&array);

	v_assert_size_t(ARRAY_INIT_SIZE, ==, array.capacity);
	v_assert_size_t(0, ==, array.len);

	array_shutdown(&array);
	VTS;
}

void suite_array_truncate_clear(void)
{
	test_00_array_truncate_Simple();
	test_01_array_truncate_Zero();
	test_02_array_truncate_Nothing();
	test_03_array_truncate_Bigger();
	test_04_array_clear();

	VSS;
}
