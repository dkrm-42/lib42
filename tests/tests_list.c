#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ft_list.h"

void checkListIntegrity(t_list *list, const char **expected, size_t len);
void fillList(t_list *list, const char **s, size_t len);

static t_list *list;

void test_ListCreate_CreateWithNormalElemSize(void)
{
	size_t elem_size = sizeof list;

	list = list_create(elem_size);
	TEST_ASSERT_NOT_NULL(list);

	TEST_ASSERT_NOT_NULL(list->pool);
	TEST_ASSERT_NULL(list->head);
	TEST_ASSERT_NULL(list->tail);
	TEST_ASSERT_EQUAL_INT(0, list->len);
	TEST_ASSERT_EQUAL_INT(elem_size, list->elem_size);

	list_destroy(list);
}

void test_ListCreate_CreateWithElemSizeAsOne(void)
{
	size_t elem_size = 1;

	list = list_create(elem_size);
	TEST_ASSERT_NOT_NULL(list);

	TEST_ASSERT_NOT_NULL(list->pool);
	TEST_ASSERT_NULL(list->head);
	TEST_ASSERT_NULL(list->tail);
	TEST_ASSERT_EQUAL_INT(0, list->len);
	TEST_ASSERT_EQUAL_INT(elem_size, list->elem_size);

	list_destroy(list);
}

void test_ListCreate_CreateWithElemSizeAsZero(void)
{
	list = list_create(0);
	TEST_ASSERT_NULL(list);
}

void checkListIntegrity(t_list *list, const char **expected, size_t len);
void fillList(t_list *list, const char **s, size_t len);

static t_list list;
static char **ret;
void setUp(void)
{
	void *ret = list_init(&list, sizeof(char *));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void) { list_shutdown(&list); }

/*
 * String, dynamically allocated
 */
static const char *s[] = {"Hello", "world", "and", "good-bye-afternoon",
			  "morning!"};
static size_t len_s = sizeof s / (sizeof s[0]);

size_t *wrapper(size_t *len, const char **s)
{
	*len += strlen(*s);
	return (len);
}

void test_ListFoldLeft_StringLength(void)
{
	fillList(&list, s, len_s);

	// Init val
	size_t init = 0;
	size_t expected = 0;
	size_t *len;

	// List fold
	len = list_fold_left(&list, (t_list_fold)&wrapper, &init);

	// Expected
	for (size_t i = 0; i < len_s; ++i)
		expected += strlen(s[i]);

	// Check list struct
	checkListIntegrity(&list, s, len_s);
	TEST_ASSERT_EQUAL(expected, *len);
	TEST_ASSERT_TRUE(&init == len);
}

static char errmsg[512];
static t_list list;
static t_list_node *node;
static char **ret;
static char **data;
static const char *s[] = {"Hello", "world", "and", "good", "morning!"};
static size_t len_s = sizeof s / (sizeof s[0]);

void setUp(void)
{
	void *ret = list_init(&list, sizeof(char *));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void)
{
	list_shutdown(&list);
	data = NULL;
}

void test_ListGetFirst_WithEmptyList(void)
{
	void *ret = list_get_first(&list);
	TEST_ASSERT_NULL(ret);
}

void test_ListGetFirst_WithSimpleList(void)
{
	fillList(&list, s, len_s);

	char **ret = list_get_first(&list);
	TEST_ASSERT_EQUAL_STRING(s[0], *ret);

	// 0 -> 1 -> 2 -> 3 -> 4
	checkListIntegrity(&list, s, len_s);
}

void test_ListGetLast_WithEmptyList(void)
{
	void *ret = list_get_last(&list);
	TEST_ASSERT_NULL(ret);
}

void test_ListGetLast_WithSimpleList(void)
{
	fillList(&list, s, len_s);

	char **ret = list_get_last(&list);
	TEST_ASSERT_EQUAL_STRING(s[4], *ret);

	// 0 -> 1 -> 2 -> 3 -> 4
	checkListIntegrity(&list, s, len_s);
}

void test_ListGetAt_GetFirstWithEmptyList(void)
{
	void *ret = list_get_at(&list, 0);
	TEST_ASSERT_NULL(ret);
}

void test_ListGetAt_GetFirstWithSimpleList(void)
{
	fillList(&list, s, len_s);

	char **ret = list_get_at(&list, 0);
	TEST_ASSERT_EQUAL_STRING(s[0], *ret);

	// hello -> world -> and -> good -> morning
	checkListIntegrity(&list, s, len_s);
}

void test_ListGetAt_GetLastWithSimpleList(void)
{
	fillList(&list, s, len_s);

	char **ret = list_get_at(&list, list.len - 1);
	TEST_ASSERT_EQUAL_STRING(s[4], *ret);

	// hello -> world -> and -> good -> morning
	checkListIntegrity(&list, s, len_s);
}

void test_ListGetAt_GetEachWithSimpleList(void)
{
	fillList(&list, s, len_s);

	for (size_t i = 0; i < list.len; ++i) {
		char **ret = list_get_at(&list, i);
		TEST_ASSERT_EQUAL_STRING(s[i], *ret);
	}

	// hello -> world -> and -> good -> morning
	checkListIntegrity(&list, s, len_s);
}

void test_ListSetAt_SetFirstWithEmptyList(void)
{
	const char *str = "first";

	void *ret = list_set_at(&list, 0, &str);
	TEST_ASSERT_NULL(ret);

	ret = list_set_at(&list, 1, &str);
	TEST_ASSERT_NULL(ret);
}

void test_ListSetAt_SetFirstWithSimpleList(void)
{
	const char *str = "first";
	fillList(&list, s, len_s);

	char **ret = list_set_at(&list, 0, &str);
	TEST_ASSERT_EQUAL_PTR(str, *ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);

	const char *arr[len_s];
	memcpy(arr, s, sizeof s);
	arr[0] = str;

	// first -> world -> and -> good -> morning
	checkListIntegrity(&list, arr, 5);
}

void test_ListSetAt_SetLastWithSimpleList(void)
{
	const char *str = "last";
	fillList(&list, s, len_s);

	char **ret = list_set_at(&list, list.len - 1, &str);
	TEST_ASSERT_EQUAL_PTR(str, *ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);

	const char *arr[len_s];
	memcpy(arr, s, sizeof s);
	arr[4] = str;

	// hello -> world -> and -> good -> last
	checkListIntegrity(&list, arr, 5);
}

void test_ListSetAt_SetMiddleWithSimpleList(void)
{
	const char *str = "middle";
	fillList(&list, s, len_s);

	char **ret = list_set_at(&list, 2, &str);
	TEST_ASSERT_EQUAL_PTR(str, *ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);

	const char *arr[len_s];
	memcpy(arr, s, sizeof s);
	arr[2] = str;

	// hello -> world -> middle -> good -> morning
	checkListIntegrity(&list, arr, 5);
}

void test_ListSetAt_SetOnePastLast(void)
{
	const char *str = "past last";
	fillList(&list, s, len_s);

	void *ret = list_set_at(&list, list.len, &str);
	TEST_ASSERT_NULL(ret);
}

void test_ListSetAt_SetAllWithFirst(void)
{
	const char *str = "all-the-same";
	fillList(&list, s, len_s);

	for (size_t i = 0; i < list.len; ++i) {
		char **ret = list_set_at(&list, i, &str);
		TEST_ASSERT_EQUAL_PTR(str, *ret);
		TEST_ASSERT_EQUAL_STRING(str, *ret);
	}

	const char *arr[len_s];
	arr[0] = str;
	arr[1] = str;
	arr[2] = str;
	arr[3] = str;
	arr[4] = str;

	// all-the-same all-the-same all-the-same all-the-same all-the-same
	checkListIntegrity(&list, arr, 5);
}

void test_ListInit_InitWithNormalElemSize(void)
{
	t_list list;
	size_t elem_size = sizeof list;

	void *ret = list_init(&list, elem_size);

	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(&list, ret);

	TEST_ASSERT_NOT_NULL(list.pool);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_INT(0, list.len);
	TEST_ASSERT_EQUAL_INT(elem_size, list.elem_size);
}

void test_ListInit_InitWithElemSizeAsOne(void)
{
	t_list list;
	size_t elem_size = 1;

	void *ret = list_init(&list, elem_size);

	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(&list, ret);

	TEST_ASSERT_NOT_NULL(list.pool);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_INT(0, list.len);
	TEST_ASSERT_EQUAL_INT(elem_size, list.elem_size);
}

void test_ListInit_InitWithElemSizeAsZero(void)
{
	t_list list;

	void *ret = list_init(&list, 0);

	TEST_ASSERT_NULL(ret);
}

void test_ListShutdown_AllFieldsAreClear(void)
{
	t_list list;

	void *ret = list_init(&list, 16);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(&list, ret);

	list_shutdown(&list);
	TEST_ASSERT_NULL(list.pool);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_INT(0, list.len);
	TEST_ASSERT_EQUAL_INT(0, list.elem_size);
}

void checkListIntegrity(t_list *list, const char **expected, size_t len);
void fillList(t_list *list, const char **s, size_t len);

static char errmsg[512];
static t_list list;
static t_list_node *node;
static char **ret;
static char **data;
static const char *s[] = {"Hello", "world", "and", "good", "morning!"};
static size_t len_s = sizeof s / (sizeof s[0]);

void setUp(void)
{
	void *ret = list_init(&list, sizeof(char *));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void)
{
	list_shutdown(&list);
	data = NULL;
}

void test_ListInsertAt_WithEmptyList(void)
{
	const char *str = "first";

	void *ret = list_insert_at(&list, 0, &str);
	TEST_ASSERT_NULL(ret);
}

void test_ListInsertAt_FirstPosition(void)
{
	const char *str = "first";
	const char *str2 = "more first";
	fillList(&list, s, len_s);

	char **ret = list_insert_at(&list, 0, &str);
	TEST_ASSERT_EQUAL_PTR(str, *ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);

	ret = list_insert_at(&list, 0, &str2);
	TEST_ASSERT_EQUAL_PTR(str2, *ret);
	TEST_ASSERT_EQUAL_STRING(str2, *ret);

	const char *arr[7];
	arr[0] = str2;
	arr[1] = str;
	memcpy(&arr[2], s, sizeof s);

	// more first -> first -> hello -> world -> and -> good -> morning
	checkListIntegrity(&list, arr, 7);
}

void test_ListInsertAt_MiddlePosition(void)
{
	const char *str = "second middle";
	const char *str2 = "middle";
	fillList(&list, s, len_s);

	char **ret = list_insert_at(&list, 2, &str);
	TEST_ASSERT_EQUAL_PTR(str, *ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);

	ret = list_insert_at(&list, 2, &str2);
	TEST_ASSERT_EQUAL_PTR(str2, *ret);
	TEST_ASSERT_EQUAL_STRING(str2, *ret);

	const char *arr[7];
	arr[0] = s[0];
	arr[1] = s[1];
	arr[2] = str2;
	arr[3] = str;
	arr[4] = s[2];
	arr[5] = s[3];
	arr[6] = s[4];

	// hello -> world -> middle -> second middle -> and -> good -> morning
	checkListIntegrity(&list, arr, 7);
}

void test_ListInsertAt_LastPosition(void)
{
	const char *str = "last";
	const char *str2 = "true last";
	fillList(&list, s, len_s);

	char **ret = list_insert_at(&list, list.len - 1, &str);
	TEST_ASSERT_EQUAL_PTR(str, *ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);

	ret = list_insert_at(&list, list.len - 1, &str2);
	TEST_ASSERT_EQUAL_PTR(str2, *ret);
	TEST_ASSERT_EQUAL_STRING(str2, *ret);

	const char *arr[7];
	arr[0] = s[0];
	arr[1] = s[1];
	arr[2] = s[2];
	arr[3] = s[3];
	arr[4] = str;
	arr[5] = str2;
	arr[6] = s[4];

	// hello -> world -> and -> good -> last -> true last -> morning
	checkListIntegrity(&list, arr, 7);
}

void test_ListInsertAt_OnePastLastPosition(void)
{
	const char *str = "one past last";
	fillList(&list, s, len_s);

	char **ret = list_insert_at(&list, list.len, &str);
	TEST_ASSERT_NULL(ret);
}

void checkListIntegrity(t_list *list, const char **expected, size_t len);
void fillList(t_list *list, const char **s, size_t len);

static t_list list;
static char **ret;
void setUp(void)
{
	void *ret = list_init(&list, sizeof(char *));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void) { list_shutdown(&list); }

/*
 * String, dynamically allocated
 */
static const char *s[] = {"Hello", "world", "and", "good-bye-afternoon",
			  "morning!"};
static size_t len_s = sizeof s / (sizeof s[0]);

char *string_upper(const char *arg)
{
	size_t len = strlen(arg);
	char *str = malloc(len + 1);

	for (size_t i = 0; i < len; ++i) {
		str[i] = toupper(arg[i]);
	}

	str[len] = '\0';
	return (str);
}

char *ptr_wrap;
char **wrapper_string_upper(const char **arg)
{
	ptr_wrap = string_upper(*arg);
	return (&ptr_wrap);
}

void test_ListMap_UpperAllStrings(void)
{
	fillList(&list, s, len_s);

	// Create mapped list
	t_list *new_list;
	new_list =
	    list_map(&list, (t_list_map)&wrapper_string_upper, sizeof(char *));

	// Check list struct
	checkListIntegrity(&list, s, len_s);
	TEST_ASSERT_NOT_NULL(new_list);
	TEST_ASSERT(new_list != &list);
	TEST_ASSERT_EQUAL(list.len, new_list->len);
	TEST_ASSERT_EQUAL(8, new_list->elem_size);

	// Check list content
	char *str;
	str = *(char **)LIST_NODE_DATA(new_list->head);
	TEST_ASSERT_EQUAL_STRING("HELLO", str);
	free(str);

	str = *(char **)LIST_NODE_DATA(new_list->head->next);
	TEST_ASSERT_EQUAL_STRING("WORLD", str);
	free(str);

	str = *(char **)LIST_NODE_DATA(new_list->head->next->next);
	TEST_ASSERT_EQUAL_STRING("AND", str);
	free(str);

	str = *(char **)LIST_NODE_DATA(new_list->head->next->next->next);
	TEST_ASSERT_EQUAL_STRING("GOOD-BYE-AFTERNOON", str);
	free(str);

	str = *(char **)LIST_NODE_DATA(new_list->head->next->next->next->next);
	TEST_ASSERT_EQUAL_STRING("MORNING!", str);
	free(str);

	TEST_ASSERT_NULL(new_list->head->next->next->next->next->next);

	// cleanup
	list_destroy(new_list);
}

/*
 * Struct, not dynamically allocated
 */
struct s_dumb {
	char *name;
	long count;
	void *next;
};
static const struct s_dumb dumbs[] = {
    {"bli", 1, NULL},
    {"tom sawyer le fou", 2, (void *)0xdeadbeef},
    {"la fin si proche", 3, (void *)0xcafecafe},
};
static struct s_dumb used_for_test;

struct s_dumb *convert(const struct s_dumb *dumb)
{
	used_for_test.name = string_upper(dumb->name);
	used_for_test.count = dumb->count * 10;
	used_for_test.next = dumb->next;
	return (&used_for_test);
}

void test_ListMap_ConvertDumbStruct(void)
{
	// Create initial list
	t_list *old_list;
	old_list = list_create(sizeof(struct s_dumb));
	TEST_ASSERT_NOT_NULL(old_list);
	list_push_back(old_list, &dumbs[0]);
	list_push_back(old_list, &dumbs[1]);
	list_push_back(old_list, &dumbs[2]);
	TEST_ASSERT_EQUAL_INT(3, old_list->len);
	TEST_ASSERT_EQUAL_INT(24, old_list->elem_size);

	// Check initial list
	struct s_dumb *p;
	t_list_node *node;
	node = old_list->head;
	for (size_t i = 0; i < old_list->len; ++i, node = node->next) {
		p = LIST_NODE_DATA(node);
		TEST_ASSERT_EQUAL_STRING(dumbs[i].name, p->name);
		TEST_ASSERT_EQUAL(dumbs[i].count, p->count);
		TEST_ASSERT_TRUE(dumbs[i].next == p->next);
		TEST_ASSERT_TRUE(&dumbs[i] != p);
	}

	// Create mapped list
	t_list *new_list;
	new_list =
	    list_map(old_list, (t_list_map)&convert, sizeof(struct s_dumb));

	// Check list struct
	TEST_ASSERT_NOT_NULL(new_list);
	TEST_ASSERT(new_list != old_list);
	TEST_ASSERT_EQUAL(old_list->len, new_list->len);
	TEST_ASSERT_EQUAL(3, new_list->len);
	TEST_ASSERT_EQUAL(24, new_list->elem_size);

	// Check list content
	p = LIST_NODE_DATA(new_list->head);
	TEST_ASSERT_EQUAL_STRING("BLI", p->name);
	TEST_ASSERT_EQUAL(10, p->count);
	TEST_ASSERT_TRUE(NULL == p->next);
	free(p->name);

	p = LIST_NODE_DATA(new_list->head->next);
	TEST_ASSERT_EQUAL_STRING("TOM SAWYER LE FOU", p->name);
	TEST_ASSERT_EQUAL(20, p->count);
	TEST_ASSERT_TRUE((void *)0xdeadbeef == p->next);
	free(p->name);

	p = LIST_NODE_DATA(new_list->head->next->next);
	TEST_ASSERT_EQUAL_STRING("LA FIN SI PROCHE", p->name);
	TEST_ASSERT_EQUAL(30, p->count);
	TEST_ASSERT_TRUE((void *)0xcafecafe == p->next);
	free(p->name);

	TEST_ASSERT_NULL(new_list->head->next->next->next);

	// Cleanup
	list_destroy(old_list);
	list_destroy(new_list);
}

void checkListIntegrity(t_list *list, const char **expected, size_t len);
void fillList(t_list *list, const char **s, size_t len);

static char errmsg[512];
static t_list list;
static t_list_node *node;
static char **ret;
static char **data;
static const char *s[] = {"Hello", "world", "and", "good", "morning!"};
static size_t len_s = sizeof s / (sizeof s[0]);

void setUp(void)
{
	void *ret = list_init(&list, sizeof(char *));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void)
{
	list_shutdown(&list);
	data = NULL;
}

void test_ListPushFront_PushOnlyOneElement(void)
{
	ret = list_push_front(&list, &s[0]);
	TEST_ASSERT_TRUE(s[0] != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(s[0], *ret);
	TEST_ASSERT_EQUAL(1, list.len);

	// head
	data = (char **)LIST_NODE_DATA(list.head);
	TEST_ASSERT_EQUAL_PTR(ret, data);
	TEST_ASSERT_EQUAL_STRING(s[0], *data);
	TEST_ASSERT_NULL(list.head->next);
	TEST_ASSERT_NULL(list.head->prev);

	// tail
	data = (char **)LIST_NODE_DATA(list.tail);
	TEST_ASSERT_EQUAL_PTR(ret, data);
	TEST_ASSERT_EQUAL_STRING(s[0], *data);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_NULL(list.tail->prev);
}

void test_ListPushFront_PushAllElement(void)
{
	for (size_t i = 0; i < len_s; ++i) {
		const char *str = s[i];
		ret = list_push_front(&list, &str);
		TEST_ASSERT_TRUE(str != (char *)ret);
		TEST_ASSERT_EQUAL_STRING(str, *ret);
		TEST_ASSERT_EQUAL(i + 1, list.len);
	}

	// 4 -> 3 -> 2 -> 1 -> 0
	const char *expected[] = {s[4], s[3], s[2], s[1], s[0]};
	checkListIntegrity(&list, expected, 5);
}

void test_ListPushBack_PushOnlyOneElement(void)
{
	ret = list_push_back(&list, &s[0]);
	TEST_ASSERT_TRUE(s[0] != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(s[0], *ret);
	TEST_ASSERT_EQUAL(1, list.len);

	// head
	data = (char **)LIST_NODE_DATA(list.head);
	TEST_ASSERT_EQUAL_PTR(ret, data);
	TEST_ASSERT_EQUAL_STRING(s[0], *data);
	TEST_ASSERT_NULL(list.head->next);

	// tail
	data = (char **)LIST_NODE_DATA(list.tail);
	TEST_ASSERT_EQUAL_PTR(ret, data);
	TEST_ASSERT_EQUAL_STRING(s[0], *data);
	TEST_ASSERT_NULL(list.tail->next);
}

void test_ListPushBack_PushAllElement(void)
{
	fillList(&list, s, len_s);

	// 0 -> 1 -> 2 -> 3 -> 4
	checkListIntegrity(&list, s, len_s);
}

void test_ListPushFrontAndBack_PushAllElementAlternatively(void)
{
	// good Hello morning and world
	// 3 -> 0 -> 4 -> 2 -> 1

	const char *str = s[4];
	ret = list_push_front(&list, &str);
	TEST_ASSERT_TRUE(str != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);
	TEST_ASSERT_EQUAL(1, list.len);

	str = s[2];
	ret = list_push_back(&list, &str);
	TEST_ASSERT_TRUE(str != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);
	TEST_ASSERT_EQUAL(2, list.len);

	str = s[0];
	ret = list_push_front(&list, &str);
	TEST_ASSERT_TRUE(str != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);
	TEST_ASSERT_EQUAL(3, list.len);

	str = s[1];
	ret = list_push_back(&list, &str);
	TEST_ASSERT_TRUE(str != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);
	TEST_ASSERT_EQUAL(4, list.len);

	str = s[3];
	ret = list_push_front(&list, &str);
	TEST_ASSERT_TRUE(str != (char *)ret);
	TEST_ASSERT_EQUAL_STRING(str, *ret);
	TEST_ASSERT_EQUAL(5, list.len);

	// 3 -> 0 -> 4 -> 2 -> 1
	const char *expected[] = {s[3], s[0], s[4], s[2], s[1]};
	checkListIntegrity(&list, expected, 5);
}

void test_ListPopFront_PopEveryElementOneByOne(void)
{
	t_list *same_list;
	char *elem;

	fillList(&list, s, len_s);

	// 0 -> 1 -> 2 -> 3 -> 4
	same_list = list_pop_front(&list, &elem);
	// 1 -> 2 -> 3 -> 4
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[0], elem);
	TEST_ASSERT_EQUAL(4, list.len);
	checkListIntegrity(&list, &s[1], 4);

	// 1 -> 2 -> 3 -> 4
	same_list = list_pop_front(&list, &elem);
	// 2 -> 3 -> 4
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[1], elem);
	TEST_ASSERT_EQUAL(3, list.len);
	checkListIntegrity(&list, &s[2], 3);

	// 2 -> 3 -> 4
	same_list = list_pop_front(&list, &elem);
	// 3 -> 4
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[2], elem);
	TEST_ASSERT_EQUAL(2, list.len);
	checkListIntegrity(&list, &s[3], 2);

	// 3 -> 4
	same_list = list_pop_front(&list, &elem);
	// 4
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[3], elem);
	TEST_ASSERT_EQUAL(1, list.len);
	checkListIntegrity(&list, &s[4], 1);

	// 4
	same_list = list_pop_front(&list, &elem);
	// [empty]
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_STRING(s[4], elem);
	TEST_ASSERT_EQUAL(0, list.len);
}

void test_ListPopBack_PopEveryElementOneByOne(void)
{
	t_list *same_list;
	char *elem;

	fillList(&list, s, len_s);

	// 0 -> 1 -> 2 -> 3 -> 4
	same_list = list_pop_back(&list, &elem);
	// 0 -> 1 -> 2 -> 3
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_EQUAL_STRING(s[4], elem);
	TEST_ASSERT_EQUAL(4, list.len);
	checkListIntegrity(&list, s, 4);

	// 0 -> 1 -> 2 -> 3
	same_list = list_pop_back(&list, &elem);
	// 0 -> 1 -> 2
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_EQUAL_STRING(s[3], elem);
	TEST_ASSERT_EQUAL(3, list.len);
	checkListIntegrity(&list, s, 3);

	// 0 -> 1 -> 2
	same_list = list_pop_back(&list, &elem);
	// 0 -> 1
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_EQUAL_STRING(s[2], elem);
	TEST_ASSERT_EQUAL(2, list.len);
	checkListIntegrity(&list, s, 2);

	// 0 -> 1
	same_list = list_pop_back(&list, &elem);
	// 0
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_EQUAL_STRING(s[1], elem);
	TEST_ASSERT_EQUAL(1, list.len);
	checkListIntegrity(&list, s, 1);

	// 0
	same_list = list_pop_back(&list, &elem);
	// [empty]
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_STRING(s[0], elem);
	TEST_ASSERT_EQUAL(0, list.len);
}

void test_ListPopFrontAndBack_PopEveryElementOneByOne(void)
{
	t_list *same_list;
	char *elem;

	fillList(&list, s, len_s);

	// 0 -> 1 -> 2 -> 3 -> 4
	same_list = list_pop_front(&list, &elem);
	// 1 -> 2 -> 3 -> 4
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[0], elem);
	TEST_ASSERT_EQUAL(4, list.len);
	checkListIntegrity(&list, &s[1], 4);

	// 1 -> 2 -> 3 -> 4
	same_list = list_pop_back(&list, &elem);
	// 1 -> 2 -> 3
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[4], elem);
	TEST_ASSERT_EQUAL(3, list.len);
	checkListIntegrity(&list, &s[1], 3);

	// 1 -> 2 -> 3
	same_list = list_pop_front(&list, &elem);
	// 2 -> 3
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[1], elem);
	TEST_ASSERT_EQUAL(2, list.len);
	checkListIntegrity(&list, &s[2], 2);

	// 2 -> 3
	same_list = list_pop_back(&list, &elem);
	// 2
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.tail->next);
	TEST_ASSERT_NULL(list.head->prev);
	TEST_ASSERT_EQUAL_STRING(s[3], elem);
	TEST_ASSERT_EQUAL(1, list.len);
	checkListIntegrity(&list, &s[2], 1);

	// 2
	same_list = list_pop_front(&list, &elem);
	// [empty]
	TEST_ASSERT_EQUAL_PTR(&list, same_list);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_STRING(s[2], elem);
	TEST_ASSERT_EQUAL(0, list.len);
}

void checkListIntegrity(t_list *list, const char **expected, size_t len);
void fillList(t_list *list, const char **s, size_t len);

static char errmsg[512];
static t_list list;
static t_list_node *node;
static char **ret;
static char **data;
static const char *s[] = {"Hello", "world", "and", "good", "morning!"};
static size_t len_s = sizeof s / (sizeof s[0]);

void setUp(void)
{
	void *ret = list_init(&list, sizeof(char *));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void)
{
	list_shutdown(&list);
	data = NULL;
}

void test_ListRemove_FromEmptyList(void)
{
	void *ret = list_remove_at(&list, 0, NULL);
	TEST_ASSERT_NULL(ret);

	void *backup = (void *)~0;
	ret = list_remove_at(&list, 0, &backup);
	TEST_ASSERT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR((void *)~0, backup);

	TEST_ASSERT_EQUAL(0, list.len);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
}

void test_ListRemove_LastElementOfList(void)
{
	const char *str = "only one";
	list_push_front(&list, &str);

	void *ret = list_remove_at(&list, 0, NULL);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(0, list.len);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);

	list_push_front(&list, &str);

	char *backup;
	ret = list_remove_at(&list, 0, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(0, list.len);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
	TEST_ASSERT_EQUAL_PTR(str, backup);
}

void test_ListRemove_FirstElementFromList(void)
{
	fillList(&list, s, len_s);

	void *ret = list_remove_at(&list, 0, NULL);
	TEST_ASSERT_EQUAL_PTR(&list, ret);

	const char *arr[4];
	arr[0] = s[1];
	arr[1] = s[2];
	arr[2] = s[3];
	arr[3] = s[4];

	// world -> and -> good -> morning
	checkListIntegrity(&list, arr, 4);

	list_push_front(&list, &s[0]);
	checkListIntegrity(&list, s, len_s);

	void *backup;
	ret = list_remove_at(&list, 0, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(4, list.len);
	TEST_ASSERT_NOT_NULL(list.head);
	TEST_ASSERT_NOT_NULL(list.tail);
	TEST_ASSERT_EQUAL_PTR(s[0], backup);

	// world -> and -> good -> morning
	checkListIntegrity(&list, arr, 4);
}

void test_ListRemove_LastElementFromList(void)
{
	fillList(&list, s, len_s);

	void *ret = list_remove_at(&list, list.len - 1, NULL);
	TEST_ASSERT_EQUAL_PTR(&list, ret);

	const char *arr[4];
	arr[0] = s[0];
	arr[1] = s[1];
	arr[2] = s[2];
	arr[3] = s[3];

	// hello -> world -> and -> good
	checkListIntegrity(&list, arr, 4);

	list_push_back(&list, &s[4]);
	checkListIntegrity(&list, s, len_s);

	void *backup;
	ret = list_remove_at(&list, list.len - 1, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(4, list.len);
	TEST_ASSERT_NOT_NULL(list.head);
	TEST_ASSERT_NOT_NULL(list.tail);
	TEST_ASSERT_EQUAL_PTR(s[4], backup);

	// hello -> world -> and -> good
	checkListIntegrity(&list, arr, 4);
}

void test_ListRemove_MiddleElementFromList(void)
{
	fillList(&list, s, len_s);

	void *ret = list_remove_at(&list, 2, NULL);
	TEST_ASSERT_EQUAL_PTR(&list, ret);

	const char *arr[4];
	arr[0] = s[0];
	arr[1] = s[1];
	arr[2] = s[3];
	arr[3] = s[4];

	// hello -> world -> good -> morning
	checkListIntegrity(&list, arr, 4);

	list_insert_at(&list, 2, &s[2]);
	checkListIntegrity(&list, s, len_s);

	void *backup;
	ret = list_remove_at(&list, 2, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(4, list.len);
	TEST_ASSERT_NOT_NULL(list.head);
	TEST_ASSERT_NOT_NULL(list.tail);
	TEST_ASSERT_EQUAL_PTR(s[2], backup);

	// hello -> world -> good -> morning
	checkListIntegrity(&list, arr, 4);
}

void test_ListRemove_OnePastTheLastElementFromList(void)
{
	fillList(&list, s, len_s);

	void *ret = list_remove_at(&list, list.len, NULL);
	TEST_ASSERT_NULL(ret);
	checkListIntegrity(&list, s, len_s);

	void *backup = (void *)~0;
	ret = list_remove_at(&list, list.len, &backup);
	TEST_ASSERT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR((void *)~0, backup);
	checkListIntegrity(&list, s, len_s);
}

void test_ListRemove_AllElement(void)
{
	void *backup;
	void *ret;
	size_t idx;
	const char *arr[5];

	// hello => world => and => good => morning
	fillList(&list, s, len_s);

	ret = list_remove_at(&list, list.len / 2, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(4, list.len);
	TEST_ASSERT_EQUAL_PTR(s[2], backup);
	arr[0] = s[0];
	arr[1] = s[1];
	arr[2] = s[3];
	arr[3] = s[4];

	// hello -> world -> good -> morning
	checkListIntegrity(&list, arr, list.len);

	ret = list_remove_at(&list, list.len / 2, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(3, list.len);
	TEST_ASSERT_EQUAL_PTR(s[3], backup);
	arr[0] = s[0];
	arr[1] = s[1];
	arr[2] = s[4];

	// hello -> world -> morning
	checkListIntegrity(&list, arr, list.len);

	ret = list_remove_at(&list, list.len / 2, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(2, list.len);
	TEST_ASSERT_EQUAL_PTR(s[1], backup);
	arr[0] = s[0];
	arr[1] = s[4];

	// hello -> morning
	checkListIntegrity(&list, arr, list.len);

	ret = list_remove_at(&list, list.len / 2, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(1, list.len);
	TEST_ASSERT_EQUAL_PTR(s[4], backup);
	arr[0] = s[0];

	// hello
	checkListIntegrity(&list, arr, list.len);

	ret = list_remove_at(&list, list.len / 2, &backup);
	TEST_ASSERT_EQUAL_PTR(&list, ret);
	TEST_ASSERT_EQUAL(0, list.len);
	TEST_ASSERT_EQUAL_PTR(s[0], backup);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);

	ret = list_remove_at(&list, list.len / 2, &backup);
	TEST_ASSERT_NULL(ret);
	TEST_ASSERT_EQUAL(0, list.len);
	TEST_ASSERT_NULL(list.head);
	TEST_ASSERT_NULL(list.tail);
}

static t_list *list;

void setUp(void) {}

void tearDown(void) { list_destroy(list); }

int compare_int(int *a, int *b) { return (*a - *b); }

void test_ListSort_EmptyList(void)
{
	list = list_create(sizeof(int));

	void *ret = list_sort(list, (t_list_compare)&compare_int);

	TEST_ASSERT_EQUAL(0, list->len);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(list, ret);
	TEST_ASSERT_NULL(list->head);
	TEST_ASSERT_NULL(list->tail);
}

void test_ListSort_SimpleUnsortedIntegersList(void)
{
	int arr[] = {55, 11, 33, 22, 44};

	list = list_create(sizeof(int));

	for (int i = 0; i < sizeof(arr) / sizeof(int); ++i)
		list_push_back(list, &arr[i]);

	void *ret = list_sort(list, (t_list_compare)&compare_int);

	TEST_ASSERT_EQUAL(5, list->len);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(list, ret);
	TEST_ASSERT_NOT_NULL(list->head);
	TEST_ASSERT_NOT_NULL(list->tail);

	// check list integrity
	const t_list_node *node;
	const t_list_node *prev;
	int n;

	node = list->head;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_NULL(node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(11, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(22, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(33, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(44, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NULL(node->next);
	TEST_ASSERT_EQUAL(55, n);
}

void test_ListSort_SimpleAlreadySortedIntegersList(void)
{
	int arr[] = {11, 22, 33, 44, 55, 66, 77};

	list = list_create(sizeof(int));

	for (int i = 0; i < sizeof(arr) / sizeof(int); ++i)
		list_push_back(list, &arr[i]);

	void *ret = list_sort(list, (t_list_compare)&compare_int);

	TEST_ASSERT_EQUAL(7, list->len);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(list, ret);
	TEST_ASSERT_NOT_NULL(list->head);
	TEST_ASSERT_NOT_NULL(list->tail);

	// check list integrity
	const t_list_node *node;
	const t_list_node *prev;
	int n;

	node = list->head;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_NULL(node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(11, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(22, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(33, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(44, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(55, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(66, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NULL(node->next);
	TEST_ASSERT_EQUAL(77, n);
}

void test_ListSort_ListWithOnlyOneElement(void)
{
	int arr[] = {11};

	list = list_create(sizeof(int));

	for (int i = 0; i < sizeof(arr) / sizeof(int); ++i)
		list_push_back(list, &arr[i]);

	void *ret = list_sort(list, (t_list_compare)&compare_int);

	TEST_ASSERT_EQUAL(1, list->len);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(list, ret);
	TEST_ASSERT_NOT_NULL(list->head);
	TEST_ASSERT_NOT_NULL(list->tail);

	// check list integrity
	const t_list_node *node;
	const t_list_node *prev;
	int n;

	node = list->head;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_NULL(node->prev);
	TEST_ASSERT_NULL(node->next);
	TEST_ASSERT_EQUAL_PTR(list->head, list->tail);
	TEST_ASSERT_EQUAL(11, n);
}

void test_ListSort_ListWithTwoElementsAlreadySorted(void)
{
	int arr[] = {11, 22};

	list = list_create(sizeof(int));

	for (int i = 0; i < sizeof(arr) / sizeof(int); ++i)
		list_push_back(list, &arr[i]);

	void *ret = list_sort(list, (t_list_compare)&compare_int);

	TEST_ASSERT_EQUAL(2, list->len);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(list, ret);
	TEST_ASSERT_NOT_NULL(list->head);
	TEST_ASSERT_NOT_NULL(list->tail);

	// check list integrity
	const t_list_node *node;
	const t_list_node *prev;
	int n;

	node = list->head;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_NULL(node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(11, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NULL(node->next);
	TEST_ASSERT_EQUAL(22, n);
}

void test_ListSort_ListWithTwoElementsUnsorted(void)
{
	int arr[] = {22, 11};

	list = list_create(sizeof(int));

	for (int i = 0; i < sizeof(arr) / sizeof(int); ++i)
		list_push_back(list, &arr[i]);

	void *ret = list_sort(list, (t_list_compare)&compare_int);

	TEST_ASSERT_EQUAL(2, list->len);
	TEST_ASSERT_NOT_NULL(ret);
	TEST_ASSERT_EQUAL_PTR(list, ret);
	TEST_ASSERT_NOT_NULL(list->head);
	TEST_ASSERT_NOT_NULL(list->tail);

	// check list integrity
	const t_list_node *node;
	const t_list_node *prev;
	int n;

	node = list->head;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_NULL(node->prev);
	TEST_ASSERT_NOT_NULL(node->next);
	TEST_ASSERT_EQUAL(11, n);

	prev = node;
	node = node->next;
	n = *(int *)LIST_NODE_DATA(node);
	TEST_ASSERT_EQUAL_PTR(prev, node->prev);
	TEST_ASSERT_NULL(node->next);
	TEST_ASSERT_EQUAL(22, n);
}

// Faire un test avec un tableau de 4096 int random

static t_list list;

void setUp(void)
{
	void *ret = list_init(&list, sizeof(int));
	TEST_ASSERT_NOT_NULL(ret);
}

void tearDown(void) { list_shutdown(&list); }

int compare(int *a, int *b) { return (*a - *b); }

static void fillList(t_list *list, int *numbers, size_t nlen)
{
	for (size_t i = 0; i < nlen; ++i) {
		list_push_back(list, &numbers[i]);
	}
}

static void checkListIntegrity(const t_list *list, int *numbers, size_t nlen,
			       const char *name)
{
	t_list_node *node;
	const int *data;

	TEST_ASSERT_EQUAL_MESSAGE(nlen, list->len, name);

	node = list->head;
	for (size_t i = 0; i < nlen; ++i, node = node->next) {
		data = (const int *)LIST_NODE_DATA(node);
		TEST_ASSERT_EQUAL(numbers[i], *data);
	}
	TEST_ASSERT_NULL(node);
}

void test_ListUniqSorted_OnlyOneElement(void)
{
	t_list *new;
	int numbers[] = {42};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_sorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	checkListIntegrity(new, numbers, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqSorted_AllElementsAreTheSame(void)
{
	t_list *new;
	int numbers[] = {42, 42, 42};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_sorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	int numbers2[] = {42};
	checkListIntegrity(new, numbers, 1, "new");

	list_destroy(new);
}

void test_ListUniqSorted_AllElementsAreDifferent(void)
{
	t_list *new;
	int numbers[] = {42, 43, 44};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_sorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	checkListIntegrity(new, numbers, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqSorted_TwoElementsUnique(void)
{
	t_list *new;
	int numbers[] = {42, 42};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_sorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	int numbers2[] = {42};
	checkListIntegrity(new, numbers2, 1, "new");

	list_destroy(new);
}

void test_ListUniqSorted_TwoElementsDifferent(void)
{
	t_list *new;
	int numbers[] = {42, 24};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_sorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	checkListIntegrity(new, numbers, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqSorted_RandomList(void)
{
	t_list *new;
	int numbers[] = {1, 2, 3, 4, 4, 4, 5, 5, 6, 7, 7};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_sorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	int numbers2[] = {1, 2, 3, 4, 5, 6, 7};
	numbers_len = sizeof(numbers2) / sizeof(numbers2[0]);
	checkListIntegrity(new, numbers2, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqUnsorted_OnlyOneElement(void)
{
	t_list *new;
	int numbers[] = {42};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_unsorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	checkListIntegrity(new, numbers, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqUnsorted_AllElementsAreTheSame(void)
{
	t_list *new;
	int numbers[] = {42, 42, 42};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_unsorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	int numbers2[] = {42};
	checkListIntegrity(new, numbers, 1, "new");

	list_destroy(new);
}

void test_ListUniqUnsorted_AllElementsAreDifferent(void)
{
	t_list *new;
	int numbers[] = {44, 33, 22};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_unsorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	checkListIntegrity(new, numbers, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqUnsorted_TwoElementsUnique(void)
{
	t_list *new;
	int numbers[] = {42, 42};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_unsorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	int numbers2[] = {42};
	checkListIntegrity(new, numbers2, 1, "new");

	list_destroy(new);
}

void test_ListUniqUnsorted_TwoElementsDifferent(void)
{
	t_list *new;
	int numbers[] = {42, 24};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_unsorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	checkListIntegrity(new, numbers, numbers_len, "new");

	list_destroy(new);
}

void test_ListUniqUnsorted_RandomList(void)
{
	t_list *new;
	int numbers[] = {8, 7, 6, 5, 4, 3, 2, 1, 0, 1, 2, 3, 4, 5, 6, 7, 8};
	size_t numbers_len = sizeof(numbers) / sizeof(numbers[0]);

	// fill list
	fillList(&list, numbers, numbers_len);

	// create uniq list
	new = list_uniq_unsorted(&list, (t_list_compare)&compare);

	// check origin list
	checkListIntegrity(&list, numbers, numbers_len, "origin");

	// check uniq list
	int numbers2[] = {8, 7, 6, 5, 4, 3, 2, 1, 0};
	numbers_len = sizeof(numbers2) / sizeof(numbers2[0]);
	checkListIntegrity(new, numbers2, numbers_len, "new");

	list_destroy(new);
}

void checkListIntegrity(t_list *list, const char **expected, size_t len)
{
	char **data;
	char errmsg[512];

	t_list_node *current = list->head;

	TEST_ASSERT_EQUAL_INT(len, list->len);
	for (size_t i = 0; i < len; ++i, current = current->next) {
		sprintf(errmsg, "Loop counter: %zu", i);

		// current
		data = (char **)LIST_NODE_DATA(current);
		TEST_ASSERT_EQUAL_STRING(expected[i], *data);

		// prev
		if (i > 0) {
			data = (char **)LIST_NODE_DATA(current->prev);
			TEST_ASSERT_EQUAL_STRING(expected[i - 1], *data);
		} else
			TEST_ASSERT_NULL(current->prev);

		// next
		if (i < len - 1) {
			data = (char **)LIST_NODE_DATA(current->next);
			TEST_ASSERT_EQUAL_STRING_MESSAGE(expected[i + 1], *data,
							 errmsg);
		} else {
			TEST_ASSERT_NULL_MESSAGE(current->next, errmsg);
		}
	}
	TEST_ASSERT_NULL(current);
}

void fillList(t_list *list, const char **s, size_t len)
{
	char **ret;

	for (size_t i = 0; i < len; ++i) {
		const char *str = s[i];
		ret = list_push_back(list, &str);
		TEST_ASSERT_TRUE(str != (char *)ret);
		TEST_ASSERT_EQUAL_STRING(str, *ret);
		TEST_ASSERT_EQUAL(i + 1, list->len);
	}
	checkListIntegrity(list, s, len);
	TEST_ASSERT_EQUAL(len, list->len);
}
#include "header.h"

extern size_t g_dlist_pool_index;
extern t_dnode *g_dlist_pool[DLIST_POOL_SIZE];
static char *s1 = "hello";
static char *s2 = "world";
static char *s3 = "& good";
static char *s4 = "morning";
static t_dlist *dlist;
static t_dnode *node1;
static t_dnode *node2;
static t_dnode *node3;
static t_dnode *node4;

static void setup_dlist(void)
{
	dlist = dlist_new();
	node1 = dnode_new(s1);
	node2 = dnode_new(s2);
	node3 = dnode_new(s3);
	node4 = dnode_new(s4);

	node1->prev = NULL;
	node1->next = node2;

	node2->prev = node1;
	node2->next = node3;

	node3->prev = node2;
	node3->next = node4;

	node4->prev = node3;
	node4->next = NULL;

	dlist->start = node1;
	dlist->end = node4;
	dlist->count = 4;
}

static void teardown_dlist(void)
{
	free(node1);
	free(node2);
	free(node3);
	free(node4);
	free(dlist);
	g_dlist_pool_index = 0;
}

/*
 * Append
 */
static void test_00_dlist_append_SimpleAdd(void)
{
	t_dnode *node;
	char *str = "###";

	setup_dlist();
	dlist_append(dlist, str);

	v_assert_size_t(5, ==, dlist->count);
	v_assert_str(str, dlist->end->data);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_ptr(node1, ==, node->prev);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_ptr(node2, ==, node->prev);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_ptr(node3, ==, node->prev);

	node = node->next;
	v_assert_str(str, node->data);
	v_assert_ptr(node4, ==, node->prev);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	free(dlist->end);
	teardown_dlist();
	VTS;
}

static void test_01_dlist_append_AddOnEmptydlist(void)
{
	t_dlist *emptydlist;
	t_dnode *node;
	char *str = "###";
	char *str2 = "!!!";

	emptydlist = dlist_new();
	dlist_append(emptydlist, str);

	v_assert_size_t(1, ==, emptydlist->count);
	node = emptydlist->start;
	v_assert_str(str, node->data);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_str(str, emptydlist->end->data);
	v_assert_ptr(NULL, ==, emptydlist->end->prev);
	v_assert_ptr(NULL, ==, emptydlist->end->next);

	dlist_append(emptydlist, str2);

	v_assert_size_t(2, ==, emptydlist->count);

	node = emptydlist->start;
	v_assert_str(str, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(str2, node->data);
	v_assert_ptr(str, ==, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	free(emptydlist->start->next);
	free(emptydlist->start);
	free(emptydlist);

	VTS;
}

/*
 * Insert
 */
static void test_00_dlist_insert_FirstPlace(void)
{
	t_dnode *node;
	char *str = "inserted";

	setup_dlist();
	dlist_insert(dlist, str, 0);

	v_assert_size_t(5, ==, dlist->count);

	node = dlist->start;
	v_assert_str(str, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s1, node->data);
	v_assert_str(str, node->prev->data);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	free(dlist->start);
	teardown_dlist();
	VTS;
}

static void test_01_dlist_insert_LastPlace(void)
{
	t_dnode *node;
	char *str = "inserted";

	setup_dlist();
	dlist_insert(dlist, str, dlist->count);

	v_assert_size_t(5, ==, dlist->count);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_str(str, node->data);
	v_assert_str(s4, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	free(dlist->end);
	teardown_dlist();
	VTS;
}

static void test_02_dlist_insert_MiddlePlace(void)
{
	t_dnode *node;
	char *str = "inserted";

	setup_dlist();
	dlist_insert(dlist, str, 2);

	v_assert_size_t(5, ==, dlist->count);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(str, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(str, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	free(dlist->start->next->next);
	teardown_dlist();
	VTS;
}

static void test_03_dlist_insert_OutOfRange(void)
{
	t_dnode *node;
	char *str = "inserted";

	setup_dlist();
	dlist_insert(dlist, str, dlist->count + 1);

	v_assert_size_t(5, ==, dlist->count);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_str(str, node->data);
	v_assert_str(s4, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	free(dlist->end);
	teardown_dlist();
	VTS;
}

/*
 * pop
 */
static void test_00_dlist_pop_FirstPlace(void)
{
	t_dnode *ret;
	t_dnode *node;

	setup_dlist();
	ret = dlist_pop(dlist, 0);

	v_assert_size_t(3, ==, dlist->count);
	v_assert_str(s1, ret->data);

	node = dlist->start;
	v_assert_str(s2, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_dlist();
	VTS;
}

static void test_01_dlist_pop_MiddlePlace(void)
{
	t_dnode *ret;
	t_dnode *node;

	setup_dlist();
	ret = dlist_pop(dlist, 2);

	v_assert_size_t(3, ==, dlist->count);
	v_assert_str(s3, ret->data);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_dlist();
	VTS;
}

static void test_02_dlist_pop_LastPlace(void)
{
	t_dnode *ret;
	t_dnode *node;

	setup_dlist();
	ret = dlist_pop(dlist, dlist->count - 1);

	v_assert_size_t(3, ==, dlist->count);
	v_assert_str(s4, ret->data);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_dlist();
	VTS;
}

static void test_03_dlist_pop_OutOfRange(void)
{
	t_dnode *ret;
	t_dnode *node;

	setup_dlist();
	ret = dlist_pop(dlist, dlist->count);

	v_assert_size_t(4, ==, dlist->count);
	v_assert_ptr(NULL, ==, ret);

	node = dlist->start;
	v_assert_str(s1, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s2, node->data);
	v_assert_str(s1, node->prev->data);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_dlist();
	VTS;
}

static void test_04_dlist_pop_data_Simple(void)
{
	t_dnode *node;
	void *ret;

	setup_dlist();

	ret = dlist_pop_data(dlist, 1);

	v_assert_size_t(3, ==, dlist->count);
	v_assert_ptr(s2, ==, ret);
	v_assert_str(s2, ret);
	v_assert_size_t(1, ==, g_dlist_pool_index);
	v_assert_ptr(node2, ==, g_dlist_pool[0]);

	teardown_dlist();
	VTS;
}

/*
 * Remove
 */
static void test_00_dlist_remove_IsPoolWorking(void)
{
	t_dnode *node;

	setup_dlist();
	dlist_remove(dlist, 0);

	v_assert_size_t(1, ==, g_dlist_pool_index);
	v_assert_ptr(node1, ==, g_dlist_pool[0]);
	v_assert_size_t(3, ==, dlist->count);

	node = dlist->start;
	v_assert_str(s2, node->data);
	v_assert_ptr(NULL, ==, node->prev);

	node = node->next;
	v_assert_str(s3, node->data);
	v_assert_str(s2, node->prev->data);

	node = node->next;
	v_assert_str(s4, node->data);
	v_assert_str(s3, node->prev->data);

	node = node->next;
	v_assert_ptr(NULL, ==, node);

	node = dnode_new("in pool");
	v_assert_ptr(node1, ==, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node1->data, "in pool");

	node = dnode_new("not in pool");
	v_assert_ptr(node1, !=, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node->data, "not in pool");

	free(node);
	teardown_dlist();
	VTS;
}

/*
 * Clear
 */
static void test_00_dlist_clear_IsPoolWorking(void)
{
	t_dnode *node;

	setup_dlist();
	dlist_clear(dlist);

	v_assert_size_t(4, ==, g_dlist_pool_index);
	v_assert_ptr(node1, ==, g_dlist_pool[0]);
	v_assert_ptr(node2, ==, g_dlist_pool[1]);
	v_assert_ptr(node3, ==, g_dlist_pool[2]);
	v_assert_ptr(node4, ==, g_dlist_pool[3]);

	v_assert_size_t(0, ==, dlist->count);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(NULL, ==, dlist->start);
	v_assert_ptr(NULL, ==, dlist->end);
	v_assert_ptr(NULL, ==, dlist->iterator);

	node = dnode_new("in pool");
	v_assert_ptr(node4, ==, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node4->data, "in pool");

	node = dnode_new("in pool");
	v_assert_ptr(node3, ==, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node3->data, "in pool");

	node = dnode_new("in pool");
	v_assert_ptr(node2, ==, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node2->data, "in pool");

	node = dnode_new("in pool");
	v_assert_ptr(node1, ==, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node1->data, "in pool");

	node = dnode_new("not in pool");
	v_assert_ptr(node1, !=, node);
	v_assert_ptr(node2, !=, node);
	v_assert_ptr(node3, !=, node);
	v_assert_ptr(node4, !=, node);
	v_assert_ptr(NULL, ==, node->next);
	v_assert_ptr(NULL, ==, node->prev);
	v_assert_str(node->data, "not in pool");

	v_assert_size_t(0, ==, g_dlist_pool_index);

	free(node);
	teardown_dlist();
	VTS;
}

/*
 * Iterator
 * Tester que ca fonctionne bien même si on enlever un element
 * y compris celui sur lequel il pointe actuellement
 */
static void test_00_dlist_iterator_SimpleLoop(void)
{
	t_dnode *node;

	setup_dlist();

	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(NULL, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(node1, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, dlist->iter_index);
	v_assert_ptr(node2, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node3, ==, node);
	v_assert_str(s3, node->data);
	v_assert_size_t(2, ==, dlist->iter_index);
	v_assert_ptr(node3, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node4, ==, node);
	v_assert_str(s4, node->data);
	v_assert_size_t(3, ==, dlist->iter_index);
	v_assert_ptr(node4, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(NULL, ==, node);

	node = dlist_iterator(dlist);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(node1, ==, dlist->iterator);

	dlist_iterator(NULL);
	teardown_dlist();
	VTS;
}

static void test_01_dlist_iterator_RemoveCurrentElem(void)
{
	t_dnode *node;

	setup_dlist();

	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(NULL, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(node1, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, dlist->iter_index);
	v_assert_ptr(node2, ==, dlist->iterator);

	dlist_remove(dlist, 2);

	node = dlist_iterator(dlist);
	v_assert_ptr(node4, ==, node);
	v_assert_str(s4, node->data);
	v_assert_size_t(2, ==, dlist->iter_index);
	v_assert_size_t(3, ==, dlist->count);
	v_assert_ptr(node4, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(NULL, ==, node);

	node = dlist_iterator(dlist);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(node1, ==, dlist->iterator);

	dlist_iterator(NULL);
	teardown_dlist();
	VTS;
}

static void test_02_dlist_iterator_NULL(void)
{
	t_dnode *node;

	setup_dlist();

	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(NULL, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(node1, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, dlist->iter_index);
	v_assert_ptr(node2, ==, dlist->iterator);

	dlist_iterator(NULL);

	node = dlist_iterator(dlist);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, dlist->iter_index);
	v_assert_ptr(node1, ==, dlist->iterator);

	node = dlist_iterator(dlist);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, dlist->iter_index);
	v_assert_ptr(node2, ==, dlist->iterator);

	dlist_iterator(NULL);
	teardown_dlist();
	VTS;
}

void suite_dlist(void)
{
	test_00_dlist_append_SimpleAdd();
	test_01_dlist_append_AddOnEmptydlist();

	test_00_dlist_insert_FirstPlace();
	test_01_dlist_insert_LastPlace();
	test_02_dlist_insert_MiddlePlace();
	test_03_dlist_insert_OutOfRange();

	test_00_dlist_pop_FirstPlace();
	test_01_dlist_pop_MiddlePlace();
	test_02_dlist_pop_LastPlace();
	test_03_dlist_pop_OutOfRange();
	test_04_dlist_pop_data_Simple();

	test_00_dlist_remove_IsPoolWorking();

	test_00_dlist_clear_IsPoolWorking();

	test_00_dlist_iterator_SimpleLoop();
	test_01_dlist_iterator_RemoveCurrentElem();
	test_02_dlist_iterator_NULL();

	VSS;
}
#include "header.h"

extern t_array g_pool_blocks;
static t_array *pool = &g_pool_blocks;

static char *s1 = "hello";
static char *s2 = "world";
static char *s3 = "& good";
static char *s4 = "morning";
static t_list *list;
static t_node *node1;
static t_node *node2;
static t_node *node3;
static t_node *node4;
static struct s_block *block;

static void setup_list(void)
{
	list = list_new();
	node1 = node_new(s1);
	node2 = node_new(s2);
	node3 = node_new(s3);
	node4 = node_new(s4);
	node1->next = node2;
	node2->next = node3;
	node3->next = node4;
	list->start = node1;
	list->end = node4;
	list->count = 4;
}

static void teardown_list(void)
{
	list_pool_destroy();
	free(list);
	list = NULL;
	node1 = NULL;
	node2 = NULL;
	node3 = NULL;
	node4 = NULL;
	block = NULL;
}

/*
 * Pooling
 */
static void test_00_pooling_EmptyList(void)
{
	list = list_new();

	// pool
	v_assert_ptr(NULL, !=, TARRAY_DATA(pool));
	v_assert_size_t(0, ==, TARRAY_COUNT(pool));

	// list
	v_assert_ptr(NULL, ==, list->start);
	v_assert_ptr(NULL, ==, list->end);
	v_assert_ptr(NULL, ==, list->iterator);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_size_t(0, ==, list->count);

	teardown_list();

	VTS;
}

static void test_01_pooling_NodeCreation(void)
{
	setup_list();
	block = TARRAY_GET(pool, 0);

	// pool
	v_assert_ptr(NULL, !=, TARRAY_DATA(pool));
	v_assert_size_t(1, ==, TARRAY_COUNT(pool));

	// block
	v_assert_ptr(block->nodes + 0, ==, node1);
	v_assert_ptr(block->nodes + 1, ==, node2);
	v_assert_ptr(block->nodes + 2, ==, node3);
	v_assert_ptr(block->nodes + 3, ==, node4);

	teardown_list();

	VTS;
}

static void test_02_pooling_BlockCreation(void)
{
	list_pool_init();
	t_node *node;

	// pool
	v_assert_ptr(NULL, !=, TARRAY_DATA(pool));

	// block
	for (size_t i = 0; i < POOL_BLOCK_SIZE; ++i) {
		node = node_new("");
		block = TARRAY_GET(pool, 0);
		v_assert_ptr(block->nodes + i, ==, node);
		v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	}
	v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	v_assert_size_t(0, ==, block->ledger);

	// new block
	node = node_new("");
	block = TARRAY_GET(pool, 1);
	v_assert_ptr(block->nodes + 0, ==, node);
	v_assert_size_t(2, ==, TARRAY_COUNT(pool));
	v_assert_size_t(~0UL - 1, ==, block->ledger);

	teardown_list();

	VTS;
}

static void test_03_pooling_AddUnusedNodeToPool(void)
{
	list_pool_init();
	t_node *nodes[POOL_BLOCK_SIZE];
	t_node *node;

	// pool
	v_assert_ptr(NULL, !=, TARRAY_DATA(pool));

	// block
	for (size_t i = 0; i < POOL_BLOCK_SIZE; ++i) {
		nodes[i] = node_new("");
		block = TARRAY_GET(pool, 0);
		v_assert_ptr(block->nodes + i, ==, nodes[i]);
		v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	}
	v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	v_assert_size_t(0, ==, block->ledger);

	// new block
	node = node_new("");
	block = TARRAY_GET(pool, 1);
	v_assert_ptr(block->nodes + 0, ==, node);
	v_assert_size_t(2, ==, TARRAY_COUNT(pool));
	v_assert_size_t(ALL_NODE_FREE - 1, ==, block->ledger);
	v_assert_ptr(block->nodes, ==, node);

	// return node to pool
	block = TARRAY_GET(pool, 0);
	list_pool_add(nodes[8]);
	v_assert_size_t(2, ==, TARRAY_COUNT(pool));
	v_assert_size_t(256, ==, block->ledger);
	v_assert_size_t(0xFF, ==, block->ledger - 1);

	// get new node in first block
	node = node_new("");
	v_assert_size_t(2, ==, TARRAY_COUNT(pool));
	v_assert_ptr(nodes[8], ==, node);
	block = TARRAY_GET(pool, 0);
	v_assert_size_t(0, ==, block->ledger);
	block = TARRAY_GET(pool, 1);
	v_assert_size_t(ALL_NODE_FREE - 1, ==, block->ledger);

	// get new node in second block
	node = node_new("");
	v_assert_size_t(2, ==, TARRAY_COUNT(pool));
	block = TARRAY_GET(pool, 0);
	v_assert_size_t(0, ==, block->ledger);
	block = TARRAY_GET(pool, 1);
	v_assert_size_t(ALL_NODE_FREE - 3, ==, block->ledger);
	v_assert_ptr(block->nodes + 1, ==, node);

	teardown_list();
	VTS;
}

static void test_04_pooling_RemoveEmptyBlock(void)
{
	list_pool_init();
	t_node *nodes[POOL_BLOCK_SIZE];
	t_node *node;
	int ret;

	// pool
	v_assert_ptr(NULL, !=, TARRAY_DATA(pool));

	// block
	for (size_t i = 0; i < POOL_BLOCK_SIZE; ++i) {
		nodes[i] = node_new("");
		block = TARRAY_GET(pool, 0);
		v_assert_ptr(block->nodes + i, ==, nodes[i]);
		v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	}
	v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	v_assert_size_t(0, ==, block->ledger);

	// new block
	node = node_new("");
	block = TARRAY_GET(pool, 1);
	v_assert_ptr(block->nodes + 0, ==, node);
	v_assert_size_t(2, ==, TARRAY_COUNT(pool));
	v_assert_size_t(ALL_NODE_FREE - 1, ==, block->ledger);
	v_assert_ptr(block->nodes, ==, node);

	// return node to second pool
	// the second block is entirely free
	ret = list_pool_add(node);
	v_assert_int(1, ==, ret);
	v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	block = TARRAY_GET(pool, 0);
	v_assert_size_t(0, ==, block->ledger);

	// second block gets freed
	ret = list_pool_add(nodes[23]);
	v_assert_int(1, ==, ret);
	v_assert_size_t(1, ==, TARRAY_COUNT(pool));
	block = TARRAY_GET(pool, 0);
	v_assert_size_t(1UL << 23, ==, block->ledger);

	teardown_list();
	VTS;
}

/*
 * Append
 */
static void test_00_list_append_SimpleAdd(void)
{
	t_node *node;
	char *str = "###";
	setup_list();

	node = list->start;
	list_append(list, str);

	v_assert_size_t(5, ==, list->count);
	v_assert_str(str, list->end->data);
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_str(str, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	/* free(list->end); */
	teardown_list();
	VTS;
}

static void test_01_list_append_AddOnEmptyList(void)
{
	t_list *emptyList;
	t_node *node;
	char *str = "###";
	char *str2 = "!!!";

	emptyList = list_new();
	list_append(emptyList, str);

	v_assert_size_t(1, ==, emptyList->count);
	v_assert_str(str, emptyList->start->data);
	v_assert_str(str, emptyList->end->data);
	v_assert_ptr(NULL, ==, emptyList->start->next);
	v_assert_ptr(NULL, ==, emptyList->end->next);

	list_append(emptyList, str2);
	v_assert_size_t(2, ==, emptyList->count);
	node = emptyList->start;
	v_assert_str(str, node->data);
	node = node->next;
	v_assert_str(str2, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	/* free(emptyList->start->next); */
	/* free(emptyList->start); */
	/* free(emptyList); */

	VTS;
}

/*
 * Insert
 */
static void test_00_list_insert_FirstPlace(void)
{
	t_node *node;
	char *str = "inserted";

	setup_list();
	list_insert(list, str, 0);

	v_assert_size_t(5, ==, list->count);
	node = list->start;
	v_assert_str(str, node->data);
	node = node->next;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	/* free(list->start); */
	teardown_list();
	VTS;
}

static void test_01_list_insert_LastPlace(void)
{
	t_node *node;
	char *str = "inserted";

	setup_list();
	list_insert(list, str, list->count);

	v_assert_size_t(5, ==, list->count);
	node = list->start;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_str(str, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	/* free(list->end); */
	teardown_list();
	VTS;
}

static void test_02_list_insert_MiddlePlace(void)
{
	t_node *node;
	char *str = "inserted";

	setup_list();
	list_insert(list, str, 2);

	v_assert_size_t(5, ==, list->count);
	node = list->start;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(str, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	/* free(list->start->next->next); */
	teardown_list();
	VTS;
}

static void test_03_list_insert_OutOfRange(void)
{
	t_node *node;
	char *str = "inserted";

	setup_list();
	list_insert(list, str, list->count + 1);

	v_assert_size_t(5, ==, list->count);
	node = list->start;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_str(str, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	/* free(list->end); */
	teardown_list();
	VTS;
}

/*
 * pop
 */
static void test_00_list_pop_FirstPlace(void)
{
	t_node *ret;
	t_node *node;

	setup_list();

	ret = list_pop(list, 0);

	v_assert_size_t(3, ==, list->count);
	v_assert_str(s1, ret->data);
	node = list->start;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_list();
	VTS;
}

static void test_01_list_pop_MiddlePlace(void)
{
	t_node *ret;
	t_node *node;

	setup_list();

	ret = list_pop(list, 2);

	v_assert_size_t(3, ==, list->count);
	v_assert_str(s3, ret->data);
	node = list->start;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_list();
	VTS;
}

static void test_02_list_pop_LastPlace(void)
{
	t_node *ret;
	t_node *node;

	setup_list();

	ret = list_pop(list, list->count - 1);

	v_assert_size_t(3, ==, list->count);
	v_assert_str(s4, ret->data);
	node = list->start;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_list();
	VTS;
}

static void test_03_list_pop_OutOfRange(void)
{
	t_node *ret;
	t_node *node;

	setup_list();

	ret = list_pop(list, list->count);

	v_assert_size_t(4, ==, list->count);
	v_assert_ptr(NULL, ==, ret);
	node = list->start;
	v_assert_str(s1, node->data);
	node = node->next;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_list();
	VTS;
}

static void test_04_list_pop_data_Simple(void)
{
	t_node *node;
	void *ret;

	setup_list();

	ret = list_pop_data(list, 1);

	v_assert_size_t(3, ==, list->count);
	v_assert_ptr(s2, ==, ret);
	v_assert_str(s2, ret);
	/* v_assert_size_t(1, ==, g_list_pool_index); */
	/* v_assert_ptr(node2, ==, g_list_pool[0]); */

	teardown_list();
	VTS;
}

/*
 * Remove
 */
static void test_00_list_remove_Simple(void)
{
	t_node *node;

	setup_list();

	list_remove(list, 0);

	v_assert_size_t(3, ==, list->count);
	node = list->start;
	v_assert_str(s2, node->data);
	node = node->next;
	v_assert_str(s3, node->data);
	node = node->next;
	v_assert_str(s4, node->data);
	node = node->next;
	v_assert_ptr(NULL, ==, node);

	teardown_list();
	VTS;
}

/*
 * Clear
 */
static void test_00_list_clear_Simple(void)
{
	t_node *node;

	setup_list();

	list_clear(list);

	v_assert_size_t(0, ==, list->count);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(NULL, ==, list->start);
	v_assert_ptr(NULL, ==, list->end);
	v_assert_ptr(NULL, ==, list->iterator);

	teardown_list();
	VTS;
}

/*
 * Iterator
 * Tester que ca fonctionne bien même si on enlever un element
 * y compris celui sur lequel il pointe actuellement
 */
static void test_00_list_iterator_SimpleLoop(void)
{
	t_node *node;

	setup_list();

	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(NULL, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(node1, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, list->iter_index);
	v_assert_ptr(node2, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node3, ==, node);
	v_assert_str(s3, node->data);
	v_assert_size_t(2, ==, list->iter_index);
	v_assert_ptr(node3, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node4, ==, node);
	v_assert_str(s4, node->data);
	v_assert_size_t(3, ==, list->iter_index);
	v_assert_ptr(node4, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(NULL, ==, node);
	/* v_assert_size_t(3, ==, list->iter_index); */
	/* v_assert_ptr(node4, ==, list->iterator); */

	node = list_iterator(list);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(node1, ==, list->iterator);

	list_iterator(NULL);
	teardown_list();
	VTS;
}

static void test_01_list_iterator_RemoveCurrentElem(void)
{
	t_node *node;

	setup_list();

	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(NULL, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(node1, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, list->iter_index);
	v_assert_ptr(node2, ==, list->iterator);

	list_remove(list, 2);

	node = list_iterator(list);
	v_assert_ptr(node4, ==, node);
	v_assert_str(s4, node->data);
	v_assert_size_t(2, ==, list->iter_index);
	v_assert_size_t(3, ==, list->count);
	v_assert_ptr(node4, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(NULL, ==, node);
	/* v_assert_size_t(2, ==, list->iter_index); */
	/* v_assert_ptr(NULL, ==, list->iterator); */

	node = list_iterator(list);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(node1, ==, list->iterator);

	list_iterator(NULL);
	teardown_list();
	VTS;
}

static void test_02_list_iterator_NULL(void)
{
	t_node *node;

	setup_list();

	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(NULL, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(node1, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, list->iter_index);
	v_assert_ptr(node2, ==, list->iterator);

	list_iterator(NULL);

	node = list_iterator(list);
	v_assert_ptr(node1, ==, node);
	v_assert_str(s1, node->data);
	v_assert_size_t(0, ==, list->iter_index);
	v_assert_ptr(node1, ==, list->iterator);

	node = list_iterator(list);
	v_assert_ptr(node2, ==, node);
	v_assert_str(s2, node->data);
	v_assert_size_t(1, ==, list->iter_index);
	v_assert_ptr(node2, ==, list->iterator);

	list_iterator(NULL);
	teardown_list();
	VTS;
}

void suite_list(void)
{
	test_00_pooling_EmptyList();
	test_01_pooling_NodeCreation();
	test_02_pooling_BlockCreation();
	test_03_pooling_AddUnusedNodeToPool();
	test_04_pooling_RemoveEmptyBlock();

	test_00_list_append_SimpleAdd();
	test_01_list_append_AddOnEmptyList();

	test_00_list_insert_FirstPlace();
	test_01_list_insert_LastPlace();
	test_02_list_insert_MiddlePlace();
	test_03_list_insert_OutOfRange();

	test_00_list_pop_FirstPlace();
	test_01_list_pop_MiddlePlace();
	test_02_list_pop_LastPlace();
	test_03_list_pop_OutOfRange();
	test_04_list_pop_data_Simple();

	test_00_list_remove_Simple();

	test_00_list_clear_Simple();

	test_00_list_iterator_SimpleLoop();
	test_01_list_iterator_RemoveCurrentElem();
	test_02_list_iterator_NULL();

	VSS;
}
#include "test.h"

static void test1(char *msg, t_list *node)
{
	printf("%s: ", msg);
	ft_lstappend(node, ft_lstnew("Test2", 6));

	if (strcmp((char *)node->next->content, "Test2") == 0)
		puts("Ok");
	else
		puts("Fail!");
	lst_display(node);
	ft_lstdel(&node, &ft_lstdelcontent);
}

void test_lstappend(void)
{
	t_list *node = ft_lstnew("Test", 5);

	puts("=== ft_lstappend ===");
	test1("Test 1", node);
	puts("");
}
#include "test.h"

static void test1(char *msg, t_list *node)
{
	t_list *node3 = ft_lstnew("Test 3", 7);
	t_list *node4 = ft_lstnew("Test 4", 7);

	printf("%s: \n", msg);
	ft_lstappend(node, ft_lstnew("Test 2", 7));
	ft_lstappend(node, node3);
	ft_lstappend(node, node4);
	lst_display(node);
	ft_lstdellastnode(&node);
	puts("ft_lstdellastnode");
	lst_display(node);
	ft_lstdel(&node, ft_lstdelcontent);
}

static void test2(char *msg, t_list *node)
{
	printf("%s: \n", msg);
	lst_display(node);
	ft_lstdellastnode(&node);
	puts("ft_lstdellastnode");
	lst_display(node);
	ft_lstdel(&node, ft_lstdelcontent);
}

void test_lstdellastnode(void)
{
	puts("=== ft_lstdellastnode ===");
	test1("Test 1", ft_lstnew("Test 1", 7));
	test2("\nTest 2", ft_lstnew("Test 1", 7));
	puts("");
}
#include "test.h"

static void test1(char *msg, char *str, char c)
{
	printf("%s:\n", msg);
	t_list *node = ft_lststrsplit(str, c);
	lst_display(node);
	ft_lstdel(&node, &ft_lstdelcontent);
}

void test_lststrsplit(void)
{
	puts("=== ft_lststrsplit ===");
	test1("Test 1", "***Bonjour**les*etudiant***", '*');
	test1("Test 2", "*****les*etudiant", '*');
	test1("Test 3", "", '*');
	test1("Test 4", "Voici une phrase très longue et peu utile !", ' ');
	puts("");
}
