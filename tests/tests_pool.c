#include "ft_pool.h"
#include "narwhal.h"

TEST(pool_test_00_pool_create_PoolSizeIsInsufficient)
{
	t_pool *p;

	p = pool_create(0, 1);
	ASSERT_EQ(p, NULL);

	p = pool_create(sizeof(t_pool) + 1, 1);
	ASSERT_EQ(p, NULL);
}

static void test_02_pool_init_SizeOfArbitraryStruct(void)
{
	t_chunk *chunk;
	size_t size;
	unsigned *n;

	struct s_pool_test {
		void *p[3];
		int i[1];
		char c[1];
	} t;

	// setup
	p = pool_init(&pool, sizeof(struct s_pool_test));
	size = ROUND_UP_8(sizeof(struct s_pool_test));

	// tests
	v_assert_size_t(size, ==, p->elem_size);
	v_assert_size_t(127, ==, p->chunk_capacity);
	v_assert_ptr(NULL, !=, p->free_list);
	v_assert_ptr(NULL, !=, p->chunks_list);

	// linkage interne
	uintptr_t *pValue = p->free_list;
	char *pAddr = (char *)&p->chunks_list->head;

	while (*pValue) {
		v_assert_ptr(pAddr, ==, pValue);
		pAddr += p->elem_size;
		v_assert_uintptr(*pValue, ==, pAddr);
		pValue += p->elem_size / 8;
	}
	v_assert_uintptr(0, ==, *pValue);

	// freelist
	v_assert_ptr(NULL, !=, p->free_list);
	n = p->free_list;
	*n = ~0U;

	teardown();
	VTS;
}

static t_pool *p;

static void setup(size_t size) { p = pool_create(size); }

static void teardown(void)
{
	void *ptr;

	while ((ptr = p->chunks_list)) {
		p->chunks_list = p->chunks_list->next;
		free(ptr);
	}
	free(p);
}

static void test_00_pool_obtain_node_UsageWithUnsignedInt(void)
{
	size_t elem_size = sizeof(unsigned);
	void *pValue;

	setup(elem_size);

	for (size_t i = 0; i < p->chunk_capacity - 1; ++i) {
		pValue = pool_obtain_node(p);
		v_assert_ptr(NULL, !=, pValue);
		v_assert_ptr(*(uintptr_t *)pValue, ==,
			     (char *)pValue + p->elem_size);
		*(unsigned *)pValue = '*';
		v_assert_uint('*', ==, *(unsigned *)pValue);
	}
	// last free node
	pValue = pool_obtain_node(p);
	v_assert_ptr(NULL, !=, pValue);
	v_assert_uintptr(0, ==, *(uintptr_t *)pValue);
	*(unsigned *)pValue = '*';
	v_assert_uint('*', ==, *(unsigned *)pValue);

	// Fin du chunk
	v_assert_ptr(NULL, ==, p->free_list);

	// Creation d'un nouveau chunk
	pValue = pool_obtain_node(p);
	v_assert_ptr(NULL, !=, p->free_list);
	v_assert_ptr(NULL, !=, pValue);
	*(unsigned *)pValue = '*';
	(void)pValue;

	teardown();
}

static void test_01_pool_obtain_node_UsageWithArbitratyStruct(void)
{
	struct s_pool_test {
		void *p;
		long l;
		char c[3];
	} t;
	size_t elem_size = sizeof(struct s_pool_test);
	void *pValue;

	setup(elem_size);

	for (size_t i = 0; i < p->chunk_capacity - 1; ++i) {
		pValue = pool_obtain_node(p);
		v_assert_ptr(NULL, !=, pValue);
		v_assert_ptr(*(uintptr_t *)pValue, ==,
			     (char *)pValue + p->elem_size);
		*(struct s_pool_test *)pValue = (struct s_pool_test){
		    .p = (void *)i,
		    .l = (long)i,
		};
		v_assert_ptr(i, ==, ((struct s_pool_test *)pValue)->p);
		v_assert_long(i, ==, ((struct s_pool_test *)pValue)->l);
	}
	// last free node
	pValue = pool_obtain_node(p);
	v_assert_ptr(NULL, !=, pValue);
	v_assert_uintptr(0, ==, *(uintptr_t *)pValue);

	// Fin du chunk
	v_assert_ptr(NULL, ==, p->free_list);

	// Creation d'un nouveau chunk
	pValue = pool_obtain_node(p);
	v_assert_ptr(NULL, !=, p->free_list);
	v_assert_ptr(NULL, !=, pValue);
	*(struct s_pool_test *)pValue = (struct s_pool_test){
	    .p = (void *)0xdeadbeef,
	    .l = (long)~0,
	};
	v_assert_ptr(0xdeadbeef, ==, ((struct s_pool_test *)pValue)->p);
	v_assert_long(~0, ==, ((struct s_pool_test *)pValue)->l);

	teardown();
}

static void test_00_pool_release_node_AllElementTypeUnsignedIntInOrder(void)
{
	size_t elem_size = sizeof(unsigned);
	char *pAddr;

	// setup
	setup(elem_size);
	void *pValue[p->chunk_capacity];

	// get each nodes
	for (size_t i = 0; i < p->chunk_capacity; ++i)
		pValue[i] = pool_obtain_node(p);
	v_assert_ptr(NULL, ==, p->free_list);

	// test
	for (size_t i = 0; i < p->chunk_capacity; ++i) {
		pool_release_node(p, pValue[i]);
		v_assert_ptr(pValue[i], ==, p->free_list);
	}

	teardown();
	VTS;
}

static void test_01_pool_release_node_AllElementTypeUnsignedIntInDisorder(void)
{
	size_t elem_size = sizeof(unsigned);
	char *pAddr;

	// setup
	setup(elem_size);
	void *pValue[p->chunk_capacity];

	// get each nodes
	for (size_t i = 0; i < p->chunk_capacity; ++i)
		pValue[i] = pool_obtain_node(p);
	v_assert_ptr(NULL, ==, p->free_list);

	// test
	pool_release_node(p, pValue[3]);
	v_assert_ptr(pValue[3], ==, p->free_list);
	pool_release_node(p, pValue[1]);
	v_assert_ptr(pValue[1], ==, p->free_list);
	pool_release_node(p, pValue[0]);
	v_assert_ptr(pValue[0], ==, p->free_list);
	pool_release_node(p, pValue[4]);
	v_assert_ptr(pValue[4], ==, p->free_list);
	pool_release_node(p, pValue[2]);
	v_assert_ptr(pValue[2], ==, p->free_list);

	// obtain
	void *pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[2], ==, pObtain);
	pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[4], ==, pObtain);
	pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[0], ==, pObtain);
	pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[1], ==, pObtain);
	pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[3], ==, pObtain);

	teardown();
	VTS;
}

static void test_02_pool_release_node_MultiChunksInDisorder(void)
{
	size_t elem_size = 4000;
	char *pAddr;

	// test
	setup(elem_size);
	v_assert_size_t(1, ==, p->chunk_capacity);
	void *pValue[3];

	// get each nodes
	for (size_t i = 0; i < 3; ++i)
		pValue[i] = pool_obtain_node(p);
	v_assert_ptr(NULL, ==, p->free_list);

	// test
	pool_release_node(p, pValue[1]);
	v_assert_ptr(pValue[1], ==, p->free_list);
	pool_release_node(p, pValue[2]);
	v_assert_ptr(pValue[2], ==, p->free_list);
	pool_release_node(p, pValue[0]);
	v_assert_ptr(pValue[0], ==, p->free_list);

	// obtain
	void *pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[0], ==, pObtain);
	pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[2], ==, pObtain);
	pObtain = pool_obtain_node(p);
	v_assert_ptr(pValue[1], ==, pObtain);

	teardown();
	VTS;
}

static void test_00_pool_shutdown_MultiChunksInPool(void)
{
	size_t elem_size = sizeof(char *);

	// setup
	setup(elem_size);
	for (size_t i = 0; i < 6; ++i) {
		pool_obtain_node(p);
	}

	// shutdown du pool
	pool_shutdown(p);
	v_assert_ptr(NULL, ==, p->free_list);
	v_assert_ptr(NULL, ==, p->chunks_list);
	v_assert_size_t(sizeof(char *), ==, p->elem_size);
	v_assert_size_t(511, ==, p->chunk_capacity);

	teardown();
	VTS;
}

static void test_01_pool_shutdown_NewAllocAftershutdown(void)
{
	size_t elem_size = sizeof(char *);
	size_t count = 1;
	void *pValue[6];

	// setup
	setup(elem_size);
	for (size_t i = 0; i < 6; ++i) {
		pValue[i] = pool_obtain_node(p);
		pValue[i] = "Hello World!";
		v_assert_str("Hello World!", pValue[i]);
	}

	// shutdown du pool
	pool_shutdown(p);
	v_assert_ptr(NULL, ==, p->free_list);
	v_assert_ptr(NULL, ==, p->chunks_list);
	v_assert_size_t(sizeof(char *), ==, p->elem_size);
	v_assert_size_t(511, ==, p->chunk_capacity);

	// nouvelle allocation
	pValue[0] = pool_obtain_node(p);
	pValue[0] = "New alloc";
	v_assert_str("New alloc", pValue[0]);

	teardown();
	VTS;
}
