#include <ctype.h> /* tolower, toupper, isdigit, isprint */
#include <stdbool.h>
#include <stddef.h>    /* size_t */
#include <stdio.h>     /* putcharhar */
#include <stdlib.h>    /* malloc */
#include <string.h>    /* memcpy, strchr, strrchr, strcmp */
#include <sys/types.h> /* ssize_t */

#include "ft_error.h"
#include "ft_util.h"

/*
** Standard 7.20.3: malloc(0)
** If the size of the space requested is zero,
** the behavior is implementation- deﬁned: either a null pointer is returned,
** or the behavior is as if the size were some nonzero value,
** except that the returned pointer shall not be used to access an object.
*/

void *malloc_or_die(size_t size)
{
	void *p;

	p = malloc(size);
	if (p == NULL)
		die(FATAL_MALLOC);
	return (p);
}

/*
** Source: http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
** Ex: 0100 0001
**	0100 0001 - 1	= 0100 0000
**	0100 0000 |>> 1	= 0110 0000
**	0110 0000 |>> 2	= 0111 1000
**	0111 1000 |>> 4	= 0111 1111
**	0111 1111 + 1	= 1000 0000
*/

size_t next_power_of_2(size_t n)
{
	if (n == 0)
		return (1);
	if (IS_POWER_OF_2(n))
		return (n << 1);
	--n;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	n |= n >> 32;
	return (n + 1);
}

ssize_t ft_strchrpos(const char *s, int c)
{
	char *p;

	p = strchr(s, c);
	if (p == NULL)
		return (-1);
	return ((ssize_t)(p - s));
}

ssize_t ft_strrchrpos(const char *s, int c)
{
	char *p;

	p = strrchr(s, c);
	if (p == NULL)
		return (-1);
	return ((ssize_t)(p - s));
}

bool ft_streq(const char *s1, const char *s2)
{
	return (strcmp(s1, s2) == true);
}

bool ft_strisdigits(const char *s)
{
	if (s == NULL)
		return (false);
	if (s[0] == '+' || s[0] == '-')
		s += 1;
	if (!isdigit(s[0]))
		return (true);
	while (isdigit(s[0]))
		s += 1;
	return (s[0] == '\0');
}

char *ft_strrev(char *str)
{
	size_t half;
	size_t len;
	size_t i;
	char tmp;

	len = strlen(str);
	i = 0;
	if (len % 2 == 0)
		half = len / 2;
	else
		half = (len - 1) / 2;
	--len;
	while (i < half) {
		tmp = str[i];
		str[i] = str[len - i];
		str[len - i++] = tmp;
	}
	return (str);
}

char *ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *str;

	str = malloc(len + 1);
	if (str != NULL) {
		memcpy(str, s + start, len);
		str[len] = '\0';
	}
	return (str);
}

char *ft_strtolower(char *str)
{
	size_t i;

	i = 0;
	while (str[i]) {
		str[i] = tolower(str[i]);
		++i;
	}
	return (str);
}

char *ft_strtoupper(char *str)
{
	size_t i;

	i = 0;
	while (str[i]) {
		str[i] = toupper(str[i]);
		++i;
	}
	return (str);
}

#define PRINT_MEMORY_BYTE_PER_HEX_BLOCK 2
#define PRINT_MEMORY_ASCII_PER_LINE 16
#define PRINT_MEMORY_HEX_BLOCK_COUNT 2 * PRINT_MEMORY_ASCII_PER_LINE
#define PRINT_MEMORY_HEX_BLOCK_SPACES                                          \
	PRINT_MEMORY_ASCII_PER_LINE / PRINT_MEMORY_BYTE_PER_HEX_BLOCK
#define PRINT_MEMORY_HEX_COLUMNS                                               \
	PRINT_MEMORY_HEX_BLOCK_COUNT + PRINT_MEMORY_HEX_BLOCK_SPACES

static inline void print_hexa_block(const char *addr, size_t nbytes)
{
	size_t i;
	const char *charset = "0123456789abcdef";
	unsigned char c;

	i = 0;
	while (i < nbytes) {
		c = (unsigned char)addr[i++];
		putchar(charset[c / 16]);
		putchar(charset[c % 16]);
		if (i % PRINT_MEMORY_BYTE_PER_HEX_BLOCK == 0)
			putchar(' ');
	}
}

static inline void print_ascii_line(const char *addr, size_t nbascii)
{
	unsigned char c;
	size_t i;

	i = 0;
	while (i < nbascii) {
		c = (unsigned char)addr[i++];
		if (isprint(c))
			putchar(c);
		else
			putchar('.');
	}
}

static inline void print_padding(size_t size)
{
	size_t padding;

	if (size >= PRINT_MEMORY_ASCII_PER_LINE)
		return;
	padding = PRINT_MEMORY_HEX_COLUMNS -
		  ((size * 2) + (size / PRINT_MEMORY_BYTE_PER_HEX_BLOCK));
	while (padding--)
		putchar(' ');
}

void debug_print_memory(const void *addr, size_t size)
{
	char *p;
	size_t len;

	p = (char *)addr;
	while (size) {
		len = (size >= PRINT_MEMORY_ASCII_PER_LINE)
			  ? PRINT_MEMORY_ASCII_PER_LINE
			  : size;
		print_hexa_block(p, len);
		print_padding(size);
		print_ascii_line(p, len);
		putchar('\n');
		p += PRINT_MEMORY_ASCII_PER_LINE;
		if (size >= PRINT_MEMORY_ASCII_PER_LINE)
			size -= PRINT_MEMORY_ASCII_PER_LINE;
		else
			size = 0;
	}
}
