#include <stdarg.h> /* va_start, va_list */
#include <stdio.h>  /* vfprintf, stderr, fputc, perror */
#include <stdlib.h> /* exit(), EXIT_FAILURE */
#include <string.h> /* strlen */

static void error_internal_print(const char *fmt, va_list ap)
{
	vfprintf(stderr, fmt, ap);

	if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
}

void die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	error_internal_print(fmt, ap);
	va_end(ap);

	exit(EXIT_FAILURE);
}

void warn(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	error_internal_print(fmt, ap);
	va_end(ap);
}
