#include <stddef.h>    /* size_t */
#include <stdint.h>    /* uintptr_t */
#include <stdlib.h>    /* malloc, realloc */
#include <string.h>    /* memcpy, memmove */
#include <sys/types.h> /* ssize_t */

#include "ft_array.h"
#include "ft_util.h" /* next_power_of_2, IS_POWER_OF_2 */

void array_clear(t_array *a) { a->len = 0; }

t_array *array_clone(t_array *dst, const t_array *src)
{
	if (array_init_with_capacity(dst, src->capacity, src->elem_size) ==
	    NULL)
		return (NULL);
	dst->len = src->len;
	memcpy(dst->data, src->data, src->len * src->elem_size);
	return (dst);
}

t_array *array_create(size_t elem_size)
{
	return (array_create_with_capacity(elem_size, ARRAY_INIT_SIZE));
}

t_array *array_create_with_capacity(size_t elem_size, size_t capacity)
{
	t_array *a;

	a = malloc(sizeof(t_array));
	if (a == NULL)
		return (NULL);
	if (array_init_with_capacity(a, elem_size, capacity) == NULL) {
		free(a);
		return (NULL);
	}
	return (a);
}

void array_destroy(t_array *a)
{
	free(a->data);
	free(a);
}

void *array_find_from(t_array set, size_t n,
		      int(match)(const void *elem, const void *param),
		      const void *param)
{
	void *elem;

	while ((elem = array_get_at(&set, n++)) != NULL)
		if (match(elem, param))
			return (elem);
	return (NULL);
}

void *array_find(t_array set, int(match)(const void *elem, const void *param),
		 const void *param)
{
	return (array_find_from(set, 0, match, param));
}

void *array_get_available(t_array *a)
{
	if (array_reserve(a, 1) == NULL)
		return (NULL);
	a->len += 1;
	return (array_get_at(a, a->len - 1));
}

void *array_get_at(const t_array *a, size_t i)
{
	if (i < a->len)
		return ((char *)a->data + i * a->elem_size);
	return (NULL);
}

void *array_set_at(t_array *a, size_t i, const void *e)
{
	if (i < a->len)
		return (memcpy(array_get_at(a, i), e, a->elem_size));
	return (NULL);
}

void *array_get_last(const t_array *a)
{
	if (a->len == 0)
		return (NULL);
	return (array_get_at(a, a->len - 1));
}

void *array_get_first(const t_array *a)
{
	if (a->len > 0)
		return (array_get_at(a, 0));
	return (NULL);
}

ssize_t array_index_of(const t_array *a, const void *e)
{
	uintptr_t index;

	if (e >= a->data &&
	    (uintptr_t)e < (uintptr_t)a->data + a->len * a->elem_size) {
		index = (uintptr_t)e - (uintptr_t)a->data;
		index /= a->elem_size;
		return ((ssize_t)index);
	}
	return (-1);
}

t_array *array_init_with_capacity(t_array *a, size_t elem_size, size_t cap)
{
	if (elem_size == 0)
		return (NULL);
	a->capacity = cap;
	if (a->capacity < ARRAY_INIT_SIZE)
		a->capacity = ARRAY_INIT_SIZE;
	else if (!IS_POWER_OF_2(a->capacity))
		a->capacity = next_power_of_2(a->capacity);
	a->len = 0;
	a->elem_size = elem_size;
	if ((a->data = malloc(a->elem_size * a->capacity)) == NULL)
		return (NULL);
	return (a);
}

t_array *array_init(t_array *a, size_t elem_size)
{
	return (array_init_with_capacity(a, elem_size, ARRAY_INIT_SIZE));
}

void array_shutdown(t_array *a) { free(a->data); }

t_array *array_insert_at(t_array *a, size_t i, const void *e)
{
	char *src;

	if (i > a->len)
		return (NULL);
	if (array_reserve(a, 1) == NULL)
		return (NULL);
	if (i == a->len)
		return (array_push(a, e));
	src = array_get_at(a, i);
	memmove(src + a->elem_size, src, (a->len - i) * a->elem_size);
	array_set_at(a, i, e);
	a->len += 1;
	return (a);
}

t_array *array_remove_at(t_array *a, size_t i, void *removed)
{
	char *p;
	size_t len;

	if (i >= a->len)
		return (NULL);
	p = array_get_at(a, i);
	if (removed != NULL)
		memcpy(removed, p, a->elem_size);
	a->len -= 1;
	len = (a->len - i) * a->elem_size;
	memmove(p, p + a->elem_size, len);
	return (a);
}

t_array *array_remove_elem(t_array *a, const void *e)
{
	ssize_t index;

	index = array_index_of(a, e);
	if (index == -1)
		return (NULL);
	return (array_remove_at(a, (size_t)index, NULL));
}

t_array *array_replace_at(t_array *a, size_t i, const void *e, void *old)
{
	void *p;

	if (i >= a->len)
		return (NULL);
	if (old != NULL) {
		p = array_get_at(a, i);
		memcpy(old, p, a->elem_size);
	}
	array_set_at(a, i, e);
	return (a);
}

t_array *array_reserve(t_array *a, size_t additional)
{
	if (a->capacity - a->len < additional) {
		if (IS_POWER_OF_2(a->len + additional))
			a->capacity = a->len + additional;
		else
			a->capacity = next_power_of_2(a->len + additional);
		a->data = realloc(a->data, a->capacity * a->elem_size);
		if (a->data == NULL)
			return (NULL);
	}
	return (a);
}

t_array *array_shrink_to_fit(t_array *a)
{
	size_t capacity;

	if (a->len != a->capacity) {
		if (a->len <= ARRAY_INIT_SIZE)
			capacity = ARRAY_INIT_SIZE;
		else
			capacity = next_power_of_2(a->len);

		if (capacity < a->capacity) {
			a->capacity = capacity;
			a->data = realloc(a->data, a->capacity * a->elem_size);
			if (a->data == NULL)
				return (NULL);
		}
	}
	return (a);
}

t_array *array_push(t_array *a, const void *e)
{
	if (array_reserve(a, 1) == NULL)
		return (NULL);
	a->len += 1;
	memcpy(array_get_at(a, a->len - 1), e, a->elem_size);
	return (a);
}

t_array *array_pop(t_array *a, void *old)
{
	if (a->len == 0)
		return (NULL);
	if (array_remove_at(a, a->len - 1, old) == NULL)
		return (NULL);
	return (a);
}

t_array *array_swap(t_array *a, size_t e1, size_t e2)
{
	unsigned char tmp[a->elem_size];
	void *elem1;
	void *elem2;

	if (e1 >= a->len || e2 >= a->len)
		return (NULL);
	elem1 = array_get_at(a, e1);
	elem2 = array_get_at(a, e2);
	memcpy(tmp, elem1, a->elem_size);
	memcpy(elem1, elem2, a->elem_size);
	memcpy(elem2, tmp, a->elem_size);
	return (a);
}

t_array *array_truncate(t_array *a, size_t n)
{
	if (n > a->len)
		return (NULL);
	a->len = n;
	return (a);
}
