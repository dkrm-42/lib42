#include <stdarg.h> /* va_args macros */
#include <stdbool.h>
#include <stddef.h> /* NULL */
#include <stdlib.h> /* malloc, free */
#include <string.h> /* memcpy */

#include "ft_list.h"

static t_list_node *internal_new_node_with_copy(t_list *list, const void *elem)
{
	t_list_node *new_node;
	void *data;

	new_node = pool_acquire_block(&list->pool);
	if (new_node == NULL)
		return (NULL);
	data = LIST_NODE_DATA(new_node);
	memcpy(data, elem, list->elem_size);
	return (new_node);
}

void list_apply(t_list *list, t_list_apply f, ...)
{
	va_list ap;
	t_list_node *node;

	va_start(ap, f);
	node = list->head;
	while (node != NULL) {
		f(LIST_NODE_DATA(node), ap);
		node = node->next;
	}
	va_end(ap);
}

t_list *list_clear(t_list *list)
{
	size_t elem_size;

	elem_size = list->elem_size;
	list_shutdown(list);
	return (list_init(list, elem_size));
}

bool list_contains(const t_list *list, const void *elem, t_list_compare cmp)
{
	t_list_node *node;
	const void *data;

	node = list->head;
	while (node != NULL) {
		data = LIST_NODE_DATA(node);
		if (cmp(data, elem) == 0)
			return (true);
		node = node->next;
	}
	return (false);
}

static const void *identity(const void *data, ...) { return (data); }

t_list *list_copy(const t_list *list)
{
	return (list_map(list, (t_list_map)&identity, list->elem_size));
}

t_list *list_create(size_t elem_size)
{
	t_list *list;

	list = malloc(sizeof(t_list));
	if (list == NULL)
		return (NULL);
	if (list_init(list, elem_size) == NULL) {
		free(list);
		return (NULL);
	}
	return (list);
}

void list_destroy(t_list *list)
{
	list_shutdown(list);
	free(list);
}

t_list *list_filter(const t_list *list, t_list_filter f, ...)
{
	va_list ap;
	t_list *new_list;
	t_list_node *node;
	void *data;

	va_start(ap, f);
	new_list = list_create(list->elem_size);
	if (new_list == NULL)
		return (NULL);
	node = list->head;
	while (node != NULL) {
		data = LIST_NODE_DATA(node);
		if (f(data, ap)) {
			if (list_push_back(new_list, data) == NULL) {
				list_destroy(new_list);
				return (NULL);
			}
		}
		node = node->next;
	}
	va_end(ap);
	return (new_list);
}

void *list_fold_left(const t_list *list, t_list_fold f, void *init_val, ...)
{
	va_list ap;
	t_list_node *node;
	void *accumulator;

	va_start(ap, init_val);
	accumulator = init_val;
	node = list->head;
	while (node != NULL) {
		accumulator = f(accumulator, LIST_NODE_DATA(node), ap);
		if (accumulator == NULL)
			return (NULL);
		node = node->next;
	}
	va_end(ap);
	return (accumulator);
}

void *list_get_at(const t_list *list, size_t index)
{
	t_list_node *node;
	void *data;

	if (index >= list->len)
		return (NULL);
	node = list->head;
	while (index > 0) {
		node = node->next;
		index -= 1;
	}
	data = LIST_NODE_DATA(node);
	return (data);
}

void *list_get_first(const t_list *list)
{
	void *data;

	if (list->len == 0)
		return (NULL);
	data = LIST_NODE_DATA(list->head);
	return (data);
}

void *list_get_last(const t_list *list)
{
	void *data;

	if (list->len == 0)
		return (NULL);
	data = LIST_NODE_DATA(list->tail);
	return (data);
}

void *list_set_at(t_list *list, size_t index, const void *elem)
{
	void *data;

	if (index >= list->len)
		return (NULL);
	if (index == 0)
		data = list_get_first(list);
	else if (index == list->len - 1)
		data = list_get_last(list);
	else
		data = list_get_at(list, index);
	memcpy(data, elem, list->elem_size);
	return (data);
}

t_list *list_init(t_list *list, size_t elem_size)
{
	if (elem_size == 0)
		return (NULL);
	list->pool = pool_create(POOL_DEFAULT_SIZE, LIST_NODE_SIZE(elem_size));
	if (list->pool == NULL)
		return (NULL);
	list->head = NULL;
	list->tail = NULL;
	list->len = 0;
	list->elem_size = elem_size;
	return (list);
}

void list_shutdown(t_list *list)
{
	pool_destroy(list->pool);
	list->pool = NULL;
	list->head = NULL;
	list->tail = NULL;
	list->len = 0;
	list->elem_size = 0;
}

void *list_insert_at(t_list *list, size_t index, const void *elem)
{
	t_list_node *new_node;
	t_list_node *current;

	if (index >= list->len)
		return (NULL);
	if (index == 0)
		return (list_push_front(list, elem));
	new_node = internal_new_node_with_copy(list, elem);
	if (new_node == NULL)
		return (NULL);

	current = list->head;
	while (index >= 1) {
		current = current->next;
		index -= 1;
	}
	current->prev->next = new_node;
	new_node->prev = current->prev;
	new_node->next = current;
	current->prev = new_node;

	list->len += 1;
	return (LIST_NODE_DATA(new_node));
}

t_list *list_map(const t_list *list, t_list_map f, size_t new_elem_size, ...)
{
	va_list ap;
	t_list *new_list;
	t_list_node *node;
	void *new_data;

	va_start(ap, new_elem_size);
	new_list = list_create(new_elem_size);
	if (new_list == NULL)
		return (NULL);
	node = list->head;
	while (node != NULL) {
		new_data = f(LIST_NODE_DATA(node), ap);
		if (new_data == NULL) {
			list_destroy(new_list);
			return (NULL);
		}
		list_push_back(new_list, new_data);
		node = node->next;
	}
	va_end(ap);
	return (new_list);
}

t_list *list_pop_front(t_list *list, void *poped_elem)
{
	if (list->len == 0)
		return (NULL);
	return (list_remove_at(list, 0, poped_elem));
}

t_list *list_pop_back(t_list *list, void *poped_elem)
{
	if (list->len == 0)
		return (NULL);
	return (list_remove_at(list, list->len - 1, poped_elem));
}

void *list_push_front(t_list *list, const void *elem)
{
	t_list_node *node;

	node = internal_new_node_with_copy(list, elem);
	if (node == NULL)
		return (NULL);
	node->prev = NULL;
	node->next = list->head;
	if (list->len == 0)
		list->tail = node;
	else
		list->head->prev = node;
	list->head = node;
	list->len += 1;
	return (LIST_NODE_DATA(node));
}

void *list_push_back(t_list *list, const void *elem)
{
	t_list_node *node;

	node = internal_new_node_with_copy(list, elem);
	if (node == NULL)
		return (NULL);
	node->prev = list->tail;
	node->next = NULL;
	if (list->len == 0)
		list->head = node;
	else
		list->tail->next = node;
	list->tail = node;
	list->len += 1;
	return (LIST_NODE_DATA(node));
}

t_list *list_remove_at(t_list *list, size_t index, void *removed)
{
	t_list_node *node;
	void *data;

	if (index >= list->len)
		return (NULL);
	else if (list->len == 1) // remove the sole node
	{
		node = list->head;
		list->head = NULL;
		list->tail = NULL;
	} else if (index == 0) // remove head
	{
		node = list->head;
		list->head = node->next;
		list->head->prev = NULL;
	} else if (index == list->len - 1) // remove tail
	{
		node = list->tail;
		list->tail = node->prev;
		list->tail->next = NULL;
	} else // remove node
	{
		node = list->head;
		while (index) {
			node = node->next;
			index -= 1;
		}
		node->prev->next = node->next;
		node->next->prev = node->prev;
	}

	list->len -= 1;
	if (removed != NULL) {
		data = LIST_NODE_DATA(node);
		memcpy(removed, data, list->elem_size);
		list->pool = pool_release_block(list->pool, node);
	}
	return (list);
}

/*
** Return the middle node of the list `head`
** `head` stays the head
*/

inline static t_list_node *split_in_half(t_list_node *head)
{
	t_list_node *slow;
	t_list_node *fast;
	t_list_node *middle;

	slow = head;
	fast = head;
	while (fast->next != NULL && fast->next->next != NULL) {
		slow = slow->next;
		fast = fast->next->next;
	}
	middle = slow->next;
	slow->next = NULL;
	return (middle);
}

/*
** Merge 2 sorted lists together
*/

inline static t_list_node *merge(t_list_node *a, t_list_node *b,
				 t_list_compare compare)
{
	t_list_node *new_head;
	t_list_node **p;
	t_list_node *prev;

	p = &new_head;
	prev = NULL;
	while (a != NULL && b != NULL) {
		if (compare(LIST_NODE_DATA(a), LIST_NODE_DATA(b)) < 0) {
			(*p) = a;
			a = a->next;
		} else {
			(*p) = b;
			b = b->next;
		}
		(*p)->prev = prev;
		prev = (*p);
		p = &(*p)->next;
	}
	(*p) = (a == NULL) ? b : a;
	(*p)->prev = prev;
	return (new_head);
}

inline static t_list_node *merge_sort(t_list_node *head, t_list_compare compare)
{
	t_list_node *second;

	if (head == NULL || head->next == NULL)
		return (head);
	second = split_in_half(head);
	head = merge_sort(head, compare);
	second = merge_sort(second, compare);
	head = merge(head, second, compare);
	return (head);
}

t_list *list_sort(t_list *list, t_list_compare compare)
{
	t_list_node *new_tail;

	list->head = merge_sort(list->head, compare);

	new_tail = list->head;
	while (new_tail != NULL && new_tail->next != NULL)
		new_tail = new_tail->next;
	list->tail = new_tail;

	return (list);
}

t_list *list_swap(t_list *list, size_t e1, size_t e2)
{
	void *data1;
	void *data2;
	void *tmp;

	if (e1 >= list->len || e2 >= list->len)
		return (NULL);
	if (e1 == e2)
		return (list);
	tmp = pool_acquire_block(&list->pool);
	if (tmp == NULL)
		return (NULL);
	data1 = list_get_at(list, e1);
	data2 = list_get_at(list, e2);
	memcpy(tmp, data1, list->elem_size);
	memcpy(data1, data2, list->elem_size);
	memcpy(data2, tmp, list->elem_size);
	pool_release_block(list->pool, tmp);
	return (list);
}

t_list *list_uniq_sorted(const t_list *list, t_list_compare cmp)
{
	t_list *uniq;
	t_list_node *iter_node;

	uniq = list_create(list->elem_size);
	if (uniq == NULL || list->len == 0)
		return (uniq);
	iter_node = list->head;
	list_push_back(uniq, LIST_NODE_DATA(iter_node));
	while (iter_node != NULL) {
		if (cmp(LIST_NODE_DATA(uniq->tail),
			LIST_NODE_DATA(iter_node)) != 0)
			list_push_back(uniq, LIST_NODE_DATA(iter_node));
		iter_node = iter_node->next;
	}
	return (uniq);
}

t_list *list_uniq_unsorted(const t_list *non_uniq, t_list_compare cmp)
{
	t_list *uniq;
	t_list_node *node;
	const void *data;

	uniq = list_create(non_uniq->elem_size);
	if (uniq == NULL || non_uniq->len == 0)
		return (uniq);
	node = non_uniq->head;
	while (node != NULL) {
		data = LIST_NODE_DATA(node);
		if (list_contains(uniq, data, cmp) == false)
			list_push_back(uniq, data);
		node = node->next;
	}
	return (uniq);
}
