#include <stdarg.h>    /* va_args macros */
#include <stddef.h>    /* size_t */
#include <stdlib.h>    /* malloc, realloc, free */
#include <string.h>    /* memset, memcpy, strlen */
#include <sys/types.h> /* ssize_t */

#include "ft_string.h"
#include "ft_util.h" /* ft_strrchrpos, next_power_of_2, IS_POWER_OF_2 */

t_string *string_create(void)
{
	return (string_create_with_capacity(STRING_INIT_SIZE - 1));
}

t_string *string_create_with_capacity(size_t capacity)
{
	t_string *s;

	s = malloc(sizeof(t_string));
	if (s == NULL)
		return (NULL);
	if (string_init_with_capacity(s, capacity) == NULL)
		return (NULL);
	return (s);
}

void string_destroy(t_string *s)
{
	free(s->str);
	free(s);
}

t_string *string_init_with_capacity(t_string *s, size_t capacity)
{
	s->capacity = capacity + 1;
	if (s->capacity < STRING_INIT_SIZE)
		s->capacity = STRING_INIT_SIZE;
	else if (!IS_POWER_OF_2(s->capacity))
		s->capacity = next_power_of_2(s->capacity);
	if ((s->str = malloc(s->capacity)) == NULL)
		return (NULL);
	s->len = 0;
	s->str[0] = '\0';
	return (s);
}

t_string *string_init(t_string *s)
{
	return (string_init_with_capacity(s, STRING_INIT_SIZE - 1));
}

void string_shutdown(t_string *s) { free(s->str); }

t_string *string_init_ndup(t_string *s, const char *str, size_t len)
{
	if (string_init_with_capacity(s, len) == NULL)
		return (NULL);
	memcpy(s->str, str, len);
	s->len = len;
	s->str[s->len] = '\0';
	return (s);
}

t_string *string_init_dup(t_string *s, const char *str)
{
	return (string_init_ndup(s, str, strlen(str)));
}

t_string *string_create_ndup(const char *str, size_t len)
{
	t_string *s;

	if ((s = string_create_with_capacity(len)) == NULL)
		return (NULL);
	memcpy(s->str, str, len);
	s->len = len;
	s->str[s->len] = '\0';
	return (s);
}

t_string *string_create_dup(const char *str)
{
	return (string_create_ndup(str, strlen(str)));
}

t_string *string_clone(t_string *dst, const t_string *src)
{
	return (string_init_ndup(dst, src->str, src->len));
}

t_string *string_merge(t_string *dst, const t_string *a, const t_string *b)
{
	size_t size;

	size = a->len + b->len;
	if (string_init_with_capacity(dst, size) == NULL)
		return (NULL);
	dst->len = a->len + b->len;
	memcpy(dst->str, a->str, a->len);
	memcpy(dst->str + a->len, b->str, b->len);
	dst->str[dst->len] = '\0';
	return (dst);
}

t_string *string_cat(t_string *s, const char *str)
{
	return (string_ncat(s, str, strlen(str)));
}

t_string *string_ncat(t_string *s, const char *str, size_t len)
{
	if (string_reserve(s, len) == NULL)
		return (NULL);
	memcpy(s->str + s->len, str, len);
	s->len += len;
	s->str[s->len] = '\0';
	return (s);
}

t_string *string_append(t_string *dst, const t_string *src)
{
	return (string_ncat(dst, src->str, src->len));
}

t_string *string_replace(t_string *s, const char *str)
{
	return (string_nreplace(s, str, strlen(str)));
}

t_string *string_nreplace(t_string *s, const char *str, size_t len)
{
	s->len = 0;
	if (string_ncat(s, str, len) == NULL)
		return (NULL);
	return (s);
}

t_string *string_set(t_string *s, size_t from, size_t n, int c)
{
	if (from > s->len)
		return (NULL);
	if (string_reserve(s, n) == NULL)
		return (NULL);
	memset(s->str + from, c, n);
	if (from + n > s->len)
		s->len = from + n;
	s->str[s->len] = '\0';
	return (s);
}

t_string *string_insert(t_string *s, size_t pos, const char *str, size_t len)
{
	if (pos > s->len)
		return (NULL);
	if (string_reserve(s, len) == NULL)
		return (NULL);
	memmove(s->str + pos + len, s->str + pos, s->len - pos);
	memcpy(s->str + pos, str, len);
	s->len += len;
	s->str[s->len] = 0;
	return (s);
}

size_t string_remove(t_string *s, size_t pos, size_t n)
{
	size_t removed;

	if (pos >= s->len)
		return (0);
	if (pos + n >= s->len) {
		removed = s->len - pos;
		s->len = pos;
	} else {
		removed = n;
		memmove(s->str + pos, s->str + pos + n, s->len - (pos + n));
		s->len -= n;
	}
	s->str[s->len] = '\0';
	return (removed);
}

ssize_t string_remove_back(t_string *s, size_t n)
{
	if (n > s->len)
		return (-1);
	s->len -= n;
	s->str[s->len] = '\0';
	return ((ssize_t)s->len);
}

ssize_t string_remove_back_chr(t_string *s, int c)
{
	ssize_t pos;
	size_t ret;

	pos = ft_strrchrpos(s->str, c);
	if (pos == -1)
		return (-1);
	s->str[pos] = '\0';
	ret = s->len - (size_t)pos;
	s->len = (size_t)pos;
	return ((ssize_t)ret);
}

t_string *string_reserve(t_string *s, size_t additional)
{
	additional += 1;
	if (s->capacity - s->len < additional) {
		if (IS_POWER_OF_2(s->len + additional))
			s->capacity = s->len + additional;
		else
			s->capacity = next_power_of_2(s->len + additional);
		s->str = realloc(s->str, s->capacity);
		if (s->str == NULL)
			return (NULL);
	}
	return (s);
}

t_string *string_shrink_to_fit(t_string *s)
{
	size_t capacity;

	if (s->len + 1 != s->capacity) {
		if (s->len + 1 <= STRING_INIT_SIZE)
			capacity = STRING_INIT_SIZE;
		else
			capacity = next_power_of_2(s->len + 1);
		if (capacity < s->capacity) {
			s->capacity = capacity;
			s->str = realloc(s->str, capacity);
			if (s->str == NULL)
				return (NULL);
		}
	}
	return (s);
}

t_string *string_truncate(t_string *s, size_t n)
{
	if (n > s->len)
		return (NULL);
	s->len = n;
	s->str[s->len] = '\0';
	return (s);
}
