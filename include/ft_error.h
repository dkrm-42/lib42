#ifndef ERROR_42_H
#define ERROR_42_H

#define FATAL_MALLOC "fatal: malloc failed!"

typedef enum e_return_code {
	OK,
	ERROR,
} t_ret;

void die(const char *fmt, ...);
void warn(const char *fmt, ...);

#endif
