#ifndef UTIL_42_H
#define UTIL_42_H

#include <stdbool.h>
#include <stddef.h>    /* size_t */
#include <sys/types.h> /* ssize_t */

#define FT_ISWHITESPACE(c) (isblank(c) || ((c) == '\n'))
#define FT_ABS(x) (((x) < 0) ? -(x) : (x))
#define FT_MAX(a, b) ((a) < (b) ? (b) : (a))
#define FT_MIN(a, b) ((a) > (b) ? (b) : (a))
#define ARR_SIZ_MAX(a) (sizeof(a) / sizeof((a)[0]))
#define IS_POWER_OF_2(n) ((((n)-1) & (n)) == 0)
#define ROUND_UP_16(n) (!(n) || (n) % 16U ? ((n) + 16U) & (~0U - 15U) : (n))
#define ROUND_UP_8(n) (!(n) || (n) % 8U ? ((n) + 8U) & (~0U - 7U) : (n))

size_t next_power_of_2(size_t n);
void *malloc_or_die(size_t size);

/* Returns true if both strings are equal */
bool ft_streq(const char *s1, const char *s2);

/* Returns the position of the character `c` in the string `s` */
ssize_t ft_strchrpos(const char *s, int c);

/* Returns the last position of the character `c` in the string `s` */
ssize_t ft_strrchrpos(const char *s, int c);

/* Returns a substring of the string `s`. The returned string is dynamically
 * allocated */
char *ft_strsub(char const *s, unsigned int start, size_t len);

/* Reverse the string in place */
char *ft_strrev(char *str);

/* Convert to uppercase, in place  */
char *ft_strtoupper(char *str);

/* Convert to lowercase, in place  */
char *ft_strtolower(char *str);

/* Returns true if the string is composed of digits only */
bool ft_strisdigits(const char *str);

/*
** Print Memory
*/
void debug_print_memory(const void *addr, size_t size);

#endif
